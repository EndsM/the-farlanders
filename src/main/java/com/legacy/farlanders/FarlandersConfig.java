package com.legacy.farlanders;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.structure_gel.util.ConfigTemplates;

import net.minecraftforge.common.ForgeConfigSpec;

public class FarlandersConfig
{
	public static final Common COMMON;
	public static final ForgeConfigSpec COMMON_SPEC;
	static
	{
		Pair<Common, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Common::new);
		COMMON_SPEC = specPair.getRight();
		COMMON = specPair.getLeft();
	}

	/*private static class Client
	{
		public Client(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Client side changes.").push("client");
			builder.pop();
		}
	}*/

	public static class Common
	{
		public final ForgeConfigSpec.ConfigValue<Integer> farlanderLootWellSpawnRate;
		public final ForgeConfigSpec.ConfigValue<Integer> obsidianSpireSpawnRate;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableLooterStealing;
		public final ConfigTemplates.StructureConfig smallHouseStructureConfig;
		public final ConfigTemplates.StructureConfig farlanderVillageStructureConfig;

		public Common(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Server and Client side changes.").push("common");
			smallHouseStructureConfig = new ConfigTemplates.StructureConfig(builder, "small_house", 1.3D, 15, 5);
			farlanderVillageStructureConfig = new ConfigTemplates.StructureConfig(builder, "farlander_village", 0.7D, 32, 15);

			farlanderLootWellSpawnRate = builder.translation("farlanderLootWellSpawnRate").comment("Farlander Loot Well rarity (higher number = lower spawnrate)").define("farlanderLootWellSpawnRate", 15);
			obsidianSpireSpawnRate = builder.translation("obsidianSpireSpawnRate").comment("Obsidian Spire rarity (higher number = lower spawnrate)").define("obsidianSpireSpawnRate", 3);

			disableLooterStealing = builder.translation("disableLooterStealing").comment("Disables the Looter's ability to take the player's weapon (This makes them spawn with a random weapon, good and bad)").define("disableLooterStealing", false);
			builder.pop();
		}
	}

}
