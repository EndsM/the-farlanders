package com.legacy.farlanders.entity.ai.tasks;

import com.google.common.collect.ImmutableMap;
import com.legacy.farlanders.entity.FarlanderEntity;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.brain.Brain;
import net.minecraft.entity.ai.brain.memory.MemoryModuleType;
import net.minecraft.entity.ai.brain.schedule.Activity;
import net.minecraft.entity.ai.brain.task.Task;
import net.minecraft.world.server.ServerWorld;

public class FarlanderPanicTask extends Task<FarlanderEntity>
{
	public FarlanderPanicTask()
	{
		super(ImmutableMap.of());
	}

	protected boolean shouldContinueExecuting(ServerWorld worldIn, FarlanderEntity entityIn, long gameTimeIn)
	{
		return func_220512_b(entityIn) || func_220513_a(entityIn);
	}

	protected void startExecuting(ServerWorld worldIn, FarlanderEntity entityIn, long gameTimeIn)
	{
		if (func_220512_b(entityIn) || func_220513_a(entityIn))
		{
			Brain<?> brain = entityIn.getBrain();
			if (!brain.hasActivity(Activity.PANIC))
			{
				brain.removeMemory(MemoryModuleType.PATH);
				brain.removeMemory(MemoryModuleType.WALK_TARGET);
				brain.removeMemory(MemoryModuleType.LOOK_TARGET);
				brain.removeMemory(MemoryModuleType.BREED_TARGET);
				brain.removeMemory(MemoryModuleType.INTERACTION_TARGET);
			}

			brain.switchTo(Activity.PANIC);
		}

	}

	public static boolean func_220513_a(LivingEntity p_220513_0_)
	{
		return p_220513_0_.getBrain().hasMemory(MemoryModuleType.NEAREST_HOSTILE);
	}

	public static boolean func_220512_b(LivingEntity p_220512_0_)
	{
		return p_220512_0_.getBrain().hasMemory(MemoryModuleType.HURT_BY);
	}
}