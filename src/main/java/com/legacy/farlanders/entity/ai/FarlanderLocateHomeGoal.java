package com.legacy.farlanders.entity.ai;

import java.util.EnumSet;

import com.legacy.farlanders.entity.FarlanderEntity;

import net.minecraft.block.BedBlock;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class FarlanderLocateHomeGoal extends Goal
{

	private final FarlanderEntity farlander;

	public FarlanderLocateHomeGoal(FarlanderEntity creature)
	{
		this.farlander = creature;
		this.setMutexFlags(EnumSet.of(Goal.Flag.TARGET));
	}

	@Override
	public boolean shouldExecute()
	{
		return farlander.getHomePos() == null;
	}

	public boolean shouldContinueExecuting()
	{
		return farlander.getHomePos() == null && super.shouldContinueExecuting();
	}

	@Override
	public void tick()
	{
		if (farlander.ticksExisted % 200 == 0 || farlander.ticksExisted < 5)
		{
			//System.out.println("Searching");
			int var1 = MathHelper.floor(farlander.getBoundingBox().minX + (farlander.getBoundingBox().maxX - farlander.getBoundingBox().minX) / 2.0D);
	        int var2 = MathHelper.floor(farlander.getBoundingBox().minZ + (farlander.getBoundingBox().maxZ - farlander.getBoundingBox().minZ) / 2.0D);
	
	        byte radius = 30;
	
	        for (int var4 = var1 - radius; var4 <= var1 + radius; ++var4)
	        {
	            for (int var5 = var2 - radius; var5 <= var2 + radius; ++var5)
	            {
	                for (int var6 = 0; var6 < 15; ++var6)
	                {
	                    int var7 = (int) (farlander.getPosY() - 5 + var6);
	
	                    if (farlander.world.getBlockState(new BlockPos(var4, var7, var5)).getBlock() instanceof BedBlock)
	                    {
	                    	farlander.setHomePos(new BlockPos(var4, var7, var5));
	                    	//System.out.println("Home pos set to: " + new BlockPos(var4, var7, var5));
	                    }
	                }
	            }
	        }
		}
	}
}
