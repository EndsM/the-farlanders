package com.legacy.farlanders.entity.ai.tasks;

import java.util.Optional;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.registry.FarlandersEntityTypes;
import com.mojang.datafixers.util.Pair;

import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.brain.memory.MemoryModuleStatus;
import net.minecraft.entity.ai.brain.memory.MemoryModuleType;
import net.minecraft.entity.ai.brain.task.BeginRaidTask;
import net.minecraft.entity.ai.brain.task.CongregateTask;
import net.minecraft.entity.ai.brain.task.DummyTask;
import net.minecraft.entity.ai.brain.task.ExpirePOITask;
import net.minecraft.entity.ai.brain.task.FindInteractionAndLookTargetTask;
import net.minecraft.entity.ai.brain.task.FindWalkTargetTask;
import net.minecraft.entity.ai.brain.task.FirstShuffledTask;
import net.minecraft.entity.ai.brain.task.GatherPOITask;
import net.minecraft.entity.ai.brain.task.HideFromRaidOnBellRingTask;
import net.minecraft.entity.ai.brain.task.InteractWithDoorTask;
import net.minecraft.entity.ai.brain.task.InteractWithEntityTask;
import net.minecraft.entity.ai.brain.task.JumpOnBedTask;
import net.minecraft.entity.ai.brain.task.LookAtEntityTask;
import net.minecraft.entity.ai.brain.task.LookTask;
import net.minecraft.entity.ai.brain.task.RunAwayTask;
import net.minecraft.entity.ai.brain.task.SwimTask;
import net.minecraft.entity.ai.brain.task.Task;
import net.minecraft.entity.ai.brain.task.UpdateActivityTask;
import net.minecraft.entity.ai.brain.task.WakeUpTask;
import net.minecraft.entity.ai.brain.task.WalkRandomlyTask;
import net.minecraft.entity.ai.brain.task.WalkToHouseTask;
import net.minecraft.entity.ai.brain.task.WalkToTargetTask;
import net.minecraft.entity.ai.brain.task.WalkToVillagerBabiesTask;
import net.minecraft.entity.ai.brain.task.WalkTowardsLookTargetTask;
import net.minecraft.entity.ai.brain.task.WorkTask;
import net.minecraft.village.PointOfInterestType;

public class FarlanderTasks
{
	public static ImmutableList<Pair<Integer, ? extends Task<? super FarlanderEntity>>> core(float speed)
	{
		// 0.4F
		return ImmutableList.of(Pair.of(0, new SwimTask(0.8F)), Pair.of(0, new InteractWithDoorTask()), Pair.of(0, new LookTask(45, 90)), Pair.of(0, new FarlanderPanicTask()), Pair.of(0, new WakeUpTask()), Pair.of(0, new HideFromRaidOnBellRingTask()), Pair.of(0, new BeginRaidTask()), Pair.of(1, new WalkToTargetTask()), Pair.of(2, new FarlanderTradeTask(speed)), Pair.of(10, new GatherPOITask(PointOfInterestType.HOME, MemoryModuleType.HOME, false, Optional.of((byte) 14))), Pair.of(10, new GatherPOITask(PointOfInterestType.MEETING, MemoryModuleType.MEETING_POINT, true, Optional.of((byte) 14))));
	}

	public static ImmutableList<Pair<Integer, ? extends Task<? super FarlanderEntity>>> work(float speed)
	{
		return ImmutableList.of(lookAtPlayersOrVillagers(), Pair.of(10, new FindInteractionAndLookTargetTask(EntityType.PLAYER, 4)), Pair.of(99, new UpdateActivityTask()));
	}

	public static ImmutableList<Pair<Integer, ? extends Task<? super FarlanderEntity>>> play(float speed)
	{
		return ImmutableList.of(Pair.of(0, new WalkToTargetTask(80, 120)), lookAtMany(), Pair.of(5, new WalkToVillagerBabiesTask()), Pair.of(5, new FirstShuffledTask<>(ImmutableMap.of(MemoryModuleType.VISIBLE_VILLAGER_BABIES, MemoryModuleStatus.VALUE_ABSENT), ImmutableList.of(Pair.of(InteractWithEntityTask.func_220445_a(FarlandersEntityTypes.FARLANDER, 8, MemoryModuleType.INTERACTION_TARGET, speed, 2), 2), Pair.of(InteractWithEntityTask.func_220445_a(EntityType.CAT, 8, MemoryModuleType.INTERACTION_TARGET, speed, 2), 1), Pair.of(new FindWalkTargetTask(speed), 1), Pair.of(new WalkTowardsLookTargetTask(speed, 2), 1), Pair.of(new JumpOnBedTask(speed), 2), Pair.of(new DummyTask(20, 40), 2)))), Pair.of(99, new UpdateActivityTask()));
	}

	public static ImmutableList<Pair<Integer, ? extends Task<? super FarlanderEntity>>> meet(float speed)
	{
		return ImmutableList.of(Pair.of(2, new FirstShuffledTask<>(ImmutableList.of(Pair.of(new WorkTask(MemoryModuleType.MEETING_POINT, 0.4F, 40), 2), Pair.of(new CongregateTask(), 2)))), /*Pair.of(10, new PigmanShowWaresTask(400, 1600)),*/ Pair.of(10, new FindInteractionAndLookTargetTask(EntityType.PLAYER, 4)), Pair.of(2, new FarlanderStayNearPointTask(MemoryModuleType.MEETING_POINT, speed, 6, 100, 200)), Pair.of(3, new ExpirePOITask(PointOfInterestType.MEETING, MemoryModuleType.MEETING_POINT)), lookAtMany(), Pair.of(99, new UpdateActivityTask()));
	}

	public static ImmutableList<Pair<Integer, ? extends Task<? super FarlanderEntity>>> rest(float speed) // TODO
	{
		return ImmutableList.of(Pair.of(2, new FarlanderStayNearPointTask(MemoryModuleType.HOME, speed, 1, 150, 1200)), Pair.of(3, new ExpirePOITask(PointOfInterestType.HOME, MemoryModuleType.HOME)), /*Pair.of(3, new SleepAtHomeTask()), */Pair.of(5, new FirstShuffledTask<>(ImmutableMap.of(MemoryModuleType.HOME, MemoryModuleStatus.VALUE_ABSENT), ImmutableList.of(Pair.of(new WalkToHouseTask(speed), 1), Pair.of(new WalkRandomlyTask(speed), 4), Pair.of(new FarlanderWalkToPOITask(speed, 4), 2), Pair.of(new DummyTask(20, 40), 2)))), lookAtPlayersOrVillagers(), Pair.of(99, new UpdateActivityTask()));
	}

	public static ImmutableList<Pair<Integer, ? extends Task<? super FarlanderEntity>>> idle(float speed)
	{
		return ImmutableList.of(Pair.of(2, new FirstShuffledTask<>(ImmutableList.of(Pair.of(InteractWithEntityTask.func_220445_a(FarlandersEntityTypes.FARLANDER, 8, MemoryModuleType.INTERACTION_TARGET, speed, 2), 2), /*Pair.of(new InteractWithEntityTask<>(FarlandersEntityTypes.FARLANDER, 8, FarlanderEntity::canBreed, FarlanderEntity::canBreed, MemoryModuleType.BREED_TARGET, speed, 2), 1),*/ Pair.of(InteractWithEntityTask.func_220445_a(EntityType.CAT, 8, MemoryModuleType.INTERACTION_TARGET, speed, 2), 1), Pair.of(new FindWalkTargetTask(speed), 1), Pair.of(new WalkTowardsLookTargetTask(speed, 2), 1), Pair.of(new JumpOnBedTask(speed), 1), Pair.of(new DummyTask(30, 60), 1)))), Pair.of(3, new FindInteractionAndLookTargetTask(EntityType.PLAYER, 4)), lookAtMany(), Pair.of(99, new UpdateActivityTask()));
	}

	public static ImmutableList<Pair<Integer, ? extends Task<? super FarlanderEntity>>> panic(float speed) // TODO
	{
		float f = speed * 1.5F;
		return ImmutableList.of(Pair.of(0, new FarlanderClearHurtTask()), Pair.of(1, RunAwayTask.func_233965_b_(MemoryModuleType.NEAREST_HOSTILE, f, 6, false)), Pair.of(1, RunAwayTask.func_233965_b_(MemoryModuleType.HURT_BY_ENTITY, f, 6, false)), Pair.of(3, new FindWalkTargetTask(f, 2, 2)), lookAtPlayersOrVillagers());
	}

	private static Pair<Integer, Task<FarlanderEntity>> lookAtMany()
	{
		return Pair.of(5, new FirstShuffledTask<>(ImmutableList.of(Pair.of(new LookAtEntityTask(FarlandersEntityTypes.ELDER_FARLANDER, 8.0F), 8), Pair.of(new LookAtEntityTask(FarlandersEntityTypes.ENDER_GUARDIAN, 8.0F), 8), Pair.of(new LookAtEntityTask(FarlandersEntityTypes.ENDER_GOLEM, 8.0F), 8), Pair.of(new LookAtEntityTask(FarlandersEntityTypes.FARLANDER, 8.0F), 2), Pair.of(new LookAtEntityTask(EntityType.PLAYER, 8.0F), 2), Pair.of(new LookAtEntityTask(EntityClassification.CREATURE, 8.0F), 1), Pair.of(new LookAtEntityTask(EntityClassification.WATER_CREATURE, 8.0F), 1), Pair.of(new LookAtEntityTask(EntityClassification.MONSTER, 8.0F), 1), Pair.of(new DummyTask(30, 60), 2))));
	}

	private static Pair<Integer, Task<FarlanderEntity>> lookAtPlayersOrVillagers()
	{
		return Pair.of(5, new FirstShuffledTask<>(ImmutableList.of(Pair.of(new LookAtEntityTask(FarlandersEntityTypes.FARLANDER, 8.0F), 2), Pair.of(new LookAtEntityTask(EntityType.VILLAGER, 8.0F), 2), Pair.of(new LookAtEntityTask(EntityType.PLAYER, 8.0F), 2), Pair.of(new DummyTask(30, 60), 8))));
	}
}