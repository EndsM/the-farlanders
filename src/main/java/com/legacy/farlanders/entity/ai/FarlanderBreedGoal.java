package com.legacy.farlanders.entity.ai;

import java.util.EnumSet;
import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.entity.util.BreedableVillagerEntity;

import net.minecraft.entity.EntityPredicate;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class FarlanderBreedGoal extends Goal
{
	private static final EntityPredicate field_220689_d = (new EntityPredicate()).setDistance(8.0D).allowInvulnerable().allowFriendlyFire().setLineOfSiteRequired();
	protected final BreedableVillagerEntity villager;
	private final Class<? extends BreedableVillagerEntity> mateClass;
	protected final World world;
	protected BreedableVillagerEntity targetMate;
	private int spawnBabyDelay;
	private final double moveSpeed;

	public FarlanderBreedGoal(BreedableVillagerEntity villager, double speedIn)
	{
		this(villager, speedIn, villager.getClass());
	}

	public FarlanderBreedGoal(BreedableVillagerEntity villager, double moveSpeed, Class<? extends BreedableVillagerEntity> mateClass)
	{
		this.villager = villager;
		this.world = villager.world;
		this.mateClass = mateClass;
		this.moveSpeed = moveSpeed;
		this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
	}

	@Override
	public boolean shouldExecute()
	{
		if (!this.villager.isInLove())
			return false;

		this.targetMate = this.getNearbyMate();
		return this.targetMate != null;
	}

	@Override
	public boolean shouldContinueExecuting()
	{
		return this.targetMate.isAlive() && this.targetMate.isInLove() && this.spawnBabyDelay < 60;
	}

	@Override
	public void resetTask()
	{
		this.targetMate = null;
		this.spawnBabyDelay = 0;
	}

	@Override
	public void tick()
	{
		this.villager.getLookController().setLookPositionWithEntity(this.targetMate, 10.0F, (float) this.villager.getVerticalFaceSpeed());
		this.villager.getNavigator().tryMoveToEntityLiving(this.targetMate, this.moveSpeed);
		++this.spawnBabyDelay;

		if (this.spawnBabyDelay >= 60 && this.villager.getDistanceSq(this.targetMate) < 9.0D)
			this.spawnBaby();
	}

	@Nullable
	private BreedableVillagerEntity getNearbyMate()
	{
		List<BreedableVillagerEntity> list = this.world.getTargettableEntitiesWithinAABB(this.mateClass, field_220689_d, this.villager, this.villager.getBoundingBox().grow(8.0D));
		double d0 = Double.MAX_VALUE;
		BreedableVillagerEntity villagerentity = null;

		for (BreedableVillagerEntity villagerentity1 : list)
		{
			if (this.villager.canMateWith(villagerentity1) && this.villager.getDistanceSq(villagerentity1) < d0)
			{
				villagerentity = villagerentity1;
				d0 = this.villager.getDistanceSq(villagerentity1);
			}
		}

		return villagerentity;
	}

	protected void spawnBaby()
	{
		this.villager.func_234177_a_((ServerWorld) this.world, this.targetMate);
	}
}
