package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.util.BaseEndermanEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.registry.FarlandersItems;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;

public class MysticEndermanEntity extends BaseEndermanEntity implements IColoredEyes
{
	public static final DataParameter<Integer> EYE_COLOR = EntityDataManager.<Integer>createKey(MysticEndermanEntity.class, DataSerializers.VARINT);

	private int power;
	private boolean isTimerSet;

	public MysticEndermanEntity(EntityType<? extends MysticEndermanEntity> p_i50210_1_, World p_i50210_2_)
	{
		super(p_i50210_1_, p_i50210_2_);
		this.stepHeight = 1.0F;

		this.setPathPriority(PathNodeType.WATER, -1.0F);
	}

	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(FarlandersItems.mystic_wand_fire_small));

		if (world.isRemote)
		{
			this.power = rand.nextInt(6);
		}

		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(EYE_COLOR, Integer.valueOf(this.rand.nextInt(2)));
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);

		compound.putInt("EyeColor", this.getEyeColor());
	}

	@Override
	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);

		this.setEyeColor(compound.getInt("EyeColor"));
	}

	@Override
	public void setEyeColor(int colorID)
	{
		this.dataManager.set(EYE_COLOR, colorID);
	}

	@Override
	public int getEyeColor()
	{
		return this.dataManager.get(EYE_COLOR);
	}

	@Override
	public void spawnParticles()
	{
		if (this.getEyeColor() == 1)
		{
			this.world.addParticle(ParticleTypes.LARGE_SMOKE, this.getPosX() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.getPosY() + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.getPosZ() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
		}
		else
		{
			for (int i = 0; i < 2; ++i)
			{
				this.world.addParticle(ParticleTypes.PORTAL, this.getPosX() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.getPosY() + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.getPosZ() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), (this.rand.nextDouble() - 0.5D) * 2.0D, -this.rand.nextDouble(), (this.rand.nextDouble() - 0.5D) * 2.0D);
			}
		}
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return this.isScreaming() ? SoundEvents.ENTITY_ENDERMAN_SCREAM : FarlandersSounds.ENTITY_MYSTIC_ENDERMAN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FarlandersSounds.ENTITY_MYSTIC_ENDERMAN_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_MYSTIC_ENDERMAN_DEATH;
	}

	@Override
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{

		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		if (source.getImmediateSource() instanceof PlayerEntity && !source.isCreativePlayer())
		{
			PlayerEntity aux = (PlayerEntity) source.getTrueSource();

			if (power == 0)
			{
				if (!world.isRemote)
				{
					((LivingEntity) aux).addPotionEffect(new EffectInstance(Effects.BLINDNESS, 3 * 20));
				}

				this.playSound(FarlandersSounds.ENTITY_MYSTIC_ENDERMAN_BLINDNESS, 1.0F, 1.0F);
			}
			else if (power == 1)
			{
				if (!world.isRemote)
				{
					((LivingEntity) aux).addPotionEffect(new EffectInstance(Effects.NAUSEA, 8 * 20));
				}

				this.playSound(FarlandersSounds.ENTITY_MYSTIC_ENDERMAN_CONFUSION, 1.0F, 1.0F);
			}
			else if (power == 2)
			{
				if (!world.isRemote)
				{
					((LivingEntity) aux).addPotionEffect(new EffectInstance(Effects.WEAKNESS, 9 * 20));
					((LivingEntity) aux).addPotionEffect(new EffectInstance(Effects.SLOWNESS, 9 * 20));
				}

				this.playSound(SoundEvents.ENTITY_DROWNED_HURT, 1.0F, 1.0F);
			}
			else if (power == 3 && isTimerSet == false && rand.nextInt(3) == 1)
			{
				if (!world.isRemote)
				{
					int power3x = (int) aux.getPosX() + (rand.nextInt(5));
					int power3y = (int) aux.getPosY() - 1;
					int power3z = (int) aux.getPosZ() + (rand.nextInt(5));
					int randomNr = rand.nextInt(18);
					BlockState block;
					if (randomNr == 0)
						block = Blocks.DIAMOND_ORE.getDefaultState();
					else if (randomNr >= 1 && randomNr <= 2)
						block = Blocks.GOLD_ORE.getDefaultState();
					else if (randomNr >= 3 && randomNr <= 4)
						block = Blocks.LAPIS_ORE.getDefaultState();
					else if (randomNr >= 5 && randomNr <= 7)
						block = Blocks.REDSTONE_ORE.getDefaultState();
					else if (randomNr >= 8 && randomNr <= 11)
						block = Blocks.IRON_ORE.getDefaultState();
					else if (randomNr == 12)
						block = Blocks.EMERALD_ORE.getDefaultState();
					else if (randomNr == 13)
						block = Blocks.ACACIA_PLANKS.getDefaultState();
					else
						block = Blocks.COAL_ORE.getDefaultState();
					this.world.setBlockState(new BlockPos(power3x, power3y, power3z), block);
					isTimerSet = true;
					System.currentTimeMillis();
				}

				this.playSound(SoundEvents.AMBIENT_CAVE, 1.0F, 1.0F);
			}
			else if (power == 4 && rand.nextInt(3) == 1)
			{
				if (!this.world.isRemote)
				{
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 5, (int) aux.getPosZ())) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 5, (int) aux.getPosZ()), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 6, (int) aux.getPosZ())) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 6, (int) aux.getPosZ()), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 5, (int) aux.getPosZ() + 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 5, (int) aux.getPosZ() + 1), Blocks.SAND.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 6, (int) aux.getPosZ() + 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 6, (int) aux.getPosZ() + 1), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 5, (int) aux.getPosZ() - 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 5, (int) aux.getPosZ() - 1), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 6, (int) aux.getPosZ() - 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX(), (int) aux.getPosY() + 6, (int) aux.getPosZ() - 1), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 5, (int) aux.getPosZ())) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 5, (int) aux.getPosZ()), Blocks.SAND.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 6, (int) aux.getPosZ())) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 6, (int) aux.getPosZ()), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 5, (int) aux.getPosZ())) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 5, (int) aux.getPosZ()), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 6, (int) aux.getPosZ())) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 6, (int) aux.getPosZ()), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 5, (int) aux.getPosZ() + 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 5, (int) aux.getPosZ() + 1), Blocks.SAND.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 6, (int) aux.getPosZ() + 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 6, (int) aux.getPosZ() + 1), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 5, (int) aux.getPosZ() - 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 5, (int) aux.getPosZ() - 1), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 6, (int) aux.getPosZ() - 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() - 1, (int) aux.getPosY() + 6, (int) aux.getPosZ() - 1), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 5, (int) aux.getPosZ() - 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 5, (int) aux.getPosZ() - 1), Blocks.SAND.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 6, (int) aux.getPosZ() - 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 6, (int) aux.getPosZ() - 1), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 5, (int) aux.getPosZ() + 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 5, (int) aux.getPosZ() + 1), Blocks.GRAVEL.getDefaultState(), 2);
					if (this.world.getBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 6, (int) aux.getPosZ() + 1)) == Blocks.AIR.getDefaultState())
						this.world.setBlockState(new BlockPos((int) aux.getPosX() + 1, (int) aux.getPosY() + 6, (int) aux.getPosZ() + 1), Blocks.SAND.getDefaultState(), 2);
				}
				this.playSound(SoundEvents.ENTITY_ZOMBIE_VILLAGER_CURE, 1.0F, 1.0F);
			}
			else if (power == 5 && rand.nextInt(3) == 1)
			{
				if (!world.isRemote)
				{
					this.addPotionEffect(new EffectInstance(Effects.REGENERATION, 5 * 20));
					this.addPotionEffect(new EffectInstance(Effects.HEALTH_BOOST, 2));
				}

				this.playSound(SoundEvents.ENTITY_ZOMBIE_INFECT, 1.0F, 1.0F);
			}

			power = rand.nextInt(6);

			return super.attackEntityFrom(source, amount);
		}
		else if (!(source instanceof IndirectEntityDamageSource))
		{
			boolean flag = super.attackEntityFrom(source, amount);
			if (source.isUnblockable() && this.rand.nextInt(10) != 0)
			{
				this.teleportRandomly();
			}
			return flag;
		}

		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleportRandomly())
				{
					return true;
				}
			}
			return false;
		}
	}
}