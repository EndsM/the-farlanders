package com.legacy.farlanders.entity.hostile.nightfall;

import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EndSpiritEntity extends CreatureEntity
{
	public static final DataParameter<Integer> INVIS_VALUE = EntityDataManager.<Integer>createKey(EndSpiritEntity.class, DataSerializers.VARINT);
	public static final DataParameter<Boolean> CAN_VANISH = EntityDataManager.<Boolean>createKey(EndSpiritEntity.class, DataSerializers.BOOLEAN);

	public int deathTicks;
	public int invisTick = 300;

	public EndSpiritEntity(EntityType<? extends EndSpiritEntity> type, World world)
	{
		super(type, world);
		this.setPathPriority(PathNodeType.WATER, -1.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.8D));
		this.goalSelector.addGoal(6, new LookAtGoal(this, LivingEntity.class, 10.0F, 9999.0F));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
	}

	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 1.87F;
	}

	/*protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(1.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3D);
	}*/

	@Override
	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(INVIS_VALUE, 200);
		this.dataManager.register(CAN_VANISH, this.rand.nextBoolean());
	}

	@Override
	public void tick()
	{
		super.tick();
		this.remove();
		if (!this.onGround && this.getMotion().y < 0.0D)
		{
			this.setMotion(this.getMotion().mul(1.0D, 0.7D, 1.0D));
		}

		List<PlayerEntity> list = this.world.<PlayerEntity>getEntitiesWithinAABB(PlayerEntity.class, this.getBoundingBox().grow(5.0D, 3.0D, 5.0D));
		if (!list.isEmpty() && this.dataManager.get(CAN_VANISH) && this.ticksExisted > 50)
		{
			this.setHealth(0);
		}
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		this.setHealth(0);
		return false;
	}

	@Override
	protected void onDeathUpdate()
	{
		++this.deathTicks;

		for (int k = 0; k < 4; k++)
		{
			this.setInvisValue(this.getInvisValue() - 1);
		}

		if (this.deathTicks > 55)
		{
			this.remove();
		}
	}

	public void setInvisValue(int value)
	{
		this.dataManager.set(INVIS_VALUE, value);
	}

	public int getInvisValue()
	{
		return this.dataManager.get(INVIS_VALUE);
	}

	public void fall(float distance, float damageMultiplier)
	{
	}

	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
	}

	@Override
	public boolean canBeCollidedWith()
	{
		return super.canBeCollidedWith();
	}

	public void applyEntityCollision(Entity entityIn)
	{
	}

	public boolean canBePushed()
	{
		return false;
	}

	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return null;
	}

	protected SoundEvent getDeathSound()
	{
		return null;
	}

	public void onKillCommand()
	{
		this.remove();
	}
}