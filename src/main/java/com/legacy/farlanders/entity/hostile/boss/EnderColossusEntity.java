package com.legacy.farlanders.entity.hostile.boss;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.hostile.boss.summon.EnderColossusShadowEntity;
import com.legacy.farlanders.entity.hostile.boss.summon.MiniDragonEntity;
import com.legacy.farlanders.entity.hostile.boss.summon.ThrownBlockEntity;
import com.legacy.farlanders.registry.FarlandersEntityTypes;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.RangedAttackGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.BossInfo;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.GameRules;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.server.ServerBossInfo;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class EnderColossusEntity extends MonsterEntity implements IRangedAttackMob
{
	protected static final DataParameter<Boolean> NIGHTFALL_SPAWNED = EntityDataManager.createKey(EnderColossusEntity.class, DataSerializers.BOOLEAN);
	public static final DataParameter<Boolean> ANGRY = EntityDataManager.<Boolean>createKey(EnderColossusEntity.class, DataSerializers.BOOLEAN);
	public static final DataParameter<Integer> SHADOW_COUNTER = EntityDataManager.<Integer>createKey(EnderColossusEntity.class, DataSerializers.VARINT);
	public static final DataParameter<Integer> DRAGON_COUNTER = EntityDataManager.<Integer>createKey(EnderColossusEntity.class, DataSerializers.VARINT);

	private final ServerBossInfo bossInfo = (ServerBossInfo) (new ServerBossInfo(this.getDisplayName(), BossInfo.Color.WHITE, BossInfo.Overlay.NOTCHED_6)).setDarkenSky(false).setPlayEndBossMusic(true).setCreateFog(true);

	public int deathTicks;
	private int attackTimer, screamTimer;
	public boolean angry;

	public EnderColossusEntity(EntityType<? extends EnderColossusEntity> type, World world)
	{
		super(type, world);
		this.ignoreFrustumCheck = true;

		/*if (this.getHealthPercentage() > 75)
		{
			this.setShadowCounter(2);
			this.setDragonCounter(1);
		}
		else if (this.getHealthPercentage() > 50)
		{
			this.setShadowCounter(1);
			this.setDragonCounter(1);
		}
		else if (this.getHealthPercentage() > 25)
		{
			this.setShadowCounter(1);
			this.setDragonCounter(0);
		}
		else
		{
			this.setShadowCounter(0);
			this.setDragonCounter(0);
		}*/
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(3, new RangedAttackGoal(this, 0.6D, 30, 40.0F)
		{
			public boolean shouldContinueExecuting()
			{
				MobEntity entity = ObfuscationReflectionHelper.getPrivateValue(RangedAttackGoal.class, this, "field_75322_b");

				if (entity.getAttackTarget() != null)
				{
					return entity.getDistance(entity.getAttackTarget()) > 15.0F && super.shouldContinueExecuting();
				}
				else
				{
					return super.shouldExecute();
				}
			}

			public boolean shouldExecute()
			{
				MobEntity entity = ObfuscationReflectionHelper.getPrivateValue(RangedAttackGoal.class, this, "field_75322_b");

				if (entity.getAttackTarget() != null)
				{
					return entity.getDistance(entity.getAttackTarget()) > 15.0F && super.shouldExecute();
				}
				else
				{
					return super.shouldExecute();
				}
			}
		});
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.6D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtGoal(this, PlayerEntity.class, 40.0F));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, false));
	}

	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 19.00F;
	}

	/*protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(40.0D);
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(300.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.5D);
		this.getAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).setBaseValue(2.0D);
	}*/
	@Override
	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(NIGHTFALL_SPAWNED, false);
		this.dataManager.register(ANGRY, false);

		this.dataManager.register(SHADOW_COUNTER, 2);
		this.dataManager.register(DRAGON_COUNTER, 1);
	}

	@Override
	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);

		if (this.hasCustomName())
		{
			this.bossInfo.setName(this.getDisplayName());
		}

		this.setNightfallSpawned(compound.getBoolean("NightfallSpawned"));
		this.setAngry(compound.getBoolean("IsAngry"));

		this.setShadowCounter(compound.getInt("ShadowCounter"));
		this.setDragonCounter(compound.getInt("DragonCounter"));
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putBoolean("NightfallSpawned", this.getNightfallSpawned());
		compound.putBoolean("IsAngry", this.getAngry());

		compound.putInt("ShadowCounter", this.getShadowCounter());
		compound.putInt("DragonCounter", this.getDragonCounter());
	}

	public void setCustomName(@Nullable ITextComponent name)
	{
		super.setCustomName(name);
		this.bossInfo.setName(this.getDisplayName());
	}

	public void addTrackingPlayer(ServerPlayerEntity player)
	{
		super.addTrackingPlayer(player);
		this.bossInfo.addPlayer(player);
	}

	public void removeTrackingPlayer(ServerPlayerEntity player)
	{
		super.removeTrackingPlayer(player);
		this.bossInfo.removePlayer(player);
	}

	public void setNightfallSpawned(boolean natural)
	{
		this.dataManager.set(NIGHTFALL_SPAWNED, natural);
	}

	public boolean getNightfallSpawned()
	{
		return this.dataManager.get(NIGHTFALL_SPAWNED);
	}

	private double getHealthPercentage()
	{
		return (this.getHealth() * 100) / this.getMaxHealth();
	}

	public boolean attackEntityAsMob(Entity entityIn)
	{
		this.attackTimer = 10;
		this.world.setEntityState(this, (byte) 4);
		boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), 10.0F);
		if (flag)
		{
			double d0 = (this.getBoundingBox().minX + this.getBoundingBox().maxX) / 2.0D;
			double d1 = (this.getBoundingBox().minZ + this.getBoundingBox().maxZ) / 2.0D;
			double d2 = entityIn.getPosX() - d0;
			double d3 = entityIn.getPosZ() - d1;
			double d4 = d2 * d2 + d3 * d3;
			entityIn.setMotion(entityIn.getMotion().add(d2 / d4 * 1.1D, 0.50000000298023224D, d3 / d4 * 10.1D));
			this.applyEnchantments(this, entityIn);
		}
		this.playSound(SoundEvents.ENTITY_IRON_GOLEM_ATTACK, 2.0F, 0.5F);

		return flag;
	}

	@OnlyIn(Dist.CLIENT)
	public void handleStatusUpdate(byte id)
	{
		if (id == 4)
		{
			this.attackTimer = 10;
			this.playSound(SoundEvents.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		}
		if (id == 5)
		{
			this.screamTimer = 100;
			this.playSound(FarlandersSounds.ENTITY_MYSTIC_ENDERMINION_DEATH, 5.0F, 0.1F);
		}
		else
		{
			super.handleStatusUpdate(id);
		}
	}

	@OnlyIn(Dist.CLIENT)
	public int getAttackTimer()
	{
		return this.attackTimer;
	}

	@OnlyIn(Dist.CLIENT)
	public int getScreamTimer()
	{
		return this.screamTimer;
	}

	public void setAttackTarget(@Nullable LivingEntity entitylivingbaseIn)
	{
		super.setAttackTarget(entitylivingbaseIn);

		if (entitylivingbaseIn != null)
		{
			this.setAngry(true);
		}
		else
		{
			this.setAngry(false);
		}
	}

	@Override
	public void tick()
	{
		super.tick();
		this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());

		if (this.attackTimer > 0)
		{
			--this.attackTimer;
		}

		if (this.screamTimer > 0)
		{
			--this.screamTimer;
		}

		if (this.getHealthPercentage() < 25)
		{
			if (rand.nextInt(400) == 0 && !this.world.isRemote && this.onGround && this.getScreamTimer() == 0 && this.getAttackTarget() != null)
			{
				this.screamTimer = 100;
				this.playSound(FarlandersSounds.ENTITY_MYSTIC_ENDERMINION_DEATH, 5.0F, 0.1F);
				this.world.setEntityState(this, (byte) 5);

				List<PlayerEntity> list = this.world.<PlayerEntity>getEntitiesWithinAABB(PlayerEntity.class, this.getBoundingBox().grow(50.0D, 100.0D, 50.0D));
				if (!list.isEmpty())
				{
					for (int k = 0; k < list.size(); k++)
					{
						PlayerEntity entity = list.get(k);

						if (!entity.isCreative())
						{
							entity.addPotionEffect(new EffectInstance(Effects.BLINDNESS, 100, 1));
							entity.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 50, 2));
						}
					}
				}
			}
			return;
		}

		if (this.getHealthPercentage() < 75 && this.getHealthPercentage() > 25)
		{
			if (rand.nextInt(400) == 0 && !this.world.isRemote && this.onGround && this.getAttackTarget() != null && this.getScreamTimer() == 0)
			{
				this.playSound(FarlandersSounds.ENTITY_ENDER_GOLEM_DEATH, 5.0F, 0.5F);
				this.setMotion(this.getMotion().add(0, 1.70000000149011612D, 0));
			}
		}

		if (this.getHealthPercentage() < 25 && this.getShadowCounter() == 1)
		{
			this.setShadowCounter(this.getShadowCounter() - 1);
			for (int i = 0; i < 3; i++)
			{
				BlockPos offsetPos = this.world.getHeight(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, this.getPosition().add(rand.nextInt(16) - 8, 0, rand.nextInt(16) - 8));
				EnderColossusShadowEntity ent = new EnderColossusShadowEntity(FarlandersEntityTypes.ENDER_COLOSSUS_SHADOW, world);
				ent.setLocationAndAngles(offsetPos.getX(), offsetPos.getY(), offsetPos.getZ(), 0F, 0F);
				ent.spawnExplosionParticle();
				this.playSound(SoundEvents.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 3.0F, 0.8F);

				if (!world.isRemote)
					world.addEntity(ent);
			}
		}
		else if (this.getHealthPercentage() < 50 && this.getDragonCounter() == 1)
		{
			this.setDragonCounter(this.getDragonCounter() - 1);
			MiniDragonEntity ent = new MiniDragonEntity(FarlandersEntityTypes.MINI_ENDER_DRAGON, world);
			ent.setLocationAndAngles(this.getPosX() + rand.nextInt(32) - 16, this.getPosY() + this.getEyeHeight() + rand.nextInt(30), this.getPosZ() + rand.nextInt(32) - 16, 0F, 0F);
			this.playSound(SoundEvents.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 3.0F, 0.8F);

			if (!world.isRemote)
				world.addEntity(ent);
		}
		else if (this.getHealthPercentage() < 75 && this.getShadowCounter() == 2)
		{
			this.setShadowCounter(this.getShadowCounter() - 1);
			for (int i = 0; i < 2; i++)
			{
				EnderColossusShadowEntity ent = new EnderColossusShadowEntity(FarlandersEntityTypes.ENDER_COLOSSUS_SHADOW, world);
				BlockPos offsetPos = this.world.getHeight(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, this.getPosition().add(rand.nextInt(16) - 8, 0, rand.nextInt(16) - 8));
				ent.setLocationAndAngles(offsetPos.getX(), offsetPos.getY(), offsetPos.getZ(), 0F, 0F);
				ent.spawnExplosionParticle();
				this.playSound(SoundEvents.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 3.0F, 0.8F);

				if (!world.isRemote)
					world.addEntity(ent);
			}
		}

		if (this.getAttackTarget() != null && this.getHealth() > 0)
		{
			if (this.getDistance(this.getAttackTarget()) >= 20.0F)
			{
				this.getMoveHelper().setMoveTo(this.getAttackTarget().getPosX(), this.getAttackTarget().getPosY(), this.getAttackTarget().getPosZ(), 1.0F);
			}

			if (this.getDistance(this.getAttackTarget()) >= 30.0F)
			{
				this.setPosition(this.getAttackTarget().getPosX(), this.getAttackTarget().getPosY() + 25.0F, this.getAttackTarget().getPosZ());
				this.playSound(FarlandersSounds.ENTITY_ENDER_GOLEM_DEATH, 5.0F, 0.5F);
				this.playSound(SoundEvents.ENTITY_ENDERMAN_TELEPORT, 5.0F, 0.5F);
				this.playSound(SoundEvents.ENTITY_ENDERMAN_TELEPORT, 5.0F, 0.7F);
			}
		}

	}

	@Override
	protected void onDeathUpdate()
	{
		this.setNoGravity(true);
		++this.deathTicks;

		if (this.deathTicks >= 180 && this.deathTicks <= 200)
		{
			float f = (this.rand.nextFloat() - 0.5F) * 8.0F;
			float f1 = (this.rand.nextFloat() - 0.5F) * 20.0F;
			float f2 = (this.rand.nextFloat() - 0.5F) * 8.0F;
			this.world.addParticle(ParticleTypes.EXPLOSION_EMITTER, this.getPosX() + (double) f, this.getPosY() + 13.0F + (double) f1, this.getPosZ() + (double) f2, 0.0D, 0.0D, 0.0D);
		}

		boolean flag = this.world.getGameRules().getBoolean(GameRules.DO_MOB_LOOT);
		int i = this.getNightfallSpawned() ? 12000 : 500;

		if (!this.world.isRemote)
		{
			if (this.deathTicks > 150 && this.deathTicks % 5 == 0 && flag)
			{
				this.dropExperience(MathHelper.floor((float) i * 0.08F));
			}

			if (this.deathTicks == 1)
			{
				this.setMotion(Vector3d.ZERO);
				this.world.playBroadcastSound(1028, new BlockPos(this.getPositionVec()), 0);
			}
		}

		this.move(MoverType.SELF, new Vector3d(0.0D, (double) 0.2F, 0.0D));
		this.renderYawOffset += 0.0F;
		this.rotationYawHead += 0.0F;
		this.rotationPitch += 0.0F;

		if (this.deathTicks == 197 && !this.world.isRemote)
		{
			if (flag)
			{
				this.dropExperience(MathHelper.floor((float) i * 0.2F));
			}

			this.remove();
		}

	}

	private void dropExperience(int amount)
	{
		while (amount > 0)
		{
			int i = ExperienceOrbEntity.getXPSplit(amount);
			amount -= i;
			this.world.addEntity(new ExperienceOrbEntity(this.world, this.getPosX(), this.getPosY(), this.getPosZ(), i));
		}

	}

	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_TITAN_IDLE;
	}

	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		this.playSound(FarlandersSounds.ENTITY_TITAN_HURT, this.getSoundVolume(), 0.1F);
		return null;
	}

	protected SoundEvent getDeathSound()
	{
		this.playSound(FarlandersSounds.ENTITY_TITAN_DEATH_ECHO, 5.0F, 0.1F);
		return null;
	}

	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.ENTITY_IRON_GOLEM_STEP, 2.5F, 0.5F);
		this.playSound(SoundEvents.BLOCK_SNOW_BREAK, 0.5F, 0.1F);
	}

	protected float getSoundPitch()
	{
		return 0.6F;
	}

	@Override
	protected float getSoundVolume()
	{
		return 2.0F;
	}

	public boolean isNonBoss()
	{
		return false;
	}

	public boolean canDespawn(double distanceToClosestPlayer)
	{
		return false;
	}

	public void fall(float distance, float damageMultiplier)
	{
		if (distance >= 10)
		{
			this.world.createExplosion(this, this.getPosX(), this.getPosY(), this.getPosZ(), 3, Mode.NONE);

			List<LivingEntity> list = this.world.<LivingEntity>getEntitiesWithinAABB(LivingEntity.class, this.getBoundingBox().grow(10.0D, 5.0D, 10.0D));
			if (!list.isEmpty())
			{
				for (int k = 0; k < list.size(); k++)
				{
					LivingEntity entity = list.get(k);

					if (!entity.isAirBorne)
						entity.setMotion(entity.getMotion().add(0.0F, 1.0F, 0.0F));
				}
			}
		}
	}

	@Override
	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor)
	{
		this.world.setEntityState(this, (byte) 4);
		Vector3d look = this.getLook(1.0F);
		double xPos = this.getPosition().getX() + look.x * 4.0D;
		double yPos = this.getPosition().getY() + this.getEyeHeight() - 5.0D;
		double zPos = this.getPosition().getZ() + look.z * 4.0D;

		ThrownBlockEntity thrownBlock = new ThrownBlockEntity(this.world, xPos, yPos, zPos, this);

		thrownBlock.fallTime = 200;
		double d0 = target.getPosX() - this.getPosX();
		double d1 = target.getBoundingBox().minY + (double) (target.getHeight() / 3.0F - thrownBlock.getPosY() + 15.0F);
		double d2 = target.getPosZ() - this.getPosZ();

		this.world.playSound((PlayerEntity) null, this.getPosX(), this.getPosY(), this.getPosZ(), SoundEvents.ENTITY_IRON_GOLEM_ATTACK, this.getSoundCategory(), 4.0F, 0.5F + (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);

		float inaccuracy = 1.0F;

		float velocity = this.getDistance(target) / 26;

		Vector3d vec3d = (new Vector3d(d0, d1, d2)).normalize().add(this.rand.nextGaussian() * (double) 0.0075F * (double) inaccuracy, this.rand.nextGaussian() * (double) 0.0075F * (double) inaccuracy, this.rand.nextGaussian() * (double) 0.0075F * (double) inaccuracy).scale((double) velocity);
		thrownBlock.setMotion(vec3d);
		float f1 = MathHelper.sqrt(horizontalMag(vec3d));
		thrownBlock.rotationYaw = (float) (MathHelper.atan2(vec3d.x, d2) * (double) (180F / (float) Math.PI));
		thrownBlock.rotationPitch = (float) (MathHelper.atan2(vec3d.y, (double) f1) * (double) (180F / (float) Math.PI));
		thrownBlock.prevRotationYaw = this.rotationYaw;
		thrownBlock.prevRotationPitch = this.rotationPitch;

		this.world.addEntity(thrownBlock);
	}

	public void setAngry(boolean ang)
	{
		this.dataManager.set(ANGRY, ang);
	}

	public boolean getAngry()
	{
		return this.dataManager.get(ANGRY);
	}

	public void setShadowCounter(int count)
	{
		this.dataManager.set(SHADOW_COUNTER, count);
	}

	public int getShadowCounter()
	{
		return this.dataManager.get(SHADOW_COUNTER);
	}

	public void setDragonCounter(int count)
	{
		this.dataManager.set(DRAGON_COUNTER, count);
	}

	public int getDragonCounter()
	{
		return this.dataManager.get(DRAGON_COUNTER);
	}

	public void onKillCommand()
	{
		this.remove();
	}
}