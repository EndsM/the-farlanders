package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.entity.util.ITeleportingEntity;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.monster.EndermiteEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class TitanEntity extends MonsterEntity implements IColoredEyes, ITeleportingEntity
{
	public static final DataParameter<Integer> EYE_COLOR = EntityDataManager.<Integer>createKey(TitanEntity.class, DataSerializers.VARINT);
	public static final DataParameter<Boolean> HOLDING_PLAYER = EntityDataManager.<Boolean>createKey(TitanEntity.class, DataSerializers.BOOLEAN);
	public static final DataParameter<Boolean> ANGRY = EntityDataManager.<Boolean>createKey(TitanEntity.class, DataSerializers.BOOLEAN);
	public static final DataParameter<Boolean> RAGE = EntityDataManager.<Boolean>createKey(TitanEntity.class, DataSerializers.BOOLEAN);

	private int attackTimer;
	private int holdingChance;
	private long holdingTimerInit;
	private long holdingTimerEnd;
	private LivingEntity entityRidingTitan;
	private int attackNumber;
	private float rageHealth;
	private long rageTimerInit;
	private long rageTimerEnd;

	public TitanEntity(EntityType<? extends TitanEntity> type, World world)
	{
		super(type, world);

		this.stepHeight = 1.0F;
		this.setPathPriority(PathNodeType.WATER, -1.0F);

		rageTimerEnd = 0;
		rageTimerInit = 0;
		holdingChance = rand.nextInt(15);
		attackNumber = -1;

		rageHealth = (this.getMaxHealth() * 25) / 100;
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		// this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.6D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, LooterEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, RebelEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, WandererEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, EndermiteEntity.class, false));
	}

	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		rageHealth = (this.getMaxHealth() * 25) / 100;
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	protected void registerData()
	{
		super.registerData();

		this.dataManager.register(EYE_COLOR, Integer.valueOf(this.rand.nextInt(36)));
		this.dataManager.register(HOLDING_PLAYER, false);
		this.dataManager.register(ANGRY, false);
		this.dataManager.register(RAGE, false);
	}

	@Override
	protected void updateFallState(double y, boolean onGroundIn, BlockState state, BlockPos pos)
	{
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("EyeColor", this.getEyeColor());
		compound.putBoolean("HoldingPlayer", this.getHoldingPlayer());
		compound.putBoolean("IsAngry", this.getAngry());
		compound.putBoolean("InRage", this.inRage());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setEyeColor(compound.getInt("EyeColor"));
		this.setHoldingPlayer(compound.getBoolean("HoldingPlayer"));
		this.setAngry(compound.getBoolean("IsAngry"));
		this.setInRage(compound.getBoolean("InRage"));
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 4.8F;
	}

	/*@Override
	protected void registerAttributes()
	{MooshroomEntity
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(135.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.5D);
		this.getAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).setBaseValue(1.0D);
	}*/

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MonsterEntity.func_234295_eP_().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.5D).createMutableAttribute(Attributes.MAX_HEALTH, 135.0D).createMutableAttribute(Attributes.KNOCKBACK_RESISTANCE, 1.0F);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public void livingTick()
	{
		super.livingTick();

		if (this.isInWaterOrBubbleColumn() == true)
		{
			this.teleportRandomly(this);
		}

		try
		{
			if (this.attackTimer > 0)
			{
				--this.attackTimer;
			}

			if (this.inRage() == true)
			{
				this.world.addParticle(ParticleTypes.CLOUD, this.getPosX() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.getPosY() + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.getPosZ() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
				this.world.addParticle(ParticleTypes.LARGE_SMOKE, this.getPosX() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.getPosY() + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.getPosZ() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
			}

			if (this.getHoldingPlayer() == true && this.getRidingEntity() == null && !this.world.isRemote && entityRidingTitan != null)
			{
				entityRidingTitan.startRiding(this);
			}
			if (this.getHealth() < this.getRageHealth() && this.inRage() == false && !this.world.isRemote)
			{
				this.addPotionEffect(new EffectInstance(Effects.STRENGTH, 8 * 29));
				this.addPotionEffect(new EffectInstance(Effects.RESISTANCE, 8 * 29));
				this.addPotionEffect(new EffectInstance(Effects.SPEED, 8 * 29));
				this.addPotionEffect(new EffectInstance(Effects.REGENERATION, 8 * 29));
				this.addPotionEffect(new EffectInstance(Effects.INSTANT_HEALTH, 5));
				this.playSound(SoundEvents.ENTITY_ZOMBIE_VILLAGER_CURE, 1.0F, 1.0F);
				rageTimerInit = System.currentTimeMillis();
				this.setInRage(true);
			}
			if (this.inRage() == true && !this.world.isRemote)
			{
				rageTimerEnd = System.currentTimeMillis() - rageTimerInit;
				if (rageTimerEnd > 15000)
					this.setInRage(false);
			}
			if (horizontalMag(this.getMotion()) > (double) 2.5000003E-7F && this.rand.nextInt(5) == 0)
			{
				int i = MathHelper.floor(this.getPosX());
				int j = MathHelper.floor(this.getPosY() - (double) 0.2F);
				int k = MathHelper.floor(this.getPosZ());
				BlockPos pos = new BlockPos(i, j, k);
				BlockState blockstate = this.world.getBlockState(pos);

				if (!this.world.isAirBlock(pos))
				{
					this.world.addParticle(new BlockParticleData(ParticleTypes.BLOCK, blockstate), this.getPosX() + ((double) this.rand.nextFloat() - 0.5D) * (double) this.getWidth(), this.getBoundingBox().minY + 0.1D, this.getPosZ() + ((double) this.rand.nextFloat() - 0.5D) * (double) this.getWidth(), 4.0D * ((double) this.rand.nextFloat() - 0.5D), 0.5D, ((double) this.rand.nextFloat() - 0.5D) * 4.0D);
				}
			}

			if (holdingChance == 1 && this.getHoldingPlayer() == true && !this.world.isRemote && entityRidingTitan != null)
			{
				if (entityRidingTitan != null && !entityRidingTitan.isAlive())
				{
					holdingChance = 2;
					this.setHoldingPlayer(false);
					entityRidingTitan.stopRiding();
					this.setAngry(false);
					entityRidingTitan = null;
					attackNumber = -1;
				}
				holdingTimerEnd = System.currentTimeMillis() - holdingTimerInit;
				if (attackNumber == 0 && holdingTimerEnd > 5000)
				{
					holdingChance = 2;
					this.setHoldingPlayer(false);
					entityRidingTitan.stopRiding();
					this.setAngry(false);
					entityRidingTitan = null;
					attackNumber = -1;
				}
				else if (attackNumber == 1 && holdingTimerEnd > 3600)
				{
					holdingChance = 2;
					this.setHoldingPlayer(false);
					entityRidingTitan.stopRiding();
					this.setAngry(false);
					entityRidingTitan = null;
					attackNumber = -1;
				}
				else if (attackNumber == 2 && holdingTimerEnd > 2200)
				{
					holdingChance = 2;
					this.setHoldingPlayer(false);
					entityRidingTitan.stopRiding();
					this.setAngry(false);
					entityRidingTitan = null;
					attackNumber = -1;
				}
			}
			else
			{
				holdingChance = rand.nextInt(15);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean attackEntityAsMob(Entity entityIn)
	{
		this.attackTimer = 10;
		this.world.setEntityState(this, (byte) 4);

		boolean flag;

		if (holdingChance == 1 && this.getHoldingPlayer() == false)
		{
			flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), (float) 0);
			if (!this.world.isRemote)
			{
				this.setHoldingPlayer(true);
				attackNumber = rand.nextInt(3);
				holdingTimerInit = System.currentTimeMillis();
				entityIn.startRiding(this);
				this.setAngry(true);
				if (attackNumber == 0)
				{
					this.world.playSound((PlayerEntity) null, this.getPosition(), FarlandersSounds.ENTITY_TITAN_HOLDING_LONG, SoundCategory.HOSTILE, 1.0F, 0.6F);
					((LivingEntity) entityIn).addPotionEffect(new EffectInstance(Effects.BLINDNESS, 8 * 15));
					((LivingEntity) entityIn).addPotionEffect(new EffectInstance(Effects.NAUSEA, 8 * 22));
					((LivingEntity) entityIn).addPotionEffect(new EffectInstance(Effects.WEAKNESS, 8 * 15));
					((LivingEntity) entityIn).addPotionEffect(new EffectInstance(Effects.WITHER, 8 * 15, 2));
				}
				else if (attackNumber == 1)
				{
					this.world.playSound((PlayerEntity) null, this.getPosition(), FarlandersSounds.ENTITY_TITAN_HOLDING_MED, SoundCategory.HOSTILE, 1.0F, 0.6F);
					((LivingEntity) entityIn).addPotionEffect(new EffectInstance(Effects.NAUSEA, 8 * 18));
					((LivingEntity) entityIn).addPotionEffect(new EffectInstance(Effects.WITHER, 8 * 10, 2));
				}
				else
				{
					this.world.playSound((PlayerEntity) null, this.getPosition(), FarlandersSounds.ENTITY_TITAN_HOLDING_SHORT, SoundCategory.HOSTILE, 1.0F, 0.6F);
					((LivingEntity) entityIn).addPotionEffect(new EffectInstance(Effects.WITHER, 8 * 7, 1));
				}
				entityRidingTitan = (LivingEntity) entityIn;
			}
			else if (this.world.isRemote)
			{
				if (attackNumber == 0)
				{
					this.world.playSound((PlayerEntity) null, this.getPosition(), FarlandersSounds.ENTITY_TITAN_HOLDING_LONG, SoundCategory.HOSTILE, 1.0F, 0.6F);
				}
				else if (attackNumber == 1)
				{
					this.world.playSound((PlayerEntity) null, this.getPosition(), FarlandersSounds.ENTITY_TITAN_HOLDING_MED, SoundCategory.HOSTILE, 1.0F, 0.6F);
				}
				else
				{
					this.world.playSound((PlayerEntity) null, this.getPosition(), FarlandersSounds.ENTITY_TITAN_HOLDING_SHORT, SoundCategory.HOSTILE, 1.0F, 0.6F);
				}
			}
		}
		else if (this.getHoldingPlayer())
			flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), (float) 1);
		else
			flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), (float) 8);
		if (flag)
		{
			entityIn.setMotion(entityIn.getMotion().add(0.0D, (double) 0.4F, 0.0D));
		}

		return flag;
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else if (!(source instanceof IndirectEntityDamageSource))
		{
			boolean flag = super.attackEntityFrom(source, amount);
			if (!this.world.isRemote() && source.isUnblockable() && this.rand.nextInt(10) != 0)
			{
				this.teleportRandomly(this);
			}

			return flag;
		}
		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleportRandomly(this))
				{
					return true;
				}
			}

			return false;
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleStatusUpdate(byte id)
	{
		if (id == 4)
		{
			this.attackTimer = 10;
			this.playSound(SoundEvents.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		}
		else
		{
			super.handleStatusUpdate(id);
		}
	}

	@OnlyIn(Dist.CLIENT)
	public int getAttackTimer()
	{
		return this.attackTimer;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_TITAN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		this.playSound(FarlandersSounds.ENTITY_TITAN_HURT, 1.0F, 0.5F);
		return null;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_TITAN_DEATH_ECHO;
	}

	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.ENTITY_IRON_GOLEM_STEP, 1.0F, 1.0F);
	}

	@Override
	protected float getSoundPitch()
	{
		return 0.8F;
	}

	@Override
	public void setEyeColor(int colorID)
	{
		this.dataManager.set(EYE_COLOR, colorID);
	}

	@Override
	public int getEyeColor()
	{
		return this.dataManager.get(EYE_COLOR);
	}

	public void setInRage(boolean rage)
	{
		this.dataManager.set(RAGE, rage);
	}

	public boolean inRage()
	{
		return this.dataManager.get(RAGE);
	}

	public void setHoldingPlayer(boolean hold)
	{
		this.dataManager.set(HOLDING_PLAYER, hold);
	}

	public boolean getHoldingPlayer()
	{
		return this.dataManager.get(HOLDING_PLAYER);
	}

	public void setAngry(boolean ang)
	{
		this.dataManager.set(ANGRY, ang);
	}

	public boolean getAngry()
	{
		return this.dataManager.get(ANGRY);
	}

	public float getRageHealth()
	{
		return rageHealth;
	}
}