package com.legacy.farlanders.entity.hostile.boss.summon;

import java.util.List;

import com.google.common.collect.Lists;
import com.legacy.farlanders.registry.FarlandersEntityTypes;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.network.IPacket;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;

public class ThrownBlockEntity extends FallingBlockEntity
{
	private LivingEntity thrower;

	public ThrownBlockEntity(EntityType<? extends ThrownBlockEntity> type, World world)
	{
		super(type, world);
	}

	public ThrownBlockEntity(World worldIn, double x, double y, double z, LivingEntity thrower)
	{
		this(FarlandersEntityTypes.THROWN_BLOCK, worldIn);
		this.preventEntitySpawning = true;
		this.setPosition(x, y + (double) ((1.0F - this.getHeight()) / 2.0F), z);
		this.setMotion(Vector3d.ZERO);
		this.prevPosX = x;
		this.prevPosY = y;
		this.prevPosZ = z;
		this.setOrigin(new BlockPos(this.getPositionVec()));
	}

	public ThrownBlockEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world)
	{
		this(FarlandersEntityTypes.THROWN_BLOCK, world);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void tick()
	{
		BlockState fallTile = Blocks.SNOW_BLOCK.getDefaultState();

		if (fallTile.isAir())
		{
			this.remove();
		}
		else
		{
			this.prevPosX = this.getPosX();
			this.prevPosY = this.getPosY();
			this.prevPosZ = this.getPosZ();
			Block block = fallTile.getBlock();

			if (this.fallTime++ == 0)
			{
				BlockPos blockpos = new BlockPos(this.getPositionVec());
				if (this.world.getBlockState(blockpos).getBlock() == block)
				{
					this.world.removeBlock(blockpos, false);
				}
				else if (!this.world.isRemote)
				{
					this.remove();
					return;
				}
			}

			if (!this.hasNoGravity())
			{
				this.setMotion(this.getMotion().add(0.0D, -0.04D, 0.0D));
			}

			this.move(MoverType.SELF, this.getMotion());
			this.setMotion(this.getMotion().scale(0.98D));
		}

		List<Entity> list = Lists.newArrayList(this.world.getEntitiesWithinAABBExcludingEntity(this, this.getBoundingBox()));
		for (Entity entity : list)
		{
			this.damage(entity, 0.0F);
		}

		if (this.ticksExisted > 500 || this.collidedHorizontally || this.collidedVertically)
		{
			if (this.world instanceof ServerWorld)
			{
				for (int k = 0; k < 10; k++)
				{
					this.playSound(SoundEvents.BLOCK_SNOW_BREAK, 0.5F, 1.0F);
					this.playSound(SoundEvents.BLOCK_SNOW_BREAK, 0.5F, 0.5F);
					((ServerWorld) this.world).spawnParticle(ParticleTypes.CLOUD, this.getPosX(), this.getPosY() + (double) this.getHeight() / 1.5D, this.getPosZ(), 10, (double) (this.getWidth() / 4.0F), (double) (this.getHeight() / 4.0F), (double) (this.getWidth() / 4.0F), 0.05D);
					((ServerWorld) this.world).spawnParticle(new BlockParticleData(ParticleTypes.BLOCK, Blocks.SNOW_BLOCK.getDefaultState()), this.getPosX(), this.getPosY() + (double) this.getHeight() / 1.5D, this.getPosZ(), 10, (double) (this.getWidth() / 4.0F), (double) (this.getHeight() / 4.0F), (double) (this.getWidth() / 4.0F), 0.05D);
				}
			}

			this.remove();
		}
	}

	private void damage(Entity hitEntityIn, float amp)
	{
		DamageSource damagesource = DamageSource.FALLING_BLOCK;
		LivingEntity thrower = this.thrower;

		if (hitEntityIn instanceof LivingEntity && hitEntityIn.isAlive() && !hitEntityIn.isInvulnerable() && hitEntityIn != thrower && ((LivingEntity) hitEntityIn).hurtTime == 0)
		{
			if (thrower == null)
			{
				hitEntityIn.attackEntityFrom(damagesource.setProjectile(), 7.0F + amp);
			}
			else
			{
				if (thrower.isOnSameTeam(hitEntityIn))
				{
					return;
				}

				hitEntityIn.attackEntityFrom(new EntityDamageSource("colossus_crush", thrower).setDifficultyScaled().setProjectile(), 7.0F + amp);

			}
		}
	}

	public void fall(float distance, float damageMultiplier)
	{
		int i = MathHelper.ceil(distance - 1.0F);
		if (i > 0)
		{
			List<Entity> list = Lists.newArrayList(this.world.getEntitiesWithinAABBExcludingEntity(this, this.getBoundingBox()));
			DamageSource damagesource = DamageSource.FALLING_BLOCK;

			for (Entity entity : list)
			{
				entity.attackEntityFrom(damagesource, (float) Math.min(MathHelper.floor((float) i * 5), 5));
			}

		}

	}

	@Override
	public IPacket<?> createSpawnPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
