package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.ai.GolemStayNearFarlanderGoal;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.entity.util.ITeleportingEntity;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.PatrolVillageGoal;
import net.minecraft.entity.ai.goal.ReturnToVillageGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class EnderGolemEntity extends MonsterEntity implements IColoredEyes, ITeleportingEntity
{
	public static final DataParameter<Integer> EYE_COLOR = EntityDataManager.<Integer>createKey(EnderGolemEntity.class, DataSerializers.VARINT);
	public static final DataParameter<Boolean> ANGRY = EntityDataManager.<Boolean>createKey(EnderGolemEntity.class, DataSerializers.BOOLEAN);

	private int attackTimer;

	public EnderGolemEntity(EntityType<? extends EnderGolemEntity> type, World world)
	{
		super(type, world);

		this.stepHeight = 1.0F;
		this.setPathPriority(PathNodeType.WATER, -1.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(2, new GolemStayNearFarlanderGoal(this, 0.6D));
		this.goalSelector.addGoal(2, new ReturnToVillageGoal(this, 0.6D, false));
		this.goalSelector.addGoal(4, new PatrolVillageGoal(this, 0.6D));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.5D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, false));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, MobEntity.class, 5, false, false, (entity) ->
		{
			return entity instanceof IMob && !(entity instanceof CreeperEntity) && !(entity instanceof TitanEntity) && !(entity instanceof EnderGolemEntity) && !(entity instanceof EnderGuardianEntity) || entity instanceof WandererEntity;
		}));
	}

	@Override
	protected void registerData()
	{
		super.registerData();

		this.dataManager.register(EYE_COLOR, Integer.valueOf(this.rand.nextInt(6)));
		this.dataManager.register(ANGRY, false);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("EyeColor", this.getEyeColor());
		compound.putBoolean("IsAngry", this.getAngry());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setEyeColor(compound.getInt("EyeColor"));
		this.setAngry(compound.getBoolean("IsAngry"));
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 3.55F;
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MonsterEntity.func_234295_eP_().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.5D).createMutableAttribute(Attributes.MAX_HEALTH, 100.0D).createMutableAttribute(Attributes.KNOCKBACK_RESISTANCE, 0.5D);
	}

	@Override
	public void setAttackTarget(@Nullable LivingEntity target)
	{
		if (target != null)
		{
			if (target instanceof PlayerEntity && ((PlayerEntity) target).inventory.armorInventory.get(3).getItem() == Blocks.CARVED_PUMPKIN.asItem())
				return;

			this.setAngry(true);
		}
		else
			this.setAngry(false);

		super.setAttackTarget(target);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubbleColumn())
			this.attackEntityFrom(DamageSource.DROWN, 1);
	}

	@Override
	public void livingTick()
	{
		super.livingTick();

		if (this.attackTimer > 0)
			--this.attackTimer;

		if (horizontalMag(this.getMotion()) > (double) 2.5000003E-7F && this.rand.nextInt(5) == 0)
		{
			int i = MathHelper.floor(this.getPosX());
			int j = MathHelper.floor(this.getPosY() - (double) 0.2F);
			int k = MathHelper.floor(this.getPosZ());
			BlockState blockstate = this.world.getBlockState(new BlockPos(i, j, k));

			if (!this.world.isAirBlock(new BlockPos(i, j, k)))
				this.world.addParticle(new BlockParticleData(ParticleTypes.BLOCK, blockstate), this.getPosX() + ((double) this.rand.nextFloat() - 0.5D) * (double) this.getWidth(), this.getBoundingBox().minY + 0.1D, this.getPosZ() + ((double) this.rand.nextFloat() - 0.5D) * (double) this.getWidth(), 4.0D * ((double) this.rand.nextFloat() - 0.5D), 0.5D, ((double) this.rand.nextFloat() - 0.5D) * 4.0D);
		}
	}

	@Override
	public boolean attackEntityAsMob(Entity entityIn)
	{
		this.attackTimer = 10;
		this.world.setEntityState(this, (byte) 4);
		boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), 8);

		if (flag)
		{
			entityIn.setMotion(entityIn.getMotion().add(0.0D, (double) 0.4F, 0.0D));
			this.applyEnchantments(this, entityIn);
		}

		this.playSound(SoundEvents.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		return flag;
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else if (!(source instanceof IndirectEntityDamageSource))
		{
			boolean flag = super.attackEntityFrom(source, amount);
			if (!this.world.isRemote() && source.isUnblockable() && this.rand.nextInt(10) != 0)
			{
				this.teleportRandomly(this);
			}

			return flag;
		}
		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleportRandomly(this))
				{
					return true;
				}
			}

			return false;
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleStatusUpdate(byte id)
	{
		if (id == 4)
		{
			this.attackTimer = 10;
			this.playSound(SoundEvents.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		}
		else
		{
			super.handleStatusUpdate(id);
		}
	}

	@OnlyIn(Dist.CLIENT)
	public int getAttackTimer()
	{
		return this.attackTimer;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_ENDER_GOLEM_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		this.playSound(FarlandersSounds.ENTITY_ENDER_GOLEM_HURT, 1.0F, 0.8F);
		return null;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_ENDER_GOLEM_DEATH;
	}

	@Override
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.ENTITY_IRON_GOLEM_STEP, 1.0F, 1.0F);
	}

	@Override
	protected float getSoundPitch()
	{
		return 1.2F;
	}

	@Override
	public void setEyeColor(int colorID)
	{
		this.dataManager.set(EYE_COLOR, colorID);
	}

	@Override
	public int getEyeColor()
	{
		return this.dataManager.get(EYE_COLOR);
	}

	public void setAngry(boolean ang)
	{
		this.dataManager.set(ANGRY, ang);
	}

	public boolean getAngry()
	{
		return this.dataManager.get(ANGRY);
	}

	@Override
	protected void updateFallState(double y, boolean onGroundIn, BlockState state, BlockPos pos)
	{
	}
}