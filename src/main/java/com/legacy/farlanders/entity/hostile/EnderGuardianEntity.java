package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.ai.GolemStayNearFarlanderGoal;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.entity.util.ITeleportingEntity;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.OpenDoorGoal;
import net.minecraft.entity.ai.goal.PatrolVillageGoal;
import net.minecraft.entity.ai.goal.RangedBowAttackGoal;
import net.minecraft.entity.ai.goal.ReturnToVillageGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.GroundPathNavigator;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;

public class EnderGuardianEntity extends MonsterEntity implements IColoredEyes, IRangedAttackMob, ITeleportingEntity
{
	public static final DataParameter<Integer> EYE_COLOR = EntityDataManager.<Integer>createKey(EnderGuardianEntity.class, DataSerializers.VARINT);
	public static final DataParameter<Boolean> GUARDIAN_ATTACKING = EntityDataManager.<Boolean>createKey(EnderGuardianEntity.class, DataSerializers.BOOLEAN);

	private int particleEffect;

	public EnderGuardianEntity(EntityType<? extends EnderGuardianEntity> type, World world)
	{
		super(type, world);
		((GroundPathNavigator) this.getNavigator()).setBreakDoors(true);
		this.setPathPriority(PathNodeType.WATER, -1.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(1, new GolemStayNearFarlanderGoal(this, 0.5D));
		this.goalSelector.addGoal(2, new RangedBowAttackGoal<EnderGuardianEntity>(this, 0.61D, 20, 15.0F));
		this.goalSelector.addGoal(2, new ReturnToVillageGoal(this, 0.6D, false));
		this.goalSelector.addGoal(4, new PatrolVillageGoal(this, 0.6D));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.6D));
		this.goalSelector.addGoal(6, new LookAtGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));

		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, false));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, MobEntity.class, 5, false, false, (entity) ->
		{
			return entity instanceof IMob && !(entity instanceof CreeperEntity) && !(entity instanceof TitanEntity) && !(entity instanceof EnderGolemEntity) && !(entity instanceof EnderGuardianEntity) || entity instanceof WandererEntity;
		}));
	}

	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(Items.BOW));
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	protected void registerData()
	{
		super.registerData();

		this.dataManager.register(EYE_COLOR, Integer.valueOf(this.rand.nextInt(6)));
		this.dataManager.register(GUARDIAN_ATTACKING, false);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("EyeColor", this.getEyeColor());
		compound.putBoolean("GuardianAttacking", this.getAttacking());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setEyeColor(compound.getInt("EyeColor"));
		this.setAttacking(compound.getBoolean("GuardianAttacking"));
	}

	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 1.60F;
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MonsterEntity.func_234295_eP_().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.5D).createMutableAttribute(Attributes.MAX_HEALTH, 30.0D);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubbleColumn() == true)
		{
			this.attackEntityFrom(DamageSource.DROWN, 1);
		}

		if (this.getEyeColor() == 0)
		{
			for (int k = 0; k < 2; k++)
				this.world.addParticle(ParticleTypes.PORTAL, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
		}
		else if (this.getEyeColor() == 1)
		{
			for (int k = 0; k < 2; k++)
				this.world.addParticle(ParticleTypes.SMOKE, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
		}
		else if (this.getEyeColor() == 2)
		{
			this.world.addParticle(ParticleTypes.FLAME, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
		}
		else if (this.getEyeColor() == 3)
		{
			for (int k = 0; k < 2; k++)
				this.world.addParticle(ParticleTypes.MYCELIUM, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
		}
		else if (this.getEyeColor() == 4)
		{
			particleEffect++;
			if (particleEffect == 3)
			{
				this.world.addParticle(ParticleTypes.HAPPY_VILLAGER, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
				particleEffect = 0;
			}
		}
		else if (this.getEyeColor() == 5)
		{
			particleEffect++;
			if (particleEffect == 1)
			{
				this.world.addParticle(ParticleTypes.WITCH, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, 0.5d + rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
				particleEffect = 0;
			}
		}
	}

	@Override
	public void setAttackTarget(@Nullable LivingEntity target)
	{
		if (target != null)
		{
			if (target instanceof PlayerEntity && ((PlayerEntity) target).inventory.armorInventory.get(3).getItem() == Blocks.CARVED_PUMPKIN.asItem())
				return;

			this.setAttacking(true);
		}
		else
			this.setAttacking(false);
		
		
		super.setAttackTarget(target);
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else if (!(source instanceof IndirectEntityDamageSource))
		{
			boolean flag = super.attackEntityFrom(source, amount);
			if (!this.world.isRemote() && source.isUnblockable() && this.rand.nextInt(10) != 0)
			{
				this.teleportRandomly(this);
			}

			return flag;
		}
		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleportRandomly(this))
				{
					return true;
				}
			}

			return false;
		}
	}

	public void setEyeColor(int colorID)
	{
		this.dataManager.set(EYE_COLOR, colorID);
	}

	public int getEyeColor()
	{
		return this.dataManager.get(EYE_COLOR);
	}

	public void setAttacking(boolean att)
	{
		this.dataManager.set(GUARDIAN_ATTACKING, att);
	}

	public boolean getAttacking()
	{
		return this.dataManager.get(GUARDIAN_ATTACKING);
	}

	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_ENDER_GUARDIAN_IDLE;
	}

	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		this.playSound(FarlandersSounds.ENTITY_ENDER_GUARDIAN_HURT, 1.0F, 1.2F);
		return null;
	}

	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_ENDER_GUARDIAN_DEATH;
	}

	@Override
	protected float getSoundPitch()
	{
		return 1.7F;
	}

	@Override
	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor)
	{
		ItemStack itemstack = this.findAmmo(this.getHeldItem(ProjectileHelper.getHandWith(this, Items.BOW)));
		AbstractArrowEntity abstractarrowentity = ProjectileHelper.fireArrow(this, itemstack, distanceFactor);
		if (this.getHeldItemMainhand().getItem() instanceof net.minecraft.item.BowItem)
			abstractarrowentity = ((net.minecraft.item.BowItem) this.getHeldItemMainhand().getItem()).customArrow(abstractarrowentity);
		double d0 = target.getPosX() - this.getPosX();
		double d1 = target.getBoundingBox().minY + (double) (target.getHeight() / 3.0F) - abstractarrowentity.getPosY();
		double d2 = target.getPosZ() - this.getPosZ();
		double d3 = (double) MathHelper.sqrt(d0 * d0 + d2 * d2);
		abstractarrowentity.shoot(d0, d1 + d3 * (double) 0.2F, d2, 1.6F, (float) (14 - this.world.getDifficulty().getId() * 4));
		this.playSound(SoundEvents.ENTITY_ARROW_SHOOT, 1.0F, 1.0F / (this.getRNG().nextFloat() * 0.4F + 0.8F));
		this.world.addEntity(abstractarrowentity);
	}
}