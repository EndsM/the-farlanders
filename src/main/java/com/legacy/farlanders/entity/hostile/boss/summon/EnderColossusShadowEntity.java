package com.legacy.farlanders.entity.hostile.boss.summon;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.hostile.nightfall.EndSpiritEntity;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class EnderColossusShadowEntity extends MonsterEntity
{
	public static final DataParameter<Integer> INVIS_VALUE = EntityDataManager.<Integer>createKey(EndSpiritEntity.class, DataSerializers.VARINT);
	public static final DataParameter<Boolean> ANGRY = EntityDataManager.<Boolean>createKey(EnderColossusShadowEntity.class, DataSerializers.BOOLEAN);

	private int attackTimer;
	public int deathTicks, invisTick = 300;

	public EnderColossusShadowEntity(EntityType<? extends EnderColossusShadowEntity> type, World world)
	{
		super(type, world);

		this.stepHeight = 1.0F;
		this.setPathPriority(PathNodeType.WATER, -1.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.5D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, false));
	}

	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(ANGRY, false);
		this.dataManager.register(INVIS_VALUE, 200);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putBoolean("IsAngry", this.getAngry());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setAngry(compound.getBoolean("IsAngry"));
	}

	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 3.55F;
	}

	/*protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(50.0D);
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(40.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.5D);
	}*/

	public void setAttackTarget(@Nullable LivingEntity entitylivingbaseIn)
	{
		super.setAttackTarget(entitylivingbaseIn);

		if (entitylivingbaseIn != null)
		{
			this.setAngry(true);
		}
		else
		{
			this.setAngry(false);
		}
	}

	@Override
	public void tick()
	{
		super.tick();

		this.remove();
	}

	@SuppressWarnings("deprecation")
	public void livingTick()
	{
		super.livingTick();

		if (this.attackTimer > 0)
		{
			--this.attackTimer;
		}

		if (this.getHealth() <= 0.0F)
		{
			for (int i = 0; i < 3; i++)
			{
				this.world.addParticle(ParticleTypes.CLOUD, this.getPosX() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.getPosY() + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.getPosZ() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
				this.world.addParticle(ParticleTypes.LARGE_SMOKE, this.getPosX() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.getPosY() + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.getPosZ() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
			}
		}

		if (horizontalMag(this.getMotion()) > (double) 2.5000003E-7F && this.rand.nextInt(5) == 0)
		{
			int i = MathHelper.floor(this.getPosX());
			int j = MathHelper.floor(this.getPosY() - (double) 0.2F);
			int k = MathHelper.floor(this.getPosZ());
			BlockState blockstate = this.world.getBlockState(new BlockPos(i, j, k));

			if (!blockstate.isAir())
			{
				this.world.addParticle(new BlockParticleData(ParticleTypes.BLOCK, blockstate), this.getPosX() + ((double) this.rand.nextFloat() - 0.5D) * (double) this.getWidth(), this.getBoundingBox().minY + 0.1D, this.getPosZ() + ((double) this.rand.nextFloat() - 0.5D) * (double) this.getWidth(), 4.0D * ((double) this.rand.nextFloat() - 0.5D), 0.5D, ((double) this.rand.nextFloat() - 0.5D) * 4.0D);
			}
		}
	}

	public boolean attackEntityAsMob(Entity entityIn)
	{
		this.attackTimer = 10;
		this.world.setEntityState(this, (byte) 4);
		boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), 7.0F);
		if (flag)
		{
			entityIn.setMotion(entityIn.getMotion().add(0.0D, (double) 0.3F, 0.0D));
			this.applyEnchantments(this, entityIn);
		}
		this.playSound(SoundEvents.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		return flag;
	}

	@OnlyIn(Dist.CLIENT)
	public void handleStatusUpdate(byte id)
	{
		if (id == 4)
		{
			this.attackTimer = 10;
			this.playSound(SoundEvents.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 0.7F);
		}
		else
		{
			super.handleStatusUpdate(id);
		}
	}

	@Override
	protected void onDeathUpdate()
	{
		++this.deathTicks;

		for (int k = 0; k < 4; k++)
		{
			this.setInvisValue(this.getInvisValue() - 1);
		}

		if (this.deathTicks > 55)
		{
			this.remove();
		}
	}

	@OnlyIn(Dist.CLIENT)
	public int getAttackTimer()
	{
		return this.attackTimer;
	}

	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_ENDER_GOLEM_IDLE;
	}

	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		this.playSound(FarlandersSounds.ENTITY_ENDER_GOLEM_HURT, this.getSoundVolume(), 0.7F);
		return null;
	}

	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_TITAN_DEATH_ECHO;
	}

	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.ENTITY_IRON_GOLEM_STEP, 2.5F, 0.5F);
		this.playSound(SoundEvents.BLOCK_SNOW_BREAK, 0.5F, 0.1F);
	}

	protected float getSoundPitch()
	{
		return 0.9F;
	}

	@Override
	protected float getSoundVolume()
	{
		return 2.0F;
	}

	public boolean canBeCollidedWith()
	{
		return this.getHealth() > 0;
	}

	public void setAngry(boolean ang)
	{
		this.dataManager.set(ANGRY, ang);
	}

	public boolean getAngry()
	{
		return this.dataManager.get(ANGRY);
	}

	public void setInvisValue(int value)
	{
		this.dataManager.set(INVIS_VALUE, value);
	}

	public int getInvisValue()
	{
		return this.dataManager.get(INVIS_VALUE);
	}

	public void fall(float distance, float damageMultiplier)
	{
	}

	public void onKillCommand()
	{
		this.remove();
	}
}