package com.legacy.farlanders.entity.hostile;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.util.BaseEndermanEntity;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.ResetAngerGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.EndermiteEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class FanmadeEndermanEntity extends BaseEndermanEntity
{
	public FanmadeEndermanEntity(EntityType<? extends FanmadeEndermanEntity> p_i50210_1_, World p_i50210_2_)
	{
		super(p_i50210_1_, p_i50210_2_);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0D, false)
		{
			@Override
			public boolean shouldContinueExecuting()
			{
				if (this.attacker.getAttackTarget() != null && this.attacker.getAttackTarget() instanceof PlayerEntity)
				{
					return ((FanmadeEndermanEntity) this.attacker).shouldAttackPlayer((PlayerEntity) this.attacker.getAttackTarget());
				}
				else
				{
					return super.shouldContinueExecuting();
				}
			}

			@Override
			public boolean shouldExecute()
			{
				if (this.attacker.getAttackTarget() != null && this.attacker.getAttackTarget() instanceof PlayerEntity)
				{
					return ((FanmadeEndermanEntity) this.attacker).shouldAttackPlayer((PlayerEntity) this.attacker.getAttackTarget());
				}
				else
				{
					return super.shouldExecute();
				}
			}
		});
		this.goalSelector.addGoal(7, new WaterAvoidingRandomWalkingGoal(this, 1.0D, 0.0F)
		{
			@Override
			public boolean shouldExecute()
			{
				return this.creature.getAttackTarget() != null ? false : super.shouldExecute();
			}
		});
		this.goalSelector.addGoal(8, new LookAtGoal(this, PlayerEntity.class, 20.0F, Float.MAX_VALUE));
		this.goalSelector.addGoal(8, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, 10, true, false, this::func_233680_b_));
		this.targetSelector.addGoal(3, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(4, new NearestAttackableTargetGoal<>(this, EndermiteEntity.class, 10, true, false, PLAYER_SPAWNED));
		this.targetSelector.addGoal(5, new ResetAngerGoal<>(this, false));
		this.registerExtraGoals();
	}

	protected void registerExtraGoals()
	{
		this.goalSelector.addGoal(10, new BaseEndermanEntity.PlaceBlockGoal(this));
		this.goalSelector.addGoal(11, new BaseEndermanEntity.TakeBlockGoal(this));
		this.targetSelector.addGoal(2, new BaseEndermanEntity.FindPlayerGoal(this));
	}

	@Override
	public void playEndermanSound()
	{
		if (this.ticksExisted >= this.lastCreepySound + 400)
		{
			this.lastCreepySound = this.ticksExisted;

			if (!this.isSilent())
			{
				this.world.playSound(this.getPosX(), this.getPosY() + (double) this.getEyeHeight(), this.getPosZ(), FarlandersSounds.ENTITY_FANMADE_ENDERMAN_IDLE, this.getSoundCategory(), 2.5F, 1.0F, false);
			}
		}
	}

	@Override
	protected boolean shouldAttackPlayer(PlayerEntity player)
	{
		ItemStack itemstack = player.inventory.armorInventory.get(3);

		if (itemstack.getItem() == Blocks.CARVED_PUMPKIN.asItem())
			return false;
		else if (player.getDistance(this) < 7.0F)
			return true;
		else
		{
			Vector3d vec3d = player.getLook(1.0F).normalize();
			Vector3d vec3d1 = new Vector3d(this.getPosX() - player.getPosX(), this.getBoundingBox().minY + (double) this.getEyeHeight() - (player.getPosY() + (double) player.getEyeHeight()), this.getPosZ() - player.getPosZ());
			double d0 = vec3d1.length();
			vec3d1 = vec3d1.normalize();
			double d1 = vec3d.dotProduct(vec3d1);

			if (!(d1 < 1.0D / d0))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}

	@Override
	public void spawnParticles()
	{
		this.world.addParticle(ParticleTypes.AMBIENT_ENTITY_EFFECT, this.getPosX() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.getPosY() + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.getPosZ() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0); // largesmoke
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return this.isScreaming() ? FarlandersSounds.ENTITY_FANMADE_ENDERMAN_IDLE : null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return SoundEvents.ENTITY_ENDERMAN_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_FANMADE_ENDERMAN_DEATH;
	}
}