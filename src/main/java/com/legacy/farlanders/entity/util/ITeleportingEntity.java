package com.legacy.farlanders.entity.util;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;

public interface ITeleportingEntity
{
	default boolean teleportRandomly(LivingEntity entity)
	{
		if (!entity.world.isRemote() && entity.isAlive())
		{
			double d0 = entity.getPosX() + (entity.world.rand.nextDouble() - 0.5D) * 16.0D;
			double d1 = entity.getPosY() + (double) (entity.world.rand.nextInt(64) - 32);
			double d2 = entity.getPosZ() + (entity.world.rand.nextDouble() - 0.5D) * 16.0D;
			return this.teleportTo(entity, d0, d1, d2);
		}
		else
		{
			return false;
		}
	}

	default boolean teleportTo(LivingEntity entity, double x, double y, double z)
	{
		BlockPos.Mutable blockpos$mutable = new BlockPos.Mutable(x, y, z);

		while (blockpos$mutable.getY() > 0 && !entity.world.getBlockState(blockpos$mutable).getMaterial().blocksMovement())
		{
			blockpos$mutable.move(Direction.DOWN);
		}

		BlockState blockstate = entity.world.getBlockState(blockpos$mutable);
		boolean flag = blockstate.getMaterial().blocksMovement();
		boolean flag1 = blockstate.getFluidState().isTagged(FluidTags.WATER);
		if (flag && !flag1)
		{
			net.minecraftforge.event.entity.living.EnderTeleportEvent event = new net.minecraftforge.event.entity.living.EnderTeleportEvent(entity, x, y, z, 0);
			if (net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(event))
				return false;
			boolean flag2 = entity.attemptTeleport(event.getTargetX(), event.getTargetY(), event.getTargetZ(), true);
			if (flag2)
			{
				entity.world.playSound((PlayerEntity) null, entity.prevPosX, entity.prevPosY, entity.prevPosZ, SoundEvents.ENTITY_ENDERMAN_TELEPORT, entity.getSoundCategory(), 1.0F, 1.0F);
				entity.playSound(SoundEvents.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.0F);
			}

			return flag2;
		}
		else
		{
			return false;
		}
	}
}
