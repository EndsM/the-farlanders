package com.legacy.farlanders.entity.util;


public interface IColoredEyes
{
	void setEyeColor(int colorID);

	int getEyeColor();
}
