package com.legacy.farlanders.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ElderFarlanderEntity extends FarlanderEntity
{
	public ElderFarlanderEntity(EntityType<? extends ElderFarlanderEntity> type, World world)
	{
		super(type, world);
		this.setEyeColor(10);
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return FarlanderEntity.registerAttributeMap().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.4D);
	}

	@Override
	protected float getSoundPitch()
	{
		return 0.7F;
	}

	@Override
	public boolean isBreedingItem(ItemStack stack)
	{
		return false;
	}
}