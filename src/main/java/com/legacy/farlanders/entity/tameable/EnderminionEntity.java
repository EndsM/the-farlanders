package com.legacy.farlanders.entity.tameable;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.util.IColoredEyes;

import net.minecraft.block.BlockState;
import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.BreedGoal;
import net.minecraft.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.OwnerHurtByTargetGoal;
import net.minecraft.entity.ai.goal.OwnerHurtTargetGoal;
import net.minecraft.entity.ai.goal.SitGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.monster.EndermiteEntity;
import net.minecraft.entity.monster.GhastEntity;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.passive.horse.AbstractHorseEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.SwordItem;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.fml.ModList;

public class EnderminionEntity extends TameableEntity implements IColoredEyes
{
	protected static final DataParameter<Float> DATA_HEALTH_ID = EntityDataManager.createKey(EnderminionEntity.class, DataSerializers.FLOAT);
	public static final DataParameter<Integer> EYE_COLOR = EntityDataManager.<Integer>createKey(EnderminionEntity.class, DataSerializers.VARINT);

	protected int chatCounter, particleEffect, applesUsed;

	public EnderminionEntity(EntityType<? extends EnderminionEntity> type, World worldIn)
	{
		super(type, worldIn);
		this.setTamed(false);

		this.setPathPriority(PathNodeType.WATER, -1.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(1, new SwimGoal(this));
		this.goalSelector.addGoal(2, new SitGoal(this));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 1.0D, true));
		this.goalSelector.addGoal(6, new FollowOwnerGoal(this, 1.0D, 10.0F, 2.0F, true));
		this.goalSelector.addGoal(7, new BreedGoal(this, 1.0D));
		this.goalSelector.addGoal(8, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
		this.goalSelector.addGoal(10, new LookAtGoal(this, PlayerEntity.class, 8.0F));
		this.goalSelector.addGoal(10, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(1, new OwnerHurtByTargetGoal(this));
		this.targetSelector.addGoal(2, new OwnerHurtTargetGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, EndermiteEntity.class, false));
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.35F).createMutableAttribute(Attributes.MAX_HEALTH, 10.0D).createMutableAttribute(Attributes.ATTACK_DAMAGE, 2.0D);
	}

	public void setAttackTarget(@Nullable LivingEntity entitylivingbaseIn)
	{
		super.setAttackTarget(entitylivingbaseIn);
	}

	protected void updateAITasks()
	{
		this.dataManager.set(DATA_HEALTH_ID, this.getHealth());
	}

	@Override
	public boolean canDespawn(double distanceToClosestPlayer)
	{
		return !this.isTamed() && this.ticksExisted > 20 * 180;
	}

	@Override
	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(DATA_HEALTH_ID, this.getHealth());
		this.dataManager.register(EYE_COLOR, Integer.valueOf(this.rand.nextInt(6)));
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("EyeColor", this.getEyeColor());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setEyeColor(compound.getInt("EyeColor"));
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_ENDERMINION_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FarlandersSounds.ENTITY_ENDERMINION_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_ENDERMINION_DEATH;
	}

	@Override
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
	}

	@Override
	protected float getSoundVolume()
	{
		return 0.4F;
	}

	@Override
	public void livingTick()
	{
		super.livingTick();
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubbleColumn() == true)
		{
			this.attackEntityFrom(DamageSource.DROWN, 1);
		}

		if (this.isTamed() && !this.isSitting())
		{
			if (this.getEyeColor() == 0)
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.PORTAL, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
			}
			else if (this.getEyeColor() == 1)
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.SMOKE, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
			}
			else if (this.getEyeColor() == 2)
			{
				this.world.addParticle(ParticleTypes.FLAME, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
			}
			else if (this.getEyeColor() == 3)
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.MYCELIUM, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
			}
			else if (this.getEyeColor() == 4)
			{
				particleEffect++;
				if (particleEffect == 3)
				{
					this.world.addParticle(ParticleTypes.HAPPY_VILLAGER, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
					particleEffect = 0;
				}
			}
			else if (this.getEyeColor() == 5)
			{
				particleEffect++;
				if (particleEffect == 1)
				{
					this.world.addParticle(ParticleTypes.WITCH, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, 0.5d + rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
					particleEffect = 0;
				}
			}
		}

		if (this.chatCounter > 0)
		{
			--this.chatCounter;
		}
	}

	@Override
	public void onDeath(DamageSource cause)
	{
		super.onDeath(cause);

		if (this.getHeldItemMainhand().getItem() instanceof SwordItem)
		{
			this.entityDropItem(this.getHeldItemMainhand());
		}
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 2.2F;
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else
		{
			Entity entity = source.getTrueSource();
			if (this.getOwner() != null)
			{
				this.func_233687_w_(false);
			}
			if (entity != null && !(entity instanceof PlayerEntity) && !(entity instanceof AbstractArrowEntity))
			{
				amount = (amount + 1.0F) / 2.0F;
			}
			return super.attackEntityFrom(source, amount);
		}
	}

	@Override
	public boolean attackEntityAsMob(Entity entityIn)
	{
		boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), (float) Math.min(8, this.getAttribute(Attributes.ATTACK_DAMAGE).getValue()));

		if (flag)
			this.applyEnchantments(this, entityIn);

		return flag;
	}

	public void setTamed(boolean tamed)
	{
		super.setTamed(tamed);

		if (tamed)
			this.setHealth(this.getMaxHealth());
	}

	@Override
	public ActionResultType func_230254_b_(PlayerEntity player, Hand hand)
	{
		ItemStack itemstack = player.getHeldItem(hand);
		Item item = itemstack.getItem();
		if (this.isTamed())
		{
			if (!itemstack.isEmpty())
			{
				if (item.isFood())
				{
					if (item == Items.APPLE && this.getHealth() < this.getMaxHealth())
					{
						if (!player.abilities.isCreativeMode)
							itemstack.shrink(1);

						this.heal((float) item.getFood().getHealing());
						return ActionResultType.SUCCESS;
					}
				}

				if (this.canBeGivenItem() && this.getHeldItemMainhand().isEmpty())
				{
					if (player.inventory.getCurrentItem().getItem() instanceof SwordItem)
					{
						ItemStack copy = player.getHeldItemMainhand().copy();
						this.setItemStackToSlot(EquipmentSlotType.MAINHAND, copy);
						player.inventory.getCurrentItem().shrink(1);
						this.world.playSound(player, this.getPosition(), SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.HOSTILE, 1.0F, 1.0F);
						this.world.playSound(player, this.getPosition(), SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, SoundCategory.HOSTILE, 1.0F, 1.0F);
						ItemStack var2 = this.getHeldItem(Hand.MAIN_HAND);
						double swordDamage = 1.5D;
						try
						{
							swordDamage += ((SwordItem) var2.getItem()).getAttackDamage();
						}
						catch (Exception e)
						{
							swordDamage = 3.0F;
						}
						finally
						{
							this.getAttribute(Attributes.ATTACK_DAMAGE).setBaseValue(swordDamage);
						}

						return ActionResultType.SUCCESS;
					}
				}
			}
			else if (player.inventory.getCurrentItem().isEmpty() && player.isCrouching())
			{
				if (this.getHeldItemMainhand().getItem() instanceof SwordItem)
				{
					ItemStack copy = this.getHeldItemMainhand().copy();
					player.setItemStackToSlot(EquipmentSlotType.MAINHAND, copy);
					this.getHeldItemMainhand().shrink(1);
					this.world.playSound(player, this.getPosition(), SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.HOSTILE, 1.0F, 1.0F);
					this.getAttribute(Attributes.ATTACK_DAMAGE).setBaseValue(1.0F);
				}

				return ActionResultType.SUCCESS;
			}

			else if (this.isOwner(player) && !this.isBreedingItem(itemstack) && !(player.inventory.getCurrentItem().getItem() instanceof SwordItem) && player.inventory.getCurrentItem().getItem() != Items.PAPER && !player.isCrouching())
			{
				int randomChance;
				if (!this.isSitting())
				{
					if (this.chatCounter <= 0)
					{
						if (this.world.isRemote)
							return ActionResultType.SUCCESS;

						this.func_233687_w_(!this.isSitting());
						randomChance = rand.nextInt(3);

						if (randomChance == 0)
						{
							TranslationTextComponent chat = new TranslationTextComponent("gui.enderminion.sit.one");
							player.sendStatusMessage(chat, true);
						}
						else if (randomChance == 1)
						{
							TranslationTextComponent chat = new TranslationTextComponent("gui.enderminion.sit.two");
							player.sendStatusMessage(chat, true);
						}
						else
						{
							TranslationTextComponent chat = new TranslationTextComponent("gui.enderminion.sit.three");
							player.sendStatusMessage(chat, true);
						}

						this.chatCounter = 5;

						return ActionResultType.SUCCESS;
					}
				}
				else
				{
					if (this.chatCounter <= 0)
					{
						if (this.world.isRemote)
							return ActionResultType.SUCCESS;

						this.func_233687_w_(!this.isSitting());
						randomChance = rand.nextInt(3);

						if (randomChance == 0)
						{
							TranslationTextComponent chat = new TranslationTextComponent("gui.enderminion.stand.one");
							player.sendStatusMessage(chat, true);
						}
						else if (randomChance == 1)
						{
							TranslationTextComponent chat = new TranslationTextComponent("gui.enderminion.stand.two");
							player.sendStatusMessage(chat, true);
						}
						else
						{
							TranslationTextComponent chat = new TranslationTextComponent("gui.enderminion.stand.three");
							player.sendStatusMessage(chat, true);
						}

						this.chatCounter = 5;

						return ActionResultType.SUCCESS;
					}
				}
			}
		}
		else if (item == Items.APPLE)
		{
			if (!player.abilities.isCreativeMode)
				itemstack.shrink(1);

			if (!this.world.isRemote)
			{
				// Premium Wood allows for easy apples, so it has to be nerfed.
				boolean appleModInstalled = ModList.get().isLoaded("premium_wood") || ModList.get().isLoaded("fruitful");
				int chance = appleModInstalled ? 20 : 7;
				int minimumApples = appleModInstalled ? 10 : 5;
				boolean hasUsedMinimumApples = this.applesUsed > minimumApples;
				if (!hasUsedMinimumApples)
				{
					++this.applesUsed;
					this.world.setEntityState(this, (byte) 6);
					return ActionResultType.SUCCESS;
				}
				else if (hasUsedMinimumApples && this.rand.nextInt(chance) == 0 && !ForgeEventFactory.onAnimalTame(this, player))
				{
					this.setTamedBy(player);
					this.navigator.clearPath();
					this.setAttackTarget((LivingEntity) null);
					this.func_233687_w_(true);
					this.setHealth(40.0F);
					this.world.setEntityState(this, (byte) 7);

					this.applesUsed = 0;
				}
				else
				{
					this.world.setEntityState(this, (byte) 6);
				}
			}

			return ActionResultType.SUCCESS;
		}

		return super.func_230254_b_(player, hand);
	}

	public boolean isBreedingItem(ItemStack stack)
	{
		Item item = stack.getItem();
		return item.isFood() && item.getFood().isMeat();
	}

	public boolean canBeGivenItem()
	{
		return true;
	}

	public int getMaxSpawnedInChunk()
	{
		return 8;
	}

	public void setEyeColor(int colorID)
	{
		this.dataManager.set(EYE_COLOR, colorID);
	}

	public int getEyeColor()
	{
		return this.dataManager.get(EYE_COLOR);
	}

	@Override
	public AgeableEntity func_241840_a(ServerWorld worldIn, AgeableEntity ageableIn)
	{
		return null;
	}

	public boolean shouldAttackEntity(LivingEntity target, LivingEntity owner)
	{
		if (!(target instanceof CreeperEntity) && !(target instanceof GhastEntity))
		{
			if (target instanceof EnderminionEntity)
			{
				EnderminionEntity wolfentity = (EnderminionEntity) target;
				if (wolfentity.isTamed() && wolfentity.getOwner() == owner)
				{
					return false;
				}
			}
			if (target instanceof PlayerEntity && owner instanceof PlayerEntity && !((PlayerEntity) owner).canAttackPlayer((PlayerEntity) target))
			{
				return false;
			}
			else if (target instanceof AbstractHorseEntity && ((AbstractHorseEntity) target).isTame())
			{
				return false;
			}
			else
			{
				return !(target instanceof CatEntity) || !((CatEntity) target).isTamed();
			}
		}
		else
		{
			return false;
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleStatusUpdate(byte id)
	{
		super.handleStatusUpdate(id);
	}
}