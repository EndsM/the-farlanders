package com.legacy.farlanders.entity.tameable;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.registry.FarlandersItems;

import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.goal.BreedGoal;
import net.minecraft.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.OwnerHurtByTargetGoal;
import net.minecraft.entity.ai.goal.OwnerHurtTargetGoal;
import net.minecraft.entity.ai.goal.RangedAttackGoal;
import net.minecraft.entity.ai.goal.SitGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.EndermiteEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.SmallFireballEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;

public class MysticEnderminionEntity extends EnderminionEntity implements IRangedAttackMob
{
	private int smallWandCounter = 0;
	private int largeWandCounter = 0;

	public MysticEnderminionEntity(EntityType<? extends MysticEnderminionEntity> type, World worldIn)
	{
		super(type, worldIn);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(1, new SwimGoal(this));
		this.goalSelector.addGoal(2, new SitGoal(this));
		this.goalSelector.addGoal(5, new RangedAttackGoal(this, 0.62D, 30, 10.0F));
		this.goalSelector.addGoal(6, new FollowOwnerGoal(this, 1.0D, 10.0F, 2.0F, true));
		this.goalSelector.addGoal(7, new BreedGoal(this, 1.0D));
		this.goalSelector.addGoal(8, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
		this.goalSelector.addGoal(10, new LookAtGoal(this, PlayerEntity.class, 8.0F));
		this.goalSelector.addGoal(10, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(1, new OwnerHurtByTargetGoal(this));
		this.targetSelector.addGoal(2, new OwnerHurtTargetGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, EndermiteEntity.class, false));
	}

	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(FarlandersItems.mystic_wand_fire_small));
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public ActionResultType func_230254_b_(PlayerEntity player, Hand hand)
	{
		ItemStack currentItem = player.inventory.getCurrentItem();
		ItemStack itemstack = player.getHeldItem(hand);
		Item item = itemstack.getItem();
		String space = " ";

		if (this.isTamed())
		{
			if (!itemstack.isEmpty())
			{
				if (item.isFood())
				{
					if (item == Items.APPLE && this.getHealth() < this.getMaxHealth())
					{
						if (!player.abilities.isCreativeMode)
							itemstack.shrink(1);

						this.heal((float) item.getFood().getHealing());
						return ActionResultType.SUCCESS;
					}
				}
			}

			else if (currentItem.getItem() == Items.ENCHANTED_GOLDEN_APPLE)
			{
				if (!player.isCreative())
				{
					currentItem.shrink(1);
				}

				this.addPotionEffect(new EffectInstance(Effects.REGENERATION, 60 * 20));
				this.addPotionEffect(new EffectInstance(Effects.STRENGTH, 60 * 20));
				this.addPotionEffect(new EffectInstance(Effects.FIRE_RESISTANCE, 60 * 20));
				this.addPotionEffect(new EffectInstance(Effects.SPEED, 60 * 20));
				this.addPotionEffect(new EffectInstance(Effects.JUMP_BOOST, 60 * 20));
				this.addPotionEffect(new EffectInstance(Effects.RESISTANCE, 60 * 20));
				this.playSound(SoundEvents.ENTITY_ZOMBIE_VILLAGER_CURE, 1.0F, 1.0F);
				return ActionResultType.SUCCESS;
			}

			else if (currentItem.getItem() == Items.PAPER)
			{
				if (this.world.isRemote)
					return ActionResultType.SUCCESS;

				TranslationTextComponent chat = new TranslationTextComponent("Health: " + (int) this.getHealth() + "/" + this.getMaxHealth());
				player.sendMessage(chat, Util.DUMMY_UUID);
				if ((5 - this.smallWandCounter) == 1)
				{
					chat = new TranslationTextComponent(I18n.format("item.farlanders.mystic_wand") + " - " + I18n.format("gui.item.wand.small_fireball") + ": " + 1 + space + I18n.format("item.minecraft.gold_ingot") + "/" + I18n.format("item.minecraft.iron_ingot"));
					player.sendMessage(chat, Util.DUMMY_UUID);
				}
				else
				{
					chat = new TranslationTextComponent(I18n.format("item.farlanders.mystic_wand") + " - " + I18n.format("gui.item.wand.small_fireball") + ": " + (5 - this.smallWandCounter) + space + I18n.format("item.minecraft.gold_ingot") + "/" + I18n.format("item.minecraft.iron_ingot"));
					player.sendMessage(chat, Util.DUMMY_UUID);
				}
				if ((5 - this.largeWandCounter) == 1)
				{
					chat = new TranslationTextComponent(I18n.format("item.farlanders.mystic_wand") + " - " + I18n.format("gui.item.wand.large_fireball") + ": " + 1 + space + I18n.format("item.minecraft.diamond") + "/" + I18n.format("item.minecraft.emerald") + "/" + I18n.format("item.farlanders.endumium_crystal"));
					player.sendMessage(chat, Util.DUMMY_UUID);
				}
				else
				{
					chat = new TranslationTextComponent(I18n.format("item.farlanders.mystic_wand") + " - " + I18n.format("gui.item.wand.large_fireball") + ": " + (5 - this.smallWandCounter) + space + I18n.format("item.minecraft.diamond") + "/" + I18n.format("item.minecraft.emerald") + "/" + I18n.format("item.farlanders.endumium_crystal"));
					player.sendMessage(chat, Util.DUMMY_UUID);
				}
				chat = new TranslationTextComponent("");
				player.sendMessage(chat, Util.DUMMY_UUID);
			}
			else if ((currentItem.getItem() == Items.GOLD_INGOT || currentItem.getItem() == Items.IRON_INGOT) && !this.world.isRemote)
			{
				if (!player.isCreative())
				{
					currentItem.shrink(1);
					;
				}
				if (smallWandCounter < 4)
				{
					this.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 0.7F);
					smallWandCounter++;
				}
				else
				{
					smallWandCounter = 0;
					this.entityDropItem(FarlandersItems.mystic_wand_fire_small, 1);
					this.playSound(SoundEvents.ENTITY_CHICKEN_EGG, 1.0F, 1.0F);
				}

				return ActionResultType.SUCCESS;
			}
			else if ((currentItem.getItem() == Items.DIAMOND || currentItem.getItem() == Items.EMERALD || currentItem.getItem() == FarlandersItems.endumium_crystal) && !this.world.isRemote)
			{
				if (!player.isCreative())
				{
					currentItem.shrink(1);
					;
				}
				if (largeWandCounter < 4)
				{
					this.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.3F);
					largeWandCounter++;
				}
				else
				{
					largeWandCounter = 0;
					this.entityDropItem(FarlandersItems.mystic_wand_fire_large, 1);
					this.playSound(SoundEvents.ENTITY_CHICKEN_EGG, 1.0F, 1.0F);
				}

				return ActionResultType.SUCCESS;
			}
		}

		return super.func_230254_b_(player, hand);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_MYSTIC_ENDERMINION_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FarlandersSounds.ENTITY_MYSTIC_ENDERMINION_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_MYSTIC_ENDERMINION_DEATH;
	}

	@Override
	protected float getSoundPitch()
	{
		return 1.4F;
	}

	@Override
	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor)
	{
		double d = target.getPosX() - getPosX();
		double d1 = (target.getBoundingBox().minY + (double) (target.getHeight() / 2.0F)) - (getPosY() + (double) (this.getHeight() / 2.0F));
		double d2 = target.getPosZ() - getPosZ();
		SmallFireballEntity entitysmallfireball = new SmallFireballEntity(this.world, this, d, d1 - 0.7f, d2);
		entitysmallfireball.setPosition(entitysmallfireball.getPosX(), getPosY() + (double) (this.getHeight() / 2.0F) + 0.5D, entitysmallfireball.getPosZ());
		entitysmallfireball.setShooter(this);
		this.world.addEntity(entitysmallfireball);
		this.playSound(SoundEvents.ENTITY_GHAST_SHOOT, 1.0F, 1.5F);

	}
}
