package com.legacy.farlanders.entity;

import java.util.Map;
import java.util.Optional;
import java.util.function.BiPredicate;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.ai.FarlanderBreedGoal;
import com.legacy.farlanders.entity.ai.tasks.FarlanderTasks;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.legacy.farlanders.entity.hostile.boss.EnderColossusEntity;
import com.legacy.farlanders.entity.util.BreedableVillagerEntity;
import com.legacy.farlanders.entity.util.FarlanderTrades;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.registry.FarlandersBlocks;
import com.legacy.farlanders.registry.FarlandersEntityTypes;
import com.legacy.farlanders.registry.FarlandersItems;
import com.legacy.farlanders.registry.FarlandersSchedules;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Dynamic;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.brain.Brain;
import net.minecraft.entity.ai.brain.memory.MemoryModuleStatus;
import net.minecraft.entity.ai.brain.memory.MemoryModuleType;
import net.minecraft.entity.ai.brain.schedule.Activity;
import net.minecraft.entity.ai.brain.sensor.Sensor;
import net.minecraft.entity.ai.brain.sensor.SensorType;
import net.minecraft.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.entity.ai.goal.OpenDoorGoal;
import net.minecraft.entity.ai.goal.TemptGoal;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.entity.merchant.villager.VillagerData;
import net.minecraft.entity.merchant.villager.VillagerTrades;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MerchantOffer;
import net.minecraft.item.MerchantOffers;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.DebugPacketSender;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.GroundPathNavigator;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.server.MinecraftServer;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.GlobalPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.village.PointOfInterestManager;
import net.minecraft.village.PointOfInterestType;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class FarlanderEntity extends BreedableVillagerEntity implements IColoredEyes
{
	public static final DataParameter<Integer> LEVEL = EntityDataManager.<Integer>createKey(FarlanderEntity.class, DataSerializers.VARINT);
	public static final DataParameter<Integer> EYE_COLOR = EntityDataManager.<Integer>createKey(FarlanderEntity.class, DataSerializers.VARINT);

	private int particleEffect, xp, timeUntilReset, resetsSinceRestocked;
	private long lastRestockTime, timeOfDay;
	private boolean customer;
	private BlockPos homePos;
	private static final ImmutableList<MemoryModuleType<?>> MEMORY_TYPES = ImmutableList.of(MemoryModuleType.HOME, MemoryModuleType.MEETING_POINT, MemoryModuleType.MOBS, MemoryModuleType.VISIBLE_MOBS, MemoryModuleType.NEAREST_PLAYERS, MemoryModuleType.NEAREST_VISIBLE_PLAYER, MemoryModuleType.WALK_TARGET, MemoryModuleType.LOOK_TARGET, MemoryModuleType.INTERACTION_TARGET, MemoryModuleType.BREED_TARGET, MemoryModuleType.PATH, MemoryModuleType.INTERACTABLE_DOORS, MemoryModuleType.NEAREST_BED, MemoryModuleType.HURT_BY, MemoryModuleType.HURT_BY_ENTITY, MemoryModuleType.NEAREST_HOSTILE, MemoryModuleType.HEARD_BELL_TIME, MemoryModuleType.CANT_REACH_WALK_TARGET_SINCE, MemoryModuleType.LAST_SLEPT);
	private static final ImmutableList<SensorType<? extends Sensor<? super FarlanderEntity>>> SENSOR_TYPES = ImmutableList.of(SensorType.NEAREST_LIVING_ENTITIES, SensorType.NEAREST_PLAYERS, SensorType.NEAREST_ITEMS, SensorType.NEAREST_BED, SensorType.HURT_BY, SensorType.VILLAGER_HOSTILES);
	public static final Map<MemoryModuleType<GlobalPos>, BiPredicate<FarlanderEntity, PointOfInterestType>> VALID_POI = ImmutableMap.of(MemoryModuleType.HOME, (entity, type) ->
	{
		return type == PointOfInterestType.HOME;
	}, MemoryModuleType.MEETING_POINT, (entity, type) ->
	{
		return type == PointOfInterestType.MEETING;
	});

	public FarlanderEntity(EntityType<? extends FarlanderEntity> type, World world)
	{
		super(type, world);
		((GroundPathNavigator) this.getNavigator()).setBreakDoors(true);
		((GroundPathNavigator) this.getNavigator()).setCanSwim(true);

		/*this.brain = this.createBrain(new Dynamic<>(NBTDynamicOps.INSTANCE, new CompoundNBT()));*/// TODO
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();

		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, LooterEntity.class, 8.0F, 0.7D, 0.8D));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, RebelEntity.class, 8.0F, 0.7D, 0.8D));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, EnderColossusEntity.class, 8.0F, 0.5D, 0.8D));
		this.goalSelector.addGoal(3, new TemptGoal(this, 0.6D, Ingredient.fromItems(FarlandersBlocks.endumium_block.asItem()), false));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));

		if (!(this instanceof ElderFarlanderEntity))
			this.goalSelector.addGoal(2, new FarlanderBreedGoal(this, 0.6D));
	}

	@Override
	protected void onGrowingAdult()
	{
		super.onGrowingAdult();

		if (this.world instanceof ServerWorld)
			this.resetBrain((ServerWorld) this.world);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return this.isChild() ? 0.75F : 1.60F;
	}

	@Override
	public boolean canDespawn(double distanceToClosestPlayer)
	{
		return false;
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.5D).createMutableAttribute(Attributes.FOLLOW_RANGE, 48.0D);
	}

	@Override
	protected void registerData()
	{
		super.registerData();

		this.dataManager.register(EYE_COLOR, Integer.valueOf(this.rand.nextInt(6)));
		this.dataManager.register(LEVEL, 1);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("EyeColor", this.getEyeColor());
		compound.putInt("Xp", this.xp);
		compound.putInt("Level", this.getLevel());

		if (this.homePos != null)
			compound.put("HomePosition", NBTUtil.writeBlockPos(this.homePos));

		compound.putLong("LastRestock", this.lastRestockTime);
		compound.putInt("RestocksToday", this.resetsSinceRestocked);
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);

		this.setEyeColor(compound.getInt("EyeColor"));
		this.setLevel(compound.getInt("Level"));

		if (compound.contains("Offers", 10))
			this.offers = new MerchantOffers(compound.getCompound("Offers"));

		if (compound.contains("Xp", 3))
			this.xp = compound.getInt("Xp");

		if (compound.contains("HomePosition"))
			this.homePos = NBTUtil.readBlockPos(compound.getCompound("HomePosition"));

		this.lastRestockTime = compound.getLong("LastRestock");
		this.setCanPickUpLoot(false);
		this.resetBrain((ServerWorld) this.world);
		this.resetsSinceRestocked = compound.getInt("RestocksToday");
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.world.getDayTime() % 6000 == 0 && !this.world.isDaytime())
		{
			if (this.canRestock())
			{
				this.restockTrades();
			}
		}

		if (this.isInWaterOrBubbleColumn() == true)
		{
			this.attackEntityFrom(DamageSource.DROWN, 1);
		}

		if (this.getClass() == FarlanderEntity.class)
		{
			switch (this.getEyeColor())
			{
			case 1:
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.SMOKE, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
				break;
			}
			case 2:
			{
				this.world.addParticle(ParticleTypes.FLAME, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
				break;
			}
			case 3:
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.MYCELIUM, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
				break;
			}
			case 4:
			{
				particleEffect++;
				if (particleEffect == 3)
				{
					this.world.addParticle(ParticleTypes.HAPPY_VILLAGER, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
					particleEffect = 0;
				}
				break;
			}
			case 5:
			{
				particleEffect++;
				if (particleEffect == 1)
				{
					this.world.addParticle(ParticleTypes.WITCH, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, 0.5d + rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
					particleEffect = 0;
				}
				break;
			}
			default:
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.PORTAL, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
				break;
			}
			}
		}
	}

	protected void updateAITasks()
	{
		this.world.getProfiler().startSection("brain");
		this.getBrain().tick((ServerWorld) this.world, this);
		this.world.getProfiler().endSection();

		if (!this.hasCustomer() && this.timeUntilReset > 0)
		{
			--this.timeUntilReset;
			if (this.timeUntilReset <= 0)
			{
				if (this.customer)
				{
					this.levelUp();
					this.customer = false;
				}
				this.addPotionEffect(new EffectInstance(Effects.REGENERATION, 200, 0));
			}
		}

		super.updateAITasks();
	}

	@OnlyIn(Dist.CLIENT)
	public void handleStatusUpdate(byte id)
	{
		switch (id)
		{
		case 12:
			this.spawnParticles(ParticleTypes.HEART);
			break;
		case 13:
			this.spawnParticles(ParticleTypes.ANGRY_VILLAGER);
			break;
		case 14:
			this.spawnParticles(ParticleTypes.HAPPY_VILLAGER);
			break;
		case 42:
			this.spawnParticles(ParticleTypes.SPLASH);
			break;
		default:
			super.handleStatusUpdate(id);
			break;
		}
	}

	public void setEyeColor(int colorID)
	{
		this.dataManager.set(EYE_COLOR, colorID);
	}

	public int getEyeColor()
	{
		return this.dataManager.get(EYE_COLOR);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_FARLANDER_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FarlandersSounds.ENTITY_FARLANDER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_FARLANDER_DEATH;
	}

	@Override
	public SoundEvent getYesSound()
	{
		this.playSound(FarlandersSounds.ENTITY_FARLANDER_IDLE, this.getSoundVolume(), 1.3F);
		return null;
	}

	@Override
	protected SoundEvent getVillagerYesNoSound(boolean positive)
	{
		if (positive)
			this.playSound(FarlandersSounds.ENTITY_FARLANDER_IDLE, this.getSoundVolume(), 1.3F);
		else
			this.playSound(FarlandersSounds.ENTITY_FARLANDER_IDLE, this.getSoundVolume(), 0.7F);

		return null;
	}

	@Override
	public AgeableEntity func_241840_a(ServerWorld worldIn, AgeableEntity ageableIn)
	{
		FarlanderEntity farlander = FarlandersEntityTypes.FARLANDER.create(this.world);
		farlander.setGrowingAge(Integer.MAX_VALUE);
		return farlander;
	}

	// processInteract
	@Override
	public ActionResultType func_230254_b_(PlayerEntity player, Hand hand)
	{
		ItemStack itemstack = player.getHeldItem(hand);

		if (itemstack.getItem() == Items.SUGAR && this.isChild())
		{
			this.setChild(false);
			this.consumeItemFromStack(player, itemstack);
			return ActionResultType.SUCCESS;
		}
		else if (itemstack.getItem() != FarlandersItems.farlander_spawn_egg && this.isAlive() && !this.isSleeping() && !this.isChild() && !player.isSecondaryUseActive() && !this.isBreedingItem(itemstack))
		{
			if (hand == Hand.MAIN_HAND)
			{
				player.addStat(Stats.TALKED_TO_VILLAGER);
			}
			if (this.getOffers().isEmpty())
			{
				return super.func_230254_b_(player, hand);
			}
			else
			{
				if (!this.world.isRemote)
				{
					this.setCustomer(player);
					this.openMerchantContainer(player, this.getDisplayName(), this.getLevel());
				}
				return ActionResultType.SUCCESS;
			}
		}
		else
		{
			return super.func_230254_b_(player, hand);
		}
	}

	private void levelUp()
	{
		this.setLevel(this.getLevel() + 1);
		this.populateTradeData();
	}

	private void beginRestock()
	{
		this.restockAndCalculateDemand();
		this.resetsSinceRestocked = 0;
	}

	public boolean canRestock()
	{
		long i = this.lastRestockTime + 12000L;
		boolean flag = this.world.getGameTime() > i;
		long j = this.world.getDayTime();
		if (this.timeOfDay > 0L)
		{
			long k = this.timeOfDay / 24000L;
			long l = j / 24000L;
			flag |= l > k;
		}

		this.timeOfDay = j;
		if (flag)
		{
			this.beginRestock();
		}

		return this.isRestockTime() && this.hasUsedOffer();
	}

	private boolean hasUsedOffer()
	{
		for (MerchantOffer merchantoffer : this.getOffers())
		{
			if (merchantoffer.hasNoUsesLeft())
			{
				return true;
			}
		}

		return false;
	}

	private boolean isRestockTime()
	{
		return this.resetsSinceRestocked < 2 && this.world.getGameTime() > this.lastRestockTime + 2400L;
	}

	public void restockTrades()
	{
		this.calculateDemandOfOffers();

		for (MerchantOffer merchantoffer : this.getOffers())
		{
			merchantoffer.resetUses();
		}

		// this.lastRestock = this.world.getGameTime();
		++this.resetsSinceRestocked;
	}

	private void restockAndCalculateDemand()
	{
		int i = 2 - this.resetsSinceRestocked;
		if (i > 0)
		{
			for (MerchantOffer merchantoffer : this.getOffers())
			{
				merchantoffer.resetUses();
			}
		}

		for (int j = 0; j < i; ++j)
		{
			this.calculateDemandOfOffers();
		}

	}

	private void calculateDemandOfOffers()
	{
		for (MerchantOffer merchantoffer : this.getOffers())
		{
			merchantoffer.calculateDemand();
		}
	}

	@Override
	protected void onVillagerTrade(MerchantOffer offer)
	{
		int i = 3 + this.rand.nextInt(4);
		this.xp += offer.getGivenExp();

		if (this.canLevelUp())
		{
			this.timeUntilReset = 40;
			this.customer = true;
			i += 5;
		}
		if (offer.getDoesRewardExp())
		{
			this.world.addEntity(new ExperienceOrbEntity(this.world, this.getPosX(), this.getPosY() + 0.5D, this.getPosZ(), i));
		}
	}

	@Override
	protected void populateTradeData()
	{
		VillagerTrades.ITrade[] avillagertrades$itrade = FarlanderTrades.farlanderTrades.get(this.getLevel());
		if (avillagertrades$itrade != null)
		{
			MerchantOffers merchantoffers = this.getOffers();
			this.addTrades(merchantoffers, avillagertrades$itrade, 2);
		}

	}

	private boolean canLevelUp()
	{
		int i = this.getLevel();
		return VillagerData.canLevelUp(i) && this.xp >= VillagerData.getExperienceNext(i);
	}

	public void setLevel(int level)
	{
		this.dataManager.set(LEVEL, level);
	}

	public int getLevel()
	{
		return this.dataManager.get(LEVEL);
	}

	public int getXp()
	{
		return this.xp;
	}

	public void setXp(int amount)
	{
		this.xp = amount;
	}

	public void setHomePos(@Nullable BlockPos pos)
	{
		this.homePos = pos;
	}

	@Nullable
	public BlockPos getHomePos()
	{
		return this.homePos;
	}

	@Override
	public void onDeath(DamageSource cause)
	{
		this.removeModule(MemoryModuleType.HOME);
		this.removeModule(MemoryModuleType.MEETING_POINT);

		super.onDeath(cause);
	}

	public void removeModule(MemoryModuleType<GlobalPos> p_213742_1_)
	{
		if (this.world instanceof ServerWorld)
		{
			MinecraftServer minecraftserver = ((ServerWorld) this.world).getServer();
			this.brain.getMemory(p_213742_1_).ifPresent((p_213752_3_) ->
			{
				ServerWorld serverworld = minecraftserver.getWorld(p_213752_3_.getDimension());
				PointOfInterestManager pointofinterestmanager = serverworld.getPointOfInterestManager();
				Optional<PointOfInterestType> optional = pointofinterestmanager.getType(p_213752_3_.getPos());
				BiPredicate<FarlanderEntity, PointOfInterestType> bipredicate = VALID_POI.get(p_213742_1_);
				if (optional.isPresent() && bipredicate.test(this, optional.get()))
				{
					pointofinterestmanager.remove(p_213752_3_.getPos());
					DebugPacketSender.func_218801_c(serverworld, p_213752_3_.getPos());
				}

			});
		}
	}

	public float getPosDistance(double x, double y, double z)
	{
		float f = (float) (this.getPosX() - x);
		float f1 = (float) (this.getPosY() - y);
		float f2 = (float) (this.getPosZ() - z);
		return MathHelper.sqrt(f * f + f1 * f1 + f2 * f2);
	}

	@Override
	public boolean isOnSameTeam(Entity entityIn)
	{
		return entityIn instanceof EnderGolemEntity || entityIn instanceof EnderGuardianEntity || entityIn instanceof FarlanderEntity || this.isOnScoreboardTeam(entityIn.getTeam());
	}

	@Override
	public boolean canBreed()
	{
		return this.getGrowingAge() == 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Brain<FarlanderEntity> getBrain()
	{
		return (Brain<FarlanderEntity>) super.getBrain();
	}

	@Override
	protected Brain.BrainCodec<FarlanderEntity> getBrainCodec()
	{
		return Brain.createCodec(MEMORY_TYPES, SENSOR_TYPES);
	}

	@Override
	protected Brain<?> createBrain(Dynamic<?> dynamicIn)
	{
		Brain<FarlanderEntity> brain = this.getBrainCodec().deserialize(dynamicIn);
		this.initBrain(brain);
		return brain;
	}

	public void resetBrain(ServerWorld p_213770_1_)
	{
		Brain<FarlanderEntity> brain = this.getBrain();
		brain.stopAllTasks(p_213770_1_, this);
		this.brain = brain.copy();
		this.initBrain(this.getBrain());
	}

	private void initBrain(Brain<FarlanderEntity> villagerBrain)
	{
		float f = (float) this.getAttribute(Attributes.MOVEMENT_SPEED).getValue();
		if (this.isChild())
		{
			villagerBrain.setSchedule(FarlandersSchedules.FARLANDER_BABY);
			villagerBrain.registerActivity(Activity.PLAY, FarlanderTasks.play(f + 0.1F));
		}
		else
		{
			villagerBrain.setSchedule(FarlandersSchedules.FARLANDER_DEFAULT);
		}

		villagerBrain.registerActivity(Activity.CORE, FarlanderTasks.core(f + 0.1F));
		villagerBrain.registerActivity(Activity.MEET, FarlanderTasks.meet(f + 0.1F), ImmutableSet.of(Pair.of(MemoryModuleType.MEETING_POINT, MemoryModuleStatus.VALUE_PRESENT)));
		villagerBrain.registerActivity(Activity.REST, FarlanderTasks.rest(f + 0.1F));
		villagerBrain.registerActivity(Activity.IDLE, FarlanderTasks.idle(f + 0.1F));
		villagerBrain.registerActivity(Activity.PANIC, FarlanderTasks.panic(f));
		villagerBrain.setDefaultActivities(ImmutableSet.of(Activity.CORE));
		villagerBrain.setFallbackActivity(Activity.IDLE);
		villagerBrain.switchTo(Activity.IDLE);
		villagerBrain.updateActivity(this.world.getDayTime(), this.world.getGameTime());
	}

	@Override
	public void startSleeping(BlockPos pos)
	{
		super.startSleeping(pos);
		this.brain.setMemory(MemoryModuleType.LAST_SLEPT, this.world.getGameTime());
		this.brain.removeMemory(MemoryModuleType.WALK_TARGET);
		this.brain.removeMemory(MemoryModuleType.CANT_REACH_WALK_TARGET_SINCE);
	}
}