package com.legacy.farlanders.client.gui;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class FarlandersPerksScreen extends Screen
{
	private final Screen parentScreen;

	public FarlandersPerksScreen(Screen parentScreenIn)
	{
		super(new TranslationTextComponent("Farlanders Perks"));
		this.parentScreen = parentScreenIn;
	}

	/*@SuppressWarnings("deprecation")
	@Override
	protected void init()
	{
		int i = 0;
		++i;
	
		if (i % 4 == 1)
		{
			++i;
		}
	
		this.addButton(new Button(this.width / 2 - 100, this.height / 6 + 34 * (i >> 1), 200, 20, "Random Sound", (p_213079_1_) ->
		{
			this.minecraft.getSoundHandler().play(SimpleSound.master(Registry.SOUND_EVENT.getRandom(new Random()), 1.0F, 1.0F));
		}));
		
		this.addButton(new Button(this.width / 2 - 100, this.height / 6 + 54 * (i >> 1), 200, 20, "Stop Sounds", (p_213079_1_) ->
		{
			this.minecraft.getSoundHandler().stop();
		}));
		
		this.addButton(new Button(this.width / 2 - 100, this.height / 6 + 74 * (i >> 1), 200, 20, I18n.format("gui.done"), (p_213079_1_) ->
		{
			this.minecraft.displayGuiScreen(this.parentScreen);
		}));
	}
	
	public void render(int p_render_1_, int p_render_2_, float p_render_3_)
	{
		this.renderBackground();
		this.drawCenteredString(this.font, TextFormatting.LIGHT_PURPLE + "Farlanders Perks", this.width / 2, 20, 16777215);
		this.drawCenteredString(this.font, "This doesn't actually do anything yet, it's just here, and nobody can see it.", this.width / 2, 50, 16777215);
		this.drawCenteredString(this.font, "So instead, have a button that plays random sounds!", this.width / 2, 60, 16777215);
	
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}*/
}