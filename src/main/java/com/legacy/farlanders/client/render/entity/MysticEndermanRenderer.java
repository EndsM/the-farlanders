package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.MysticEyeLayer;
import com.legacy.farlanders.client.render.model.MysticEndermanModel;
import com.legacy.farlanders.entity.hostile.MysticEndermanEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.util.ResourceLocation;

public class MysticEndermanRenderer extends MobRenderer<MysticEndermanEntity, MysticEndermanModel<MysticEndermanEntity>>
{

	public MysticEndermanRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new MysticEndermanModel<>(), 0.5F);
		this.addLayer(new MysticEyeLayer(this));
		this.addLayer(new HeldItemLayer<MysticEndermanEntity, MysticEndermanModel<MysticEndermanEntity>>(this)
		{
			public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, MysticEndermanEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
			{
				matrixStackIn.push();
				matrixStackIn.translate(0.1F, 0.4F, 0.0F);
				super.render(matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch);
				matrixStackIn.pop();
			}
		});

	}

	public ResourceLocation getEntityTexture(MysticEndermanEntity entity)
	{
		String type = entity.getEyeColor() == 1 ? "green" : "purple";
		return TheFarlandersMod.locate("textures/entity/mystic_enderman/mystic_" + type + ".png");
	}
}