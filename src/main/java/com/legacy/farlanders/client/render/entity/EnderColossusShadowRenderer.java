package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.model.EnderGolemModel;
import com.legacy.farlanders.entity.hostile.boss.summon.EnderColossusShadowEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class EnderColossusShadowRenderer extends MobRenderer<EnderColossusShadowEntity, EnderGolemModel<EnderColossusShadowEntity>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/colossus/shadow_colossus.png");

	public EnderColossusShadowRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new EnderGolemModel<>(), 0.5F);
	}

	/*@Override
	protected void preRenderCallback(EnderColossusShadowEntity entityliving, float f)
	{
		float f1 = 4.0F;
		GlStateManager.scalef(f1, f1, f1);
	}
	
	public void doRender(EnderColossusShadowEntity entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
	
		this.getEntityModel().isAttacking = entity.getAngry();
	}
	
	@Override
	protected void renderModel(EnderColossusShadowEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor)
	{
		GlStateManager.pushMatrix();
		this.bindEntityTexture(entity);
	
		GlStateManager.enableBlend();
		GlStateManager.disableAlphaTest();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ONE);
	
		if (entity.isInvisible())
		{
			GlStateManager.depthMask(false);
		}
		else
		{
			GlStateManager.depthMask(true);
		}
	
		int i = entity.getInvisValue();
		int j = i % 65536;
		int k = i / 65536;
		GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, (float) j, (float) k);
		GlStateManager.color4f(2.0F, 2.0F, 2.0F, 1.0F);
		GameRenderer gamerenderer = Minecraft.getInstance().gameRenderer;
		gamerenderer.setupFogColor(true);
		this.entityModel.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
		gamerenderer.setupFogColor(false);
		i = entity.getBrightnessForRender();
		j = i % 65536;
		k = i / 65536;
		GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, (float) j, (float) k);
		GlStateManager.depthMask(true);
		GlStateManager.disableBlend();
		GlStateManager.enableAlphaTest();
		GlStateManager.popMatrix();
	}*/

	public ResourceLocation getEntityTexture(EnderColossusShadowEntity entity)
	{
		return TEXTURE;
	}

}