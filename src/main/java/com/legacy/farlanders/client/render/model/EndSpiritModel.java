package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class EndSpiritModel<T extends Entity> extends SegmentedModel<T>
{
	//fields
	public ModelRenderer Head;
	ModelRenderer Headwear;
	ModelRenderer Body;
	public ModelRenderer RightArm;
	ModelRenderer LeftArm;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer Neck;

	/** Is the enderman attacking an entity? */
	public boolean isAttacking;
	public boolean hasSword;

	public EndSpiritModel()
	{
		textureWidth = 64;
		textureHeight = 32;

		Head = new ModelRenderer(this, 0, 0);
		Head.addBox(-4F, -8F, -4F, 8, 8, 8);
		Head.setRotationPoint(0F, 0F, 0F);
		Head.setTextureSize(64, 32);
		setRotation(Head, 0F, 0F, 0F);
		Headwear = new ModelRenderer(this, 0, 16);
		Headwear.addBox(-4F, -8F, -4F, 8, 8, 8);
		Headwear.setRotationPoint(0F, 0F, 0F);
		Headwear.setTextureSize(64, 32);
		setRotation(Headwear, 0F, 0F, 0F);
		Body = new ModelRenderer(this, 32, 16);
		Body.addBox(-4F, 0F, -2F, 8, 6, 4);
		Body.setRotationPoint(0F, 1F, 0F);
		Body.setTextureSize(64, 32);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 56, 0);
		RightArm.addBox(-1F, -2F, -1F, 2, 19, 2);
		RightArm.setRotationPoint(-5F, 3F, 0F);
		RightArm.setTextureSize(64, 32);
		setRotation(RightArm, 0F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 56, 0);
		LeftArm.addBox(-1F, -2F, -1F, 2, 19, 2);
		LeftArm.setRotationPoint(5F, 3F, 0F);
		LeftArm.setTextureSize(64, 32);
		setRotation(LeftArm, 0F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 56, 0);
		RightLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		RightLeg.setRotationPoint(-2F, 7F, 0F);
		RightLeg.setTextureSize(64, 32);
		setRotation(RightLeg, 0F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 56, 0);
		LeftLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		LeftLeg.setRotationPoint(2F, 7F, 0F);
		LeftLeg.setTextureSize(64, 32);
		setRotation(LeftLeg, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 8, 3);
		Neck.addBox(0F, 0F, 0F, 2, 1, 2);
		Neck.setRotationPoint(-1F, 0F, -1F);
		Neck.setTextureSize(64, 32);
		setRotation(Neck, 0F, 0F, 0F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.Head, this.Headwear, this.Body, this.RightArm, this.LeftArm, this.RightLeg, this.LeftLeg, this.Neck);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T par7Entity, float f, float f1, float f2, float f3, float f4)
	{
		Head.rotateAngleY = f3 / 57.29578F;
		Head.rotateAngleX = f4 / 57.29578F;
		Headwear.rotateAngleY = f3 / 57.29578F;
		Headwear.rotateAngleX = f4 / 57.29578F;
		LeftArm.rotateAngleX = f1;
		LeftArm.rotateAngleZ = 0.0F;	
		RightArm.rotateAngleX = f1;
		RightArm.rotateAngleZ = 0.0F;
		RightLeg.rotateAngleX = f1;
		LeftLeg.rotateAngleX = f1;
		RightLeg.rotateAngleZ = 0.0F;
		LeftLeg.rotateAngleZ = 0.0F;	
		this.Head.rotationPointY = 0.0f;
	}
}