package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ElderModel<T extends Entity> extends SegmentedModel<T>
{
	// fields
	ModelRenderer headwearSet, beard1, beard2, beard3, beard4, beard5, beard6, beard7, beard8, beard9, beard10, beard11,
			beard12;
	ModelRenderer headSet;
	ModelRenderer eyeBrows;
	ModelRenderer Body;
	ModelRenderer RightArm;
	ModelRenderer LeftArm;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer Neck;
	ModelRenderer downArm;
	ModelRenderer shoulderThing;
	ModelRenderer backCape;
	ModelRenderer frontCape;
	ModelRenderer bengalSet;
	ModelRenderer bengalSetBottom;

	public ElderModel()
	{
		textureWidth = 128;
		textureHeight = 32;

		headwearSet = new ModelRenderer(this, 0, 16);
		headwearSet.setRotationPoint(0F, 0F, -2F);
		setRotation(headwearSet, 0F, 0F, 0F);
		headwearSet.addBox(-4F, -8F, -4F, 8, 8, 8).setTextureOffset(0, 16);

		beard1 = new ModelRenderer(this, 32, 0);
		beard1.setRotationPoint(0F, 0F, 0F);
		beard1.addBox(1F, -1F, -4.2F, 1, 5, 1).setTextureOffset(32, 0);
		headwearSet.addChild(beard1);
		beard2 = new ModelRenderer(this, 33, 0);
		beard2.setRotationPoint(0F, 0F, 0F);
		beard2.addBox(0F, -2F, -4.333333F, 1, 9, 1).setTextureOffset(33, 0);
		headwearSet.addChild(beard2);
		beard3 = new ModelRenderer(this, 34, 0);
		beard3.setRotationPoint(0F, 0F, 0F);
		beard3.addBox(-1F, -2F, -4.333333F, 1, 6, 1).setTextureOffset(34, 0);
		headwearSet.addChild(beard3);
		beard4 = new ModelRenderer(this, 32, 0);
		beard4.setRotationPoint(0F, 0F, 0F);
		beard4.addBox(-2F, -1F, -4.066667F, 1, 6, 1).setTextureOffset(32, 0);
		headwearSet.addChild(beard4);
		beard5 = new ModelRenderer(this, 33, 0);
		beard5.setRotationPoint(0F, 0F, 0F);
		beard5.addBox(2F, -1F, -4.133333F, 2, 3, 1).setTextureOffset(33, 0);
		headwearSet.addChild(beard5);
		beard6 = new ModelRenderer(this, 32, 0);
		beard6.setRotationPoint(0F, 0F, 0F);
		beard6.addBox(-4F, -1F, -4.333333F, 2, 4, 1).setTextureOffset(32, 0);
		headwearSet.addChild(beard6);
		beard7 = new ModelRenderer(this, 34, 0);
		beard7.setRotationPoint(0F, 0F, 0F);
		beard7.addBox(3.066667F, -2F, -4.333333F, 1, 7, 1).setTextureOffset(34, 0);
		headwearSet.addChild(beard7);
		beard8 = new ModelRenderer(this, 33, 0);
		beard8.setRotationPoint(0F, 0F, 0F);
		beard8.addBox(-4.2F, -2F, -4.266667F, 1, 6, 1).setTextureOffset(33, 0);
		headwearSet.addChild(beard8);
		beard9 = new ModelRenderer(this, 33, 0);
		beard9.setRotationPoint(0F, 0F, 0F);
		beard9.addBox(3.133333F, -1F, -4F, 1, 5, 1).setTextureOffset(33, 0);
		headwearSet.addChild(beard9);
		beard10 = new ModelRenderer(this, 32, 0);
		beard10.setRotationPoint(0F, 0F, 0F);
		beard10.addBox(-4.133333F, -1F, -4F, 1, 5, 1).setTextureOffset(32, 0);
		headwearSet.addChild(beard10);
		beard11 = new ModelRenderer(this, 33, 0);
		beard11.setRotationPoint(0F, 0F, 0F);
		beard11.addBox(-4.2F, -2F, -3F, 1, 3, 1).setTextureOffset(33, 0);
		headwearSet.addChild(beard11);
		beard12 = new ModelRenderer(this, 34, 0);
		beard12.setRotationPoint(0F, 0F, 0F);
		beard12.addBox(3.2F, -2F, -3F, 1, 3, 1).setTextureOffset(34, 0);
		headwearSet.addChild(beard12);

		headwearSet.setTextureSize(128, 32);
		headSet = new ModelRenderer(this, 0, 0); // headSet
		headSet.setRotationPoint(0F, 0F, -2F);
		setRotation(headSet, 0F, 0F, 0F);
		headSet.addBox(-4F, -8F, -4F, 8, 8, 8);// .setTextureOffset(0, 0);
		// headSet.addBox(2F, -6F, -4.333333F, 3, 1, 1).setTextureOffset(32, 4);
		// headSet.addBox(-5F, -6F, -4.266667F, 3, 1, 1).setTextureOffset(32, 4);
		eyeBrows = new ModelRenderer(this, 32, 4); // headSet
		eyeBrows.setRotationPoint(0F, 0F, -2F);
		setRotation(eyeBrows, 0F, 0F, 0F);
		eyeBrows.addBox(2F, -6F, -4.333333F, 3, 1, 1);
		eyeBrows.addBox(-5F, -6F, -4.266667F, 3, 1, 1);
		// headSet.setTextureSize(128, 32);
		Body = new ModelRenderer(this, 32, 16);
		Body.addBox(-4F, 0F, -2F, 8, 6, 4);
		Body.setRotationPoint(0F, 1F, 0F);
		Body.setTextureSize(128, 32);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 56, 0);
		RightArm.addBox(-1F, -2F, -1F, 2, 8, 2);
		RightArm.setRotationPoint(-5F, 3F, 0F);
		RightArm.setTextureSize(128, 32);
		setRotation(RightArm, -0.5205006F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 56, 0);
		LeftArm.addBox(-1F, -2F, -1F, 2, 19, 2);
		LeftArm.setRotationPoint(5F, 3F, 0F);
		LeftArm.setTextureSize(128, 32);
		setRotation(LeftArm, 0F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 56, 0);
		RightLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		RightLeg.setRotationPoint(-2F, 7F, 0F);
		RightLeg.setTextureSize(128, 32);
		setRotation(RightLeg, 0F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 56, 0);
		LeftLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		LeftLeg.setRotationPoint(2F, 7F, 0F);
		LeftLeg.setTextureSize(128, 32);
		setRotation(LeftLeg, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 8, 3);
		Neck.addBox(0F, 0F, 0F, 2, 1, 2);
		Neck.setRotationPoint(-1F, 0F, -1F);
		Neck.setTextureSize(128, 32);
		setRotation(Neck, 0F, 0F, 0F);
		downArm = new ModelRenderer(this, 56, 0);
		downArm.addBox(-6F, 0F, 0F, 2, 8, 2);
		downArm.setRotationPoint(0F, 6.733333F, -2.466667F);
		downArm.setTextureSize(128, 32);
		setRotation(downArm, -1.412787F, 0F, 0F);
		shoulderThing = new ModelRenderer(this, 88, 0);
		shoulderThing.addBox(0F, 0F, 0F, 2, 0, 4);
		shoulderThing.setRotationPoint(-4F, 0.8666667F, -2F);
		shoulderThing.setTextureSize(128, 32);
		setRotation(shoulderThing, 0F, 0F, 0F);
		backCape = new ModelRenderer(this, 69, 0); // backcape
		backCape.setRotationPoint(-3F, 1F, 2.066667F);
		setRotation(backCape, 0F, 0F, 0F);
		backCape.addBox(-1F, 0F, 0F, 8, 14, 0).setTextureOffset(69, 0);
		backCape.addBox(-1F, 14F, 0F, 2, 2, 0).setTextureOffset(44, 4);
		backCape.addBox(2F, 14F, 0F, 2, 1, 0).setTextureOffset(44, 5);
		backCape.addBox(5F, 14F, 0F, 2, 2, 0).setTextureOffset(44, 4);
		// backCape.setTextureSize(128, 32);
		frontCape = new ModelRenderer(this, 69, 15); // frontCape
		frontCape.setRotationPoint(-3F, 1F, -2.133333F);
		setRotation(frontCape, 0F, 0F, 0F);
		frontCape.addBox(-1F, 0F, 0F, 8, 14, 0).setTextureOffset(69, 15);
		frontCape.addBox(-1F, 14F, 0F, 2, 2, 0).setTextureOffset(44, 4);
		frontCape.addBox(2F, 14F, 0F, 2, 1, 0).setTextureOffset(44, 4);
		frontCape.addBox(5F, 14F, 0F, 2, 2, 0).setTextureOffset(44, 4);
		frontCape.setTextureSize(128, 32);
		bengalSet = new ModelRenderer(this, 42, 0); // bengalSet
		bengalSet.setRotationPoint(-5F, 10F, -9F);
		setRotation(bengalSet, 0F, 0F, 0F);
		bengalSet.addBox(-1F, 0F, -1F, 3, 1, 1).setTextureOffset(42, 0);

		bengalSetBottom = new ModelRenderer(this, 50, 0); // bengalSet2
		bengalSetBottom.setRotationPoint(-5F, 10F, -9F);
		setRotation(bengalSetBottom, 0F, 0F, 0F);
		bengalSetBottom.addBox(0F, 0F, -1F, 1, 14, 1).setTextureOffset(50, 0);
		// bengalSet.setTextureSize(128, 32);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.headwearSet, this.headSet, this.eyeBrows, this.Body, this.RightArm, this.LeftArm, this.RightLeg, this.LeftLeg, this.Neck, this.downArm, this.shoulderThing, this.backCape, this.bengalSet, this.bengalSetBottom);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T par7Entity, float f, float f1, float f2, float f3, float f4)
	{
		headSet.rotateAngleY = f3 / 57.29578F;
		headSet.rotateAngleX = f4 / 57.29578F;
		headwearSet.rotateAngleY = f3 / 57.29578F;
		headwearSet.rotateAngleX = f4 / 57.29578F;
		LeftArm.rotateAngleX = f1;
		LeftArm.rotateAngleZ = 0.0F;
		RightLeg.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
		LeftLeg.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * f1;
		RightLeg.rotateAngleY = 0.0F;
		LeftLeg.rotateAngleY = 0.0F;
		backCape.rotateAngleX = f1 * 0.4F;
		backCape.rotateAngleZ = 0.0F;
		float onGround = 0;
		float var7 = MathHelper.sin(onGround - 0.5F * (float) Math.PI);
		float var8 = MathHelper.sin((1.0F - (1.0F - onGround - 0.5F) * (1.0F - onGround - 0.5F)) * (float) Math.PI);
		this.bengalSet.rotateAngleZ = 0.08F;
		this.bengalSet.rotateAngleY = -(0.1F - var7 * 0.6F);
		this.bengalSet.rotateAngleX = -((float) Math.PI / 2F);
		this.bengalSet.rotateAngleX -= var7 * 1.2F - var8 * 0.4F;
		this.bengalSet.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.02F;
		this.bengalSet.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.02F;

		this.bengalSetBottom.rotateAngleX = this.bengalSet.rotateAngleX;
		this.bengalSetBottom.rotateAngleY = this.bengalSet.rotateAngleY;
		this.bengalSetBottom.rotateAngleZ = this.bengalSet.rotateAngleZ;

		this.eyeBrows.rotateAngleX = this.headSet.rotateAngleX;
		this.eyeBrows.rotateAngleY = this.headSet.rotateAngleY;
		this.eyeBrows.rotateAngleZ = this.headSet.rotateAngleZ;
	}
}
