package com.legacy.farlanders.client.render.entity.layer.eyes;

import com.legacy.farlanders.client.render.entity.layer.GlowingEyeLayer;
import com.legacy.farlanders.client.render.model.MysticEndermanModel;
import com.legacy.farlanders.entity.hostile.MysticEndermanEntity;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class MysticEyeLayer extends GlowingEyeLayer<MysticEndermanEntity, MysticEndermanModel<MysticEndermanEntity>>
{
	private static final ResourceLocation GREEN_EYES = new ResourceLocation("farlanders", "textures/entity/mystic_enderman/mystic_green_eyes.png");
	private static final ResourceLocation PURPLE_EYES = new ResourceLocation("farlanders", "textures/entity/mystic_enderman/mystic_purple_eyes.png");

	public MysticEyeLayer(IEntityRenderer<MysticEndermanEntity, MysticEndermanModel<MysticEndermanEntity>> p_i50921_1_)
	{
		super(p_i50921_1_);
	}

	@Override
	public RenderType getRenderType(MysticEndermanEntity entity)
	{
		ResourceLocation glowTexture = entity.getEyeColor() == 1 ? GREEN_EYES : PURPLE_EYES;
		return RenderType.getEyes(glowTexture);
	}
}