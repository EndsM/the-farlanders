package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class BlockEntityModel<T extends Entity> extends SegmentedModel<T>
{
	public ModelRenderer block;

	public BlockEntityModel()
	{
		this.textureWidth = 64;
		this.textureHeight = 32;
		this.block = new ModelRenderer(this, 0, 0);
		this.block.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.block.addBox(-8.0F, -8.0F, -8.0F, 16, 16, 16, 0.0F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.block);
	}

	@Override
	public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
	}
}
