package com.legacy.farlanders.client.render.model.armor;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class RebelHelmetModel<T extends LivingEntity> extends BipedModel<T>
{

	// fields
	ModelRenderer Helmet;
	ModelRenderer Shape2;
	ModelRenderer Shape1;
	ModelRenderer RightLowerHorn;
	ModelRenderer RightUpperHorn;
	ModelRenderer LeftLowerHorn;
	ModelRenderer LeftUpperHorn;

	public RebelHelmetModel(float f)
	{
		super(f);
		textureWidth = 64;
		textureHeight = 64;

		Helmet = new ModelRenderer(this, 0, 0);
		Helmet.addBox(-4.466667F, -8.466666F, -4.466667F, 9, 8, 9);
		Helmet.setRotationPoint(0F, 0F, 0F);
		Helmet.setTextureSize(64, 64);
		Helmet.mirror = true;
		setRotation(Helmet, 0F, 0F, 0F);
		Shape2 = new ModelRenderer(this, 19, 10);
		Shape2.addBox(-5F, -7F, -1F, 1, 3, 3);
		Shape2.setRotationPoint(0F, 0F, 0F);
		Shape2.setTextureSize(64, 64);
		Shape2.mirror = true;
		setRotation(Shape2, 0F, 0F, 0F);
		Shape1 = new ModelRenderer(this, 19, 10);
		Shape1.addBox(4F, -7F, -1F, 1, 3, 3);
		Shape1.setRotationPoint(0F, 0F, 0F);
		Shape1.setTextureSize(64, 64);
		Shape1.mirror = true;
		setRotation(Shape1, 0F, 0F, 0F);
		RightLowerHorn = new ModelRenderer(this, 18, 10);
		RightLowerHorn.addBox(4.5F, -6.466667F, -0.4666667F, 4, 2, 2);
		RightLowerHorn.setRotationPoint(0F, 0F, 0F);
		RightLowerHorn.setTextureSize(64, 64);
		RightLowerHorn.mirror = true;
		setRotation(RightLowerHorn, 0F, 0F, 0F);
		RightUpperHorn = new ModelRenderer(this, 18, 10);
		RightUpperHorn.addBox(6.5F, -8.5F, -0.5F, 2, 2, 2);
		RightUpperHorn.setRotationPoint(0F, 0F, 0F);
		RightUpperHorn.setTextureSize(64, 64);
		RightUpperHorn.mirror = true;
		setRotation(RightUpperHorn, 0F, 0F, 0F);
		LeftLowerHorn = new ModelRenderer(this, 18, 10);
		LeftLowerHorn.addBox(-8.5F, -6.5F, 0F, 4, 2, 2);
		LeftLowerHorn.setRotationPoint(0F, 0F, 0F);
		LeftLowerHorn.setTextureSize(64, 64);
		LeftLowerHorn.mirror = true;
		setRotation(LeftLowerHorn, 0F, 0F, 0F);
		LeftUpperHorn = new ModelRenderer(this, 18, 10);
		LeftUpperHorn.addBox(-8.5F, -8.5F, 0F, 2, 2, 2);
		LeftUpperHorn.setRotationPoint(0F, 0F, 0F);
		LeftUpperHorn.setTextureSize(64, 64);
		LeftUpperHorn.mirror = true;
		setRotation(LeftUpperHorn, 0F, 0F, 0F);

		this.bipedHead.addChild(Helmet);
		this.bipedHead.addChild(LeftLowerHorn);
		this.bipedHead.addChild(LeftUpperHorn);
		this.bipedHead.addChild(RightLowerHorn);
		this.bipedHead.addChild(RightUpperHorn);
		this.bipedHead.addChild(Shape1);
		this.bipedHead.addChild(Shape2);

		/*Helmet.isHidden = true;
		Shape1.isHidden = true;
		Shape2.isHidden = true;
		LeftLowerHorn.isHidden = true;
		LeftUpperHorn.isHidden = true;
		RightLowerHorn.isHidden = true;
		RightUpperHorn.isHidden = true;*/
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}
}
