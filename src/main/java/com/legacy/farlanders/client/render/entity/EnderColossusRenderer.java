package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.ColossusEyesLayer;
import com.legacy.farlanders.client.render.model.EnderColossusModel;
import com.legacy.farlanders.entity.hostile.boss.EnderColossusEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class EnderColossusRenderer extends MobRenderer<EnderColossusEntity, EnderColossusModel<EnderColossusEntity>>
{

	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/colossus/colossus.png");
	// private static final ResourceLocation EXPLODING =
	// TheFarlandersMod.locate("textures/entity/colossus/shadow_colossus_explosion.png");

	public EnderColossusRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new EnderColossusModel<>(), 0.5F);
		this.addLayer(new ColossusEyesLayer<>(this));
		// this.addLayer(new EnderColossusDeathLayer(this));
	}

	/*@Override
	protected void preRenderCallback(EnderColossusEntity entityliving, float f)
	{
		float f1 = 5.5F;
		GlStateManager.scalef(f1, f1, f1);
	}
	
	public void doRender(EnderColossusEntity entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
	}
	
	protected void renderModel(EnderColossusEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor)
	{
		this.bindEntityTexture(entitylivingbaseIn);
		this.entityModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
		if (entitylivingbaseIn.hurtTime > 0)
		{
			GlStateManager.depthFunc(514);
			GlStateManager.disableTexture();
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			GlStateManager.color4f(1.0F, 0.0F, 0.0F, 0.5F);
			this.entityModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
			GlStateManager.enableTexture();
			GlStateManager.disableBlend();
			GlStateManager.depthFunc(515);
		}
	
	}*/

	public ResourceLocation getEntityTexture(EnderColossusEntity entity)
	{
		return TEXTURE;
	}

}