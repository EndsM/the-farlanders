package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.FanmadeEyesLayer;
import com.legacy.farlanders.entity.hostile.FanmadeEndermanEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.model.EndermanModel;
import net.minecraft.util.ResourceLocation;

public class FanmadeEndermanRenderer extends MobRenderer<FanmadeEndermanEntity, EndermanModel<FanmadeEndermanEntity>>
{

	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/fanmade_enderman.png");

	public FanmadeEndermanRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new EndermanModel<>(0.0F), 0.5F);
		this.addLayer(new FanmadeEyesLayer<>(this));
		// this.addLayer(new HeldBlockLayer(this));
	}

	public ResourceLocation getEntityTexture(FanmadeEndermanEntity entity)
	{
		return TEXTURE;
	}
}