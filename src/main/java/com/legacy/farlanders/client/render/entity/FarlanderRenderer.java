package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.IMutlipleLayers;
import com.legacy.farlanders.client.render.entity.layer.eyes.FarlanderEyeLayer;
import com.legacy.farlanders.client.render.model.FarlanderModel;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.util.ResourceLocation;

public class FarlanderRenderer extends MobRenderer<FarlanderEntity, FarlanderModel<FarlanderEntity>> implements IMutlipleLayers
{

	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/farlander/farlander.png");

	public FarlanderRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new FarlanderModel<>(), 0.5F);
		this.addLayer(new HeldItemLayer<FarlanderEntity, FarlanderModel<FarlanderEntity>>(this)
		{
			public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, FarlanderEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
			{
				matrixStackIn.push();
				matrixStackIn.translate(0.05F, 0.2F, 0.0F);
				super.render(matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch);
				matrixStackIn.pop();
			}
		});
		this.addLayer(new FarlanderEyeLayer(this));
	}

	@Override
	protected void preRenderCallback(FarlanderEntity entityliving, MatrixStack matrixStackIn, float f)
	{
		if (entityliving.isChild())
		{
			float f1 = 0.4375F;
			matrixStackIn.scale(f1, f1, f1);
		}
		else
		{
			float f1 = 0.9375F;
			matrixStackIn.scale(f1, f1, f1);
		}

	}

	public ResourceLocation getEntityTexture(FarlanderEntity entity)
	{
		return TEXTURE;
	}
}