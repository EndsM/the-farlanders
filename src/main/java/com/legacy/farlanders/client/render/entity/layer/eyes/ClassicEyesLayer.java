package com.legacy.farlanders.client.render.entity.layer.eyes;

import com.legacy.farlanders.TheFarlandersMod;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.AbstractEyesLayer;
import net.minecraft.client.renderer.entity.model.EndermanModel;
import net.minecraft.entity.LivingEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ClassicEyesLayer<T extends LivingEntity> extends AbstractEyesLayer<T, EndermanModel<T>>
{
	private static final RenderType RENDER_TYPE = RenderType.getEyes(TheFarlandersMod.locate("textures/entity/classic_eyes.png"));

	public ClassicEyesLayer(IEntityRenderer<T, EndermanModel<T>> rendererIn)
	{
		super(rendererIn);
	}

	public RenderType getRenderType()
	{
		return RENDER_TYPE;
	}
}