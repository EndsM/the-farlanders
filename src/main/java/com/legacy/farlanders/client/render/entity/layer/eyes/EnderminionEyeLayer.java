package com.legacy.farlanders.client.render.entity.layer.eyes;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.GlowingEyeLayer;
import com.legacy.farlanders.client.render.model.EnderminionModel;
import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class EnderminionEyeLayer extends GlowingEyeLayer<EnderminionEntity, EnderminionModel<EnderminionEntity>>
{
	public EnderminionEyeLayer(IEntityRenderer<EnderminionEntity, EnderminionModel<EnderminionEntity>> renderer)
	{
		super(renderer);
	}

	@Override
	public RenderType getRenderType(EnderminionEntity entity)
	{
		ResourceLocation glowTexture = TheFarlandersMod.locate("textures/entity/enderminion/" + this.getEyeTexture(entity) + "_eyes" + ".png");
		return RenderType.getEyes(glowTexture);
	}
	
	public String getEyeTexture(IColoredEyes entity)
	{
		switch (entity.getEyeColor())
		{
		case 1:
			return "green";
		case 2:
			return "red";
		case 3:
			return "white";
		case 4:
			return "brown";
		case 5:
			return "blue";
		default:
			return "purple";
		}
	}
}