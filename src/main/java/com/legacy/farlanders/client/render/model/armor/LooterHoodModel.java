package com.legacy.farlanders.client.render.model.armor;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class LooterHoodModel<T extends LivingEntity> extends BipedModel<T>
{
	private ModelRenderer helmet;

	public LooterHoodModel(float f)
	{
		super(f);

		textureWidth = 64;
		textureHeight = 64;
		helmet = new ModelRenderer(this, 0, 0);
		helmet.addBox(-4.466667F, -8.466666F, -4.466667F, 9, 11, 9);
		helmet.setRotationPoint(0F, 0F, 0F);
		helmet.setTextureSize(64, 64);
		setRotation(helmet, 0F, 0F, 0F);
		this.bipedHead.addChild(helmet);
	}

	/*@Override
	public Iterable<ModelRenderer> getHeadParts()
	{
		return ImmutableList.of(this.helmet, this.bipedHead);
	}*/

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}
}
