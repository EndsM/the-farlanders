package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.TitanEyeLayer;
import com.legacy.farlanders.client.render.model.TitanModel;
import com.legacy.farlanders.entity.hostile.TitanEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class TitanRenderer extends MobRenderer<TitanEntity, TitanModel<TitanEntity>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/titan/titan.png");

	public TitanRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new TitanModel<>(), 1.0F);
		this.addLayer(new TitanEyeLayer(this));
	}

	@Override
	public ResourceLocation getEntityTexture(TitanEntity entity)
	{
		return TEXTURE;
	}
}