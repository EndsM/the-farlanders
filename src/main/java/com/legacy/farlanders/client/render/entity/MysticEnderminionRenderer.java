package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.EnderminionTameLayer;
import com.legacy.farlanders.client.render.entity.layer.eyes.MysticEnderminionEyeLayer;
import com.legacy.farlanders.client.render.model.MysticEnderminionModel;
import com.legacy.farlanders.entity.tameable.MysticEnderminionEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.util.ResourceLocation;

public class MysticEnderminionRenderer extends MobRenderer<MysticEnderminionEntity, MysticEnderminionModel<MysticEnderminionEntity>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/enderminion/mystic_enderminion.png");

	public MysticEnderminionRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new MysticEnderminionModel<>(), 0.5F);
		this.addLayer(new EnderminionTameLayer<>(this));
		this.addLayer(new HeldItemLayer<MysticEnderminionEntity, MysticEnderminionModel<MysticEnderminionEntity>>(this)
		{
			public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, MysticEnderminionEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
			{
				matrixStackIn.push();
				matrixStackIn.translate(0.0F, 0.3F, 0.0F);
				super.render(matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch);
				matrixStackIn.pop();
			}
		});
		this.addLayer(new MysticEnderminionEyeLayer(this));
	}

	public ResourceLocation getEntityTexture(MysticEnderminionEntity entity)
	{
		return TEXTURE;
	}
}