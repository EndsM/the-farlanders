package com.legacy.farlanders.client.render.entity.layer.eyes;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.GlowingEyeLayer;
import com.legacy.farlanders.client.render.model.TitanModel;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class TitanEyeLayer extends GlowingEyeLayer<TitanEntity, TitanModel<TitanEntity>>
{
	public TitanEyeLayer(IEntityRenderer<TitanEntity, TitanModel<TitanEntity>> renderer)
	{
		super(renderer);
	}

	@Override
	public RenderType getRenderType(TitanEntity entity)
	{
		ResourceLocation glowTexture = TheFarlandersMod.locate("textures/entity/titan/titan_" + this.getEyeTexture(entity) + ".png");
		return RenderType.getEyes(glowTexture);
	}

	public String getEyeTexture(IColoredEyes entity)
	{
		switch (entity.getEyeColor())
		{
		case 1:
			return "blue_blue";
		case 2:
			return "blue_brown";
		case 3:
			return "blue_green";
		case 4:
			return "blue_purple";
		case 5:
			return "blue_red";
		case 6:
			return "blue_white";
		case 7:
			return "brown_blue";
		case 8:
			return "brown_brown";
		case 9:
			return "brown_green";
		case 10:
			return "brown_purple";
		case 11:
			return "brown_red";
		case 12:
			return "brown_white";
		case 13:
			return "green_blue";
		case 14:
			return "green_brown";
		case 15:
			return "green_green";
		case 16:
			return "green_purple";
		case 17:
			return "green_red";
		case 18:
			return "green_white";
		case 19:
			return "purple_blue";
		case 20:
			return "purple_brown";
		case 21:
			return "purple_green";
		case 22:
			return "purple_purple";
		case 23:
			return "purple_red";
		case 24:
			return "purple_white";
		case 25:
			return "red_blue";
		case 26:
			return "red_brown";
		case 27:
			return "red_green";
		case 28:
			return "red_purple";
		case 29:
			return "red_red";
		case 30:
			return "red_white";
		case 31:
			return "white_blue";
		case 32:
			return "white_brown";
		case 33:
			return "white_green";
		case 34:
			return "white_purple";
		case 35:
			return "white_red";
		default:
			return "white_white";
		}
	}
}