package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.ElderEyesLayer;
import com.legacy.farlanders.client.render.model.ElderModel;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class ElderFarlanderRenderer extends MobRenderer<ElderFarlanderEntity, ElderModel<ElderFarlanderEntity>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/farlander/farlander_elder.png");

	public ElderFarlanderRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new ElderModel<>(), 0.5F);
		this.addLayer(new ElderEyesLayer<>(this));
	}

	@Override
	protected void preRenderCallback(ElderFarlanderEntity entityliving, MatrixStack matrixStackIn, float f)
	{
		float f1 = 0.9375F;
		matrixStackIn.scale(f1, f1, f1);
	}

	public ResourceLocation getEntityTexture(ElderFarlanderEntity entity)
	{
		return TEXTURE;
	}

}