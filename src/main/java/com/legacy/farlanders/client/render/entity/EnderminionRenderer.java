package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.EnderminionTameLayer;
import com.legacy.farlanders.client.render.entity.layer.eyes.EnderminionEyeLayer;
import com.legacy.farlanders.client.render.model.EnderminionModel;
import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.util.ResourceLocation;

public class EnderminionRenderer extends MobRenderer<EnderminionEntity, EnderminionModel<EnderminionEntity>>
{

	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/enderminion/enderminion.png");

	public EnderminionRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new EnderminionModel<>(), 0.5F);
		this.addLayer(new EnderminionTameLayer<>(this));
		this.addLayer(new HeldItemLayer<EnderminionEntity, EnderminionModel<EnderminionEntity>>(this)
		{
			public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, EnderminionEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
			{
				matrixStackIn.push();
				matrixStackIn.translate(0.0F, 0.3F, 0.0F);
				super.render(matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch);
				matrixStackIn.pop();
			}
		});
		this.addLayer(new EnderminionEyeLayer(this));
	}

	@Override
	public void render(EnderminionEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
	{
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);

		this.getEntityModel().shouldSwing = entityIn.swingProgress;
	}

	public ResourceLocation getEntityTexture(EnderminionEntity entity)
	{
		return TEXTURE;
	}
}