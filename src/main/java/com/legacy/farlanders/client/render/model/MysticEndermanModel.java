package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.model.IHasArm;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;

public class MysticEndermanModel<T extends Entity> extends SegmentedModel<T> implements IHasArm
{
	// fields
	public ModelRenderer Head;
	ModelRenderer Body;
	public ModelRenderer RightArm;
	ModelRenderer LeftArm;
	ModelRenderer LeftLeg1;
	ModelRenderer LeftLeg2;
	ModelRenderer RightLeg1;
	ModelRenderer RightLeg2;
	ModelRenderer Cape;

	public MysticEndermanModel()
	{
		textureWidth = 128;
		textureHeight = 32;

		Head = new ModelRenderer(this, 0, 16);
		Head.addBox(-4F, -8F, -4F, 8, 8, 8);
		Head.setRotationPoint(0F, -13.86667F, -0.4666667F);
		Head.setTextureSize(128, 32);
		setRotation(Head, 0F, 0F, 0F);
		Body = new ModelRenderer(this, 88, 16);
		Body.addBox(0F, 0F, 0F, 8, 12, 4);
		Body.setRotationPoint(-4F, -14F, -2F);
		Body.setTextureSize(128, 32);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 56, 0);
		RightArm.addBox(0F, 0F, 0F, 2, 17, 2);
		RightArm.setRotationPoint(-6F, -14F, -1F);
		RightArm.setTextureSize(128, 32);
		setRotation(RightArm, 0F, 0F, 0.0743572F);
		LeftArm = new ModelRenderer(this, 56, 0);
		LeftArm.addBox(0F, 0F, 0F, 2, 17, 2);
		LeftArm.setRotationPoint(4F, -14F, -1F);
		LeftArm.setTextureSize(128, 32);
		setRotation(LeftArm, 0.1115358F, 0F, -0.0743572F);
		LeftLeg1 = new ModelRenderer(this, 33, 0);
		LeftLeg1.addBox(0F, 0F, 0F, 2, 13, 2);
		LeftLeg1.setRotationPoint(-3F, -3F, -1F);
		LeftLeg1.setTextureSize(128, 32);
		setRotation(LeftLeg1, -0.2230717F, 0F, 0F);
		LeftLeg2 = new ModelRenderer(this, 44, 0);
		LeftLeg2.addBox(0F, 11.66667F, -5F, 2, 12, 2);
		LeftLeg2.setRotationPoint(-3F, -3F, -1F);
		LeftLeg2.setTextureSize(128, 32);
		setRotation(LeftLeg2, 0.1858931F, 0F, 0F);
		RightLeg1 = new ModelRenderer(this, 33, 0);
		RightLeg1.addBox(0F, 0F, 0F, 2, 13, 2);
		RightLeg1.setRotationPoint(1F, -3F, -1F);
		RightLeg1.setTextureSize(128, 32);
		setRotation(RightLeg1, -0.8922867F, 0F, 0F);
		RightLeg2 = new ModelRenderer(this, 44, 0);
		RightLeg2.addBox(0F, -4.266667F, -12.4F, 2, 12, 2);
		RightLeg2.setRotationPoint(1F, -3F, -1F);
		RightLeg2.setTextureSize(128, 32);
		setRotation(RightLeg2, 1.152537F, 0F, 0F);
		Cape = new ModelRenderer(this, 67, 0);
		Cape.addBox(0F, 0F, 0F, 10, 29, 0);
		Cape.setRotationPoint(-5F, -14F, 2.2F);
		Cape.setTextureSize(128, 32);
		setRotation(Cape, 0F, 0F, 0F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.Head, this.Body, this.RightArm, this.LeftArm, this.RightLeg1, this.LeftLeg1, this.RightLeg2, this.LeftLeg2, this.Cape);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T par7Entity, float f, float f1, float f2, float f3, float f4)
	{
		float onGround = 0.0F;

		Head.rotateAngleY = f3 / 57.29578F;
		Head.rotateAngleX = f4 / 57.29578F;
		Cape.rotateAngleX = f1;
		Cape.rotateAngleZ = 0.0F;
		float var7 = MathHelper.sin((onGround - 1.03F) * (float) Math.PI);
		float var8 = MathHelper.sin((1.0F - (1.0F - onGround - 0.5F) * (1.0F - onGround - 0.5F)) * (float) Math.PI);

		if (par7Entity.getMotion() != Vector3d.ZERO)
		{
			LeftLeg1.rotateAngleX = f1 - 0.12f;
			LeftLeg1.rotateAngleZ = 0.0F;
			LeftLeg1.rotateAngleY = 0.0F;
			LeftLeg2.rotateAngleX = f1 + 0.285f;
			LeftLeg2.rotateAngleZ = 0.0F;
			LeftLeg2.rotateAngleY = 0.0F;
			RightLeg1.rotateAngleX = f1 - 0.87f;
			RightLeg1.rotateAngleZ = 0.0F;
			RightLeg1.rotateAngleY = 0.0F;
			RightLeg2.rotateAngleX = f1 + 1.20f;
			RightLeg2.rotateAngleZ = 0.0F;
			RightLeg2.rotateAngleY = 0.0F;
		}
		else
		{
			this.LeftLeg1.rotateAngleZ = 0.01F;
			this.LeftLeg1.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.LeftLeg1.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.LeftLeg1.rotateAngleX -= var7 * 1.2F - var8 * (-0.43F);
			this.LeftLeg1.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.015F;
			this.LeftLeg1.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;
			this.LeftLeg2.rotateAngleZ = 0.01F;
			this.LeftLeg2.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.LeftLeg2.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.LeftLeg2.rotateAngleX -= var7 * 1.2F - var8 * (0.14F);
			this.LeftLeg2.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.015F;
			this.LeftLeg2.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;

			this.RightLeg1.rotateAngleZ = 0.01F;
			this.RightLeg1.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.RightLeg1.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.RightLeg1.rotateAngleX -= var7 * 1.2F - var8 * (-1.33F);
			this.RightLeg1.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.005F;
			this.RightLeg1.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.04F;
			this.RightLeg2.rotateAngleZ = 0.01F;
			this.RightLeg2.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.RightLeg2.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.RightLeg2.rotateAngleX -= var7 * 1.2F - var8 * (1.55F);
			this.RightLeg2.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.005F;
			this.RightLeg2.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.04F;
		}

		this.LeftArm.rotateAngleZ = -0.08F;
		this.LeftArm.rotateAngleY = -(0.1F - var7 * 0.6F);
		this.LeftArm.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
		this.LeftArm.rotateAngleX -= var7 * 1.2F - var8 * (-0.2F);
		this.LeftArm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.050F;
		this.LeftArm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.03F;

		/*if (this.canDoAttackAnim == true)
		{
			if (this.RightArm.rotateAngleX > (0.05f + MathHelper.cos(2.567F) * 0.35f))
				this.RightArm.rotateAngleX = 0.05f + MathHelper.cos(2.567F) * 0.35f;
			else
				this.RightArm.rotateAngleX += 0.12f + MathHelper.cos(2.567F) * 0.35f;
		}
		else*/
		{
			this.RightArm.rotateAngleZ = 0.08F;
			this.RightArm.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.RightArm.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.RightArm.rotateAngleX -= var7 * 1.2F - var8 * (-0.2F);
			this.RightArm.rotateAngleZ -= MathHelper.cos(f2 * 0.09F) * 0.050F;
			this.RightArm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.03F;
		}
	}

	@Override
	public void translateHand(HandSide side, MatrixStack matrixStackIn)
	{
		this.getArmForSide(side).translateRotate(matrixStackIn);
	}

	protected ModelRenderer getArmForSide(HandSide side)
	{
		return side == HandSide.LEFT ? this.LeftArm : this.RightArm;
	}
}