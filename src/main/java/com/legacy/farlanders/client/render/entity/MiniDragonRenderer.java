/*package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.GlowingEyeLayer;
import com.legacy.farlanders.client.render.entity.layer.MiniDragonDeathLayer;
import com.legacy.farlanders.client.render.model.MiniDragonModel;
import com.legacy.farlanders.entity.hostile.boss.summon.MiniDragonEntity;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class MiniDragonRenderer extends MobRenderer<MiniDragonEntity, MiniDragonModel>
{
	private static final ResourceLocation DRAGON_TEXTURES = TheFarlandersMod.locate("textures/entity/mini_dragon/mini_dragon.png");
	private static final ResourceLocation EYES = TheFarlandersMod.locate("textures/entity/mini_dragon/dragon_eyes.png");

	public MiniDragonRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, new MiniDragonModel(0.0F), 0.5F);
		this.addLayer(new GlowingEyeLayer<>(this, EYES));
		this.addLayer(new MiniDragonDeathLayer(this));
	}

	protected void applyRotations(MiniDragonEntity entityLiving, float ageInTicks, float rotationYaw, float partialTicks)
	{
		float f = (float) entityLiving.getMovementOffsets(7, partialTicks)[0];
		float f1 = (float) (entityLiving.getMovementOffsets(5, partialTicks)[1] - entityLiving.getMovementOffsets(10, partialTicks)[1]);
		GlStateManager.rotatef(-f, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotatef(f1 * 10.0F, 1.0F, 0.0F, 0.0F);
		GlStateManager.translatef(0.0F, 0.0F, 1.0F);

		if (entityLiving.deathTime > 0)
		{
			float f2 = ((float) entityLiving.deathTime + partialTicks - 1.0F) / 20.0F * 1.6F;
			f2 = MathHelper.sqrt(f2);
			if (f2 > 1.0F)
			{
				f2 = 1.0F;
			}

			GlStateManager.rotatef(f2 * this.getDeathMaxRotation(entityLiving), 0.0F, 0.0F, 1.0F);
		}

	}

	protected void renderModel(MiniDragonEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor)
	{
		GlStateManager.scalef(0.7F, 0.7F, 0.7F);
		this.bindEntityTexture(entitylivingbaseIn);
		this.entityModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
		if (entitylivingbaseIn.hurtTime > 0)
		{
			GlStateManager.depthFunc(514);
			GlStateManager.disableTexture();
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			GlStateManager.color4f(1.0F, 0.0F, 0.0F, 0.5F);
			this.entityModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
			GlStateManager.enableTexture();
			GlStateManager.disableBlend();
			GlStateManager.depthFunc(515);
		}

	}

	protected ResourceLocation getEntityTexture(MiniDragonEntity entity)
	{
		return DRAGON_TEXTURES;
	}
}*/