package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;
import com.legacy.farlanders.entity.hostile.TitanEntity;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class TitanModel<T extends TitanEntity> extends SegmentedModel<T>
{
	// fields
	ModelRenderer HeadwearRight;
	ModelRenderer HeadwearLeft;
	ModelRenderer NeckDownLeft;
	ModelRenderer NeckDownRight;
	ModelRenderer NeckUpRight;
	ModelRenderer NeckUpLeft;
	ModelRenderer Chest;
	ModelRenderer Body;
	ModelRenderer RightArm;
	ModelRenderer RightArmDown;
	ModelRenderer LeftArm;
	ModelRenderer LeftArmDown;
	ModelRenderer MidWaist;
	ModelRenderer LowerWaist;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer RightDownLeg;
	ModelRenderer LeftDownLeg;
	ModelRenderer Ab1;
	ModelRenderer Ab2;
	ModelRenderer Ab3;
	ModelRenderer Ab5;
	ModelRenderer Ab4;
	ModelRenderer Ab6;
	ModelRenderer LeftHead, LeftLowerLeftHorn, LeftUpperLeftHorn, LeftLowerRightHorn, LeftUpperRightHorn;
	ModelRenderer RightHead, RightLowerLeftHorn, RightUpperLeftHorn, RightLowerRightHorn, RightUpperRightHorn;
	public boolean isAttacking = false;
	public int attackTimer;

	public TitanModel()
	{
		textureWidth = 128;
		textureHeight = 64;

		HeadwearRight = new ModelRenderer(this, 0, 49);
		HeadwearRight.addBox(-3.5F, -8F, -3.5F, 7, 8, 7);
		HeadwearRight.setRotationPoint(-12F, -50F, -2F);
		HeadwearRight.setTextureSize(64, 32);
		setRotation(HeadwearRight, 0F, 0F, 0F);
		HeadwearLeft = new ModelRenderer(this, 0, 49);
		HeadwearLeft.addBox(-3.5F, -8F, -3.5F, 7, 8, 7);
		HeadwearLeft.setRotationPoint(10.5F, -50F, -2F);
		HeadwearLeft.setTextureSize(64, 32);
		setRotation(HeadwearLeft, 0F, 0F, 0F);
		NeckDownLeft = new ModelRenderer(this, 28, 10);
		NeckDownLeft.addBox(0F, 0F, 0F, 6, 1, 6);
		NeckDownLeft.setRotationPoint(7.533333F, -45.5F, -2F);
		NeckDownLeft.setTextureSize(128, 64);
		setRotation(NeckDownLeft, 0.2602503F, 0F, 0F);
		NeckDownRight = new ModelRenderer(this, 25, 10);
		NeckDownRight.addBox(0F, 0F, 0F, 6, 1, 6);
		NeckDownRight.setRotationPoint(-15F, -45.46667F, -2F);
		NeckDownRight.setTextureSize(128, 64);
		setRotation(NeckDownRight, 0.2602503F, 0F, 0F);
		NeckUpRight = new ModelRenderer(this, 16, 0);
		NeckUpRight.addBox(0F, 0F, 0F, 4, 5, 4);
		NeckUpRight.setRotationPoint(-14F, -50F, -2.2F);
		NeckUpRight.setTextureSize(128, 64);
		setRotation(NeckUpRight, 0.2602503F, 0F, 0F);
		NeckUpLeft = new ModelRenderer(this, 16, 0);
		NeckUpLeft.addBox(0F, 0F, 0F, 4, 5, 4);
		NeckUpLeft.setRotationPoint(8.5F, -50F, -2.2F);
		NeckUpLeft.setTextureSize(128, 64);
		setRotation(NeckUpLeft, 0.2602503F, 0F, 0F);
		Chest = new ModelRenderer(this, 40, 27);
		Chest.addBox(0F, -2F, 0F, 36, 10, 8);
		Chest.setRotationPoint(-19F, -43F, -2F);
		Chest.setTextureSize(128, 64);
		setRotation(Chest, 0.2602503F, 0F, 0F);
		Body = new ModelRenderer(this, 78, 2);
		Body.addBox(-4.466667F, 0F, -2F, 20, 18, 5);
		Body.setRotationPoint(-6.533333F, -37F, 3F);
		Body.setTextureSize(128, 64);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 19, 18);
		RightArm.addBox(-1F, -2F, -1F, 5, 24, 5);
		RightArm.setRotationPoint(-23F, -41F, 1F);
		RightArm.setTextureSize(64, 32);
		setRotation(RightArm, 0.1115358F, 0F, 0F);
		RightArmDown = new ModelRenderer(this, 19, 18);
		RightArmDown.addBox(-1F, 18.53333F, 7.333333F, 5, 26, 5);
		RightArmDown.setRotationPoint(-23F, -41F, 1F);
		RightArmDown.setTextureSize(64, 32);
		setRotation(RightArmDown, -0.2974289F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 19, 18);
		LeftArm.addBox(0F, -2F, -1F, 5, 24, 5);
		LeftArm.setRotationPoint(17F, -41F, 1F);
		LeftArm.setTextureSize(64, 32);
		setRotation(LeftArm, 0.1115358F, 0F, 0F);
		LeftArmDown = new ModelRenderer(this, 19, 18);
		LeftArmDown.addBox(0F, 18.5F, 7.3F, 5, 26, 5);
		LeftArmDown.setRotationPoint(17F, -41F, 1F);
		LeftArmDown.setTextureSize(64, 32);
		setRotation(LeftArmDown, -0.2974289F, 0F, 0F);
		MidWaist = new ModelRenderer(this, 34, 0);
		MidWaist.addBox(0F, 0F, 0F, 5, 4, 5);
		MidWaist.setRotationPoint(-3.5F, -19F, 1F);
		MidWaist.setTextureSize(128, 64);
		setRotation(MidWaist, 0F, 0F, 0F);
		LowerWaist = new ModelRenderer(this, 52, 36);
		LowerWaist.addBox(0F, 0F, 0F, 10, 3, 5);
		LowerWaist.setRotationPoint(-6F, -15F, 1F);
		LowerWaist.setTextureSize(128, 64);
		setRotation(LowerWaist, 0F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 0, 10);
		RightLeg.addBox(-1F, 0F, -1F, 5, 20, 5);
		RightLeg.setRotationPoint(-10F, -15F, 2F);
		RightLeg.setTextureSize(128, 64);
		setRotation(RightLeg, -0.1487144F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 0, 10);
		LeftLeg.addBox(-1F, 0F, -1F, 5, 20, 5);
		LeftLeg.setRotationPoint(5F, -15F, 2F);
		LeftLeg.setTextureSize(128, 64);
		setRotation(LeftLeg, -0.1487144F, 0F, 0F);
		RightDownLeg = new ModelRenderer(this, 56, 0);
		RightDownLeg.addBox(-1F, 19.2F, -5.933333F, 5, 19, 5);
		RightDownLeg.setRotationPoint(-10F, -15F, 2F);
		RightDownLeg.setTextureSize(128, 64);
		setRotation(RightDownLeg, 0.1115358F, 0F, 0F);
		LeftDownLeg = new ModelRenderer(this, 56, 0);
		LeftDownLeg.addBox(-1F, 19.2F, -5.9F, 5, 19, 5);
		LeftDownLeg.setRotationPoint(5F, -15F, 2F);
		LeftDownLeg.setTextureSize(128, 64);
		setRotation(LeftDownLeg, 0.1115358F, 0F, 0F);
		Ab1 = new ModelRenderer(this, 0, 0);
		Ab1.addBox(0F, 0F, 0F, 3, 3, 1);
		Ab1.setRotationPoint(0F, -23F, 0.5F);
		Ab1.setTextureSize(128, 64);
		setRotation(Ab1, 0F, 0F, 0F);
		Ab2 = new ModelRenderer(this, 0, 0);
		Ab2.addBox(0F, 0F, 0F, 3, 3, 1);
		Ab2.setRotationPoint(0F, -27F, 0.5F);
		Ab2.setTextureSize(128, 64);
		setRotation(Ab2, 0F, 0F, 0F);
		Ab3 = new ModelRenderer(this, 0, 0);
		Ab3.addBox(0F, 0F, 0F, 3, 3, 1);
		Ab3.setRotationPoint(0F, -31F, 0.5F);
		Ab3.setTextureSize(128, 64);
		setRotation(Ab3, 0F, 0F, 0F);
		Ab5 = new ModelRenderer(this, 0, 0);
		Ab5.addBox(0F, 0F, 0F, 3, 3, 1);
		Ab5.setRotationPoint(-5F, -23F, 0.5F);
		Ab5.setTextureSize(128, 64);
		setRotation(Ab5, 0F, 0F, 0F);
		Ab4 = new ModelRenderer(this, 0, 0);
		Ab4.addBox(0F, 0F, 0F, 3, 3, 1);
		Ab4.setRotationPoint(-5F, -27F, 0.5F);
		Ab4.setTextureSize(128, 64);
		setRotation(Ab4, 0F, 0F, 0F);
		Ab6 = new ModelRenderer(this, 0, 0);
		Ab6.addBox(0F, 0F, 0F, 3, 3, 1);
		Ab6.setRotationPoint(-5F, -31F, 0.5F);
		Ab6.setTextureSize(128, 64);
		setRotation(Ab6, 0F, 0F, 0F);
		LeftHead = new ModelRenderer(this, 92, 46);
		LeftHead.setRotationPoint(10.5F, -50F, -2F);
		setRotation(LeftHead, 0F, 0F, 0F);
		LeftHead.addBox(-4.5F, -9F, -4.5F, 9, 9, 9);

		LeftLowerLeftHorn = new ModelRenderer(this, 34, 60);
		LeftLowerLeftHorn.setRotationPoint(10.5F, -50F, -2F);
		setRotation(LeftHead, 0F, 0F, 0F);
		LeftLowerLeftHorn.addBox(4.5F, -7F, -1F, 3, 2, 2);
		// this.LeftHead.addChild(LeftLowerLeftHorn);

		LeftUpperLeftHorn = new ModelRenderer(this, 36, 57);
		LeftUpperLeftHorn.setRotationPoint(10.5F, -50F, -2F);
		setRotation(LeftHead, 0F, 0F, 0F);
		LeftUpperLeftHorn.addBox(7.5F, -10F, -1F, 2, 5, 2);
		// this.LeftHead.addChild(LeftUpperLeftHorn);

		LeftLowerRightHorn = new ModelRenderer(this, 34, 60);
		LeftLowerRightHorn.setRotationPoint(10.5F, -50F, -2F);
		setRotation(LeftHead, 0F, 0F, 0F);
		LeftLowerRightHorn.addBox(-7.533333F, -7F, -1F, 3, 2, 2);
		// this.LeftHead.addChild(LeftLowerRightHorn);

		LeftUpperRightHorn = new ModelRenderer(this, 34, 57);
		LeftUpperRightHorn.setRotationPoint(10.5F, -50F, -2F);
		setRotation(LeftUpperRightHorn, 0F, 0F, 0F);
		LeftUpperRightHorn.addBox(-9.5F, -10F, -1F, 2, 5, 2);
		LeftUpperRightHorn.setTextureSize(128, 64);
		// this.LeftHead.addChild(LeftUpperRightHorn);

		RightHead = new ModelRenderer(this, 54, 46);
		RightHead.setRotationPoint(-12F, -50F, -2F);
		setRotation(RightHead, 0F, 0F, 0F);
		RightHead.addBox(-4.5F, -9F, -4.5F, 9, 9, 9);

		RightLowerLeftHorn = new ModelRenderer(this, 34, 60);
		RightLowerLeftHorn.setRotationPoint(-12F, -50F, -2F);
		setRotation(RightHead, 0F, 0F, 0F);
		RightLowerLeftHorn.addBox(4.5F, -7F, -1F, 3, 2, 2);
		// this.RightHead.addChild(RightLowerLeftHorn);

		RightUpperLeftHorn = new ModelRenderer(this, 34, 57);
		RightUpperLeftHorn.setRotationPoint(-12F, -50F, -2F);
		setRotation(RightHead, 0F, 0F, 0F);
		RightUpperLeftHorn.addBox(7.533333F, -10F, -1F, 2, 5, 2);
		// this.RightHead.addChild(RightUpperLeftHorn);

		RightLowerRightHorn = new ModelRenderer(this, 34, 60);
		RightLowerRightHorn.setRotationPoint(-12F, -50F, -2F);
		setRotation(RightLowerRightHorn, 0F, 0F, 0F);
		RightLowerRightHorn.addBox(-7.466667F, -7F, -1F, 3, 2, 2);
		// this.RightHead.addChild(RightLowerRightHorn);

		RightUpperRightHorn = new ModelRenderer(this, 36, 57);
		RightUpperRightHorn.setRotationPoint(-12F, -50F, -2F);
		setRotation(RightHead, 0F, 0F, 0F);
		RightUpperRightHorn.addBox(-9.466666F, -10F, -1F, 2, 5, 2);
		RightUpperRightHorn.setTextureSize(128, 64);
		// this.RightHead.addChild(RightUpperRightHorn);

	}
	/*
		public void render(T entity, float f, float f1, float f2, float f3, float f4, float f5)
		{
			super.render(entity, f, f1, f2, f3, f4, f5);
			setRotationAngles(entity, f, f1, f2, f3, f4, f5);
			HeadwearRight.render(f5);
			HeadwearLeft.render(f5);
			NeckDownLeft.render(f5);
			NeckDownRight.render(f5);
			NeckUpRight.render(f5);
			NeckUpLeft.render(f5);
			Chest.render(f5);
			Body.render(f5);
			RightArm.render(f5);
			RightArmDown.render(f5);
			LeftArm.render(f5);
			LeftArmDown.render(f5);
			MidWaist.render(f5);
			LowerWaist.render(f5);
			RightLeg.render(f5);
			LeftLeg.render(f5);
			RightDownLeg.render(f5);
			LeftDownLeg.render(f5);
			Ab1.render(f5);
			Ab2.render(f5);
			Ab3.render(f5);
			Ab5.render(f5);
			Ab4.render(f5);
			Ab6.render(f5);
	
			LeftHead.render(f5);
			LeftLowerLeftHorn.render(f5);
			LeftUpperLeftHorn.render(f5);
			LeftLowerRightHorn.render(f5);
			LeftUpperRightHorn.render(f5);
	
			RightHead.render(f5);
			RightLowerLeftHorn.render(f5);
			RightUpperLeftHorn.render(f5);
			RightLowerRightHorn.render(f5);
			RightUpperRightHorn.render(f5);
		}*/

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.HeadwearRight, this.HeadwearLeft, this.NeckDownRight, this.NeckDownLeft, this.NeckUpLeft, this.NeckUpRight, this.Chest, this.Body, this.RightArm, this.RightArmDown, this.LeftArm, this.LeftArmDown, this.MidWaist, this.LowerWaist, this.RightLeg, this.LeftLeg, this.RightDownLeg, this.LeftDownLeg, this.Ab1, this.Ab2, this.Ab3, this.Ab4, this.Ab5, this.Ab6, this.LeftHead, this.RightHead, LeftLowerLeftHorn, LeftUpperLeftHorn, LeftLowerRightHorn, LeftUpperRightHorn, RightLowerLeftHorn, RightUpperLeftHorn, RightLowerRightHorn, RightUpperRightHorn);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(T par7Entity, float f, float f1, float f2, float f3, float f4)
	{
		this.LeftHead.showModel = true;
		this.RightHead.showModel = true;

		RightLeg.rotateAngleX = -0.1487144F + MathHelper.cos(f * 0.5662F) * f1;
		RightDownLeg.rotateAngleX = 0.1115358F + MathHelper.cos(f * 0.5662F) * f1;
		LeftLeg.rotateAngleX = -0.1487144F + MathHelper.cos(f * 0.5662F + 3.141593F) * f1;
		LeftDownLeg.rotateAngleX = 0.1115358F + MathHelper.cos(f * 0.5662F + 3.141593F) * f1;

		float var7 = -46.5F;

		/*this.LeftHead.rotateAngleY = (10f + f3) / 57.29578F;
		this.LeftHead.rotateAngleX = f4 / 57.29578F;
		
		this.RightHead.rotateAngleY = (-10.f + f3) / 57.29578F;
		this.RightHead.rotateAngleX = f4 / 57.29578F;*/

		this.RightHead.rotationPointY = var7 - 3.0F;
		this.LeftHead.rotationPointY = var7 - 3.0F;

		this.HeadwearRight.rotationPointY = var7 - 2.0F;
		this.HeadwearLeft.rotationPointY = var7 - 2.0F;

		/*this.HeadwearLeft.rotateAngleY = (10f + f3) / 57.29578F;
		this.HeadwearLeft.rotateAngleX = f4 / 57.29578F;		
		
		this.HeadwearRight.rotateAngleY = (-10f + f3) / 57.29578F;
		this.HeadwearRight.rotateAngleX = f4 / 57.29578F;*/

		if (par7Entity.getHoldingPlayer())
		{
			float var9 = 1.0F;
			this.LeftHead.rotationPointY -= var9 * 5.0F;
			this.RightHead.rotationPointY -= var9 * 5.0F;
			this.RightArm.rotateAngleX = 0.1115358F - 1.5F;
			this.RightArmDown.rotateAngleX = -0.2974289F - 1.5F;
			this.LeftArm.rotateAngleX = 0.1115358F - 1.5F;
			this.LeftArmDown.rotateAngleX = -0.2974289F - 1.5F;

			this.LeftHead.rotationPointZ = 20f;
			this.RightHead.rotationPointZ = 20f;
			this.HeadwearLeft.rotationPointZ = 20f;
			this.HeadwearRight.rotationPointZ = 20f;
			this.NeckUpLeft.rotationPointZ = 19.8f;
			this.NeckUpRight.rotationPointZ = 19.8f;
			this.NeckDownLeft.rotationPointZ = 20f;
			this.NeckDownRight.rotationPointZ = 20f;
			this.Chest.rotationPointZ = 20f;
			this.Body.rotationPointZ = 25f;
			this.LeftArm.rotationPointZ = 23f;
			this.LeftArmDown.rotationPointZ = 23f;
			this.RightArm.rotationPointZ = 23f;
			this.RightArmDown.rotationPointZ = 23f;
			this.Ab1.rotationPointZ = 22.5f;
			this.Ab2.rotationPointZ = 22.5f;
			this.Ab3.rotationPointZ = 22.5f;
			this.Ab4.rotationPointZ = 22.5f;
			this.Ab5.rotationPointZ = 22.5f;
			this.Ab6.rotationPointZ = 22.5f;
			this.MidWaist.rotationPointZ = 23f;
			this.LowerWaist.rotationPointZ = 23f;
			this.LeftLeg.rotationPointZ = 24f;
			this.LeftDownLeg.rotationPointZ = 24f;
			this.RightLeg.rotationPointZ = 24f;
			this.RightDownLeg.rotationPointZ = 24f;

			this.LeftHead.rotateAngleY = 0.5f;
			this.LeftHead.rotateAngleX = -0.05f;

			this.RightHead.rotateAngleY = -0.5f;
			this.RightHead.rotateAngleX = -0.05f;

			this.HeadwearLeft.rotateAngleY = 0.5f;
			this.HeadwearLeft.rotateAngleX = -0.05f;

			this.HeadwearRight.rotateAngleY = -0.5f;
			this.HeadwearRight.rotateAngleX = -0.05f;
		}

		else
		{
			this.RightHead.rotationPointZ = -2F;
			this.LeftHead.rotationPointZ = -2F;
			this.HeadwearLeft.rotationPointZ = -2F;
			this.HeadwearRight.rotationPointZ = -2F;
			this.NeckUpLeft.rotationPointZ = -2.2f;
			this.NeckUpRight.rotationPointZ = -2.2f;
			this.NeckDownLeft.rotationPointZ = -2f;
			this.NeckDownRight.rotationPointZ = -2f;
			this.Chest.rotationPointZ = -2f;
			this.Body.rotationPointZ = 3f;
			this.LeftArm.rotationPointZ = 1f;
			this.LeftArmDown.rotationPointZ = 1f;
			this.RightArm.rotationPointZ = 1f;
			this.RightArmDown.rotationPointZ = 1f;
			this.Ab1.rotationPointZ = 0.5f;
			this.Ab2.rotationPointZ = 0.5f;
			this.Ab3.rotationPointZ = 0.5f;
			this.Ab4.rotationPointZ = 0.5f;
			this.Ab5.rotationPointZ = 0.5f;
			this.Ab6.rotationPointZ = 0.5f;
			this.MidWaist.rotationPointZ = 1f;
			this.LowerWaist.rotationPointZ = 1f;
			this.LeftLeg.rotationPointZ = 2f;
			this.LeftDownLeg.rotationPointZ = 2f;
			this.RightLeg.rotationPointZ = 2f;
			this.RightDownLeg.rotationPointZ = 2f;
			this.LeftHead.rotateAngleY = (10f + f3) / 57.29578F;
			this.LeftHead.rotateAngleX = f4 / 57.29578F;
			this.RightHead.rotateAngleY = (-10.f + f3) / 57.29578F;
			this.RightHead.rotateAngleX = f4 / 57.29578F;
			this.HeadwearLeft.rotateAngleY = (10f + f3) / 57.29578F;
			this.HeadwearLeft.rotateAngleX = f4 / 57.29578F;
			this.HeadwearRight.rotateAngleY = (-10f + f3) / 57.29578F;
			this.HeadwearRight.rotateAngleX = f4 / 57.29578F;
		}

		LeftLowerLeftHorn.rotateAngleX = LeftHead.rotateAngleX;
		LeftLowerLeftHorn.rotateAngleY = LeftHead.rotateAngleY;
		LeftLowerLeftHorn.rotateAngleZ = LeftHead.rotateAngleZ;
		LeftLowerLeftHorn.rotationPointZ = LeftHead.rotationPointZ;
		LeftLowerLeftHorn.rotationPointY = LeftHead.rotationPointY;
		
		LeftUpperLeftHorn.rotateAngleX = LeftHead.rotateAngleX;
		LeftUpperLeftHorn.rotateAngleY = LeftHead.rotateAngleY;
		LeftUpperLeftHorn.rotateAngleZ = LeftHead.rotateAngleZ;
		LeftUpperLeftHorn.rotationPointZ = LeftHead.rotationPointZ;
		LeftUpperLeftHorn.rotationPointY = LeftHead.rotationPointY;
		
		LeftLowerRightHorn.rotateAngleX = LeftHead.rotateAngleX;
		LeftLowerRightHorn.rotateAngleY = LeftHead.rotateAngleY;
		LeftLowerRightHorn.rotateAngleZ = LeftHead.rotateAngleZ;
		LeftLowerRightHorn.rotationPointZ = LeftHead.rotationPointZ;
		LeftLowerRightHorn.rotationPointY = LeftHead.rotationPointY;
		
		LeftUpperRightHorn.rotateAngleX = LeftHead.rotateAngleX;
		LeftUpperRightHorn.rotateAngleY = LeftHead.rotateAngleY;
		LeftUpperRightHorn.rotateAngleZ = LeftHead.rotateAngleZ;
		LeftUpperRightHorn.rotationPointZ = LeftHead.rotationPointZ;
		LeftUpperRightHorn.rotationPointY = LeftHead.rotationPointY;
		
		RightLowerLeftHorn.rotateAngleX = RightHead.rotateAngleX;
		RightLowerLeftHorn.rotateAngleY = RightHead.rotateAngleY;
		RightLowerLeftHorn.rotateAngleZ = RightHead.rotateAngleZ;
		RightLowerLeftHorn.rotationPointZ = RightHead.rotationPointZ;
		RightLowerLeftHorn.rotationPointY = RightHead.rotationPointY;
		
		RightUpperLeftHorn.rotateAngleX = RightHead.rotateAngleX;
		RightUpperLeftHorn.rotateAngleY = RightHead.rotateAngleY;
		RightUpperLeftHorn.rotateAngleZ = RightHead.rotateAngleZ;
		RightUpperLeftHorn.rotationPointZ = RightHead.rotationPointZ;
		RightUpperLeftHorn.rotationPointY = RightHead.rotationPointY;
		
		RightLowerRightHorn.rotateAngleX = RightHead.rotateAngleX;
		RightLowerRightHorn.rotateAngleY = RightHead.rotateAngleY;
		RightLowerRightHorn.rotateAngleZ = RightHead.rotateAngleZ;
		RightLowerRightHorn.rotationPointZ = RightHead.rotationPointZ;
		RightLowerRightHorn.rotationPointY = RightHead.rotationPointY;
		
		RightUpperRightHorn.rotateAngleX = RightHead.rotateAngleX;
		RightUpperRightHorn.rotateAngleY = RightHead.rotateAngleY;
		RightUpperRightHorn.rotateAngleZ = RightHead.rotateAngleZ;
		RightUpperRightHorn.rotationPointZ = RightHead.rotationPointZ;
		RightUpperRightHorn.rotationPointY = RightHead.rotationPointY;
	}

	/**
	 * Used for easily adding entity-dependent animations. The second and third
	 * float params here are the same second and third as in the setRotationAngles
	 * method.
	 */
	public void setLivingAnimations(T entityIn, float par2, float par3, float par4)
	{

		if (attackTimer > 0)
		{
			this.RightArm.rotateAngleX = 0.1115358F + (-2.0F + 2.2F * this.func_78172_a((float) attackTimer - par4, 10.0F));
			this.RightArmDown.rotateAngleX = -0.2974289F + (-2.0F + 2.2F * this.func_78172_a((float) attackTimer - par4, 10.0F));
			this.LeftArm.rotateAngleX = 0.1115358F + (-2.0F + 2.2F * this.func_78172_a((float) attackTimer - par4, 10.0F));
			this.LeftArmDown.rotateAngleX = -0.2974289F + (-2.0F + 2.2F * this.func_78172_a((float) attackTimer - par4, 10.0F));
		}
		else
		{
			this.RightArm.rotateAngleX = 0.1115358F + (-0.2F + 1.5F * this.func_78172_a(par2, 13.0F)) * par3;
			this.RightArmDown.rotateAngleX = -0.2974289F + (-0.2F + 1.5F * this.func_78172_a(par2, 13.0F)) * par3;
			this.LeftArm.rotateAngleX = 0.1115358F + (-0.2F - 1.5F * this.func_78172_a(par2, 13.0F)) * par3;
			this.LeftArmDown.rotateAngleX = -0.2974289F + (-0.2F - 1.5F * this.func_78172_a(par2, 13.0F)) * par3;
		}
	}

	private float func_78172_a(float par1, float par2)
	{
		return (Math.abs(par1 % par2 - par2 * 0.5F) - par2 * 0.25F) / (par2 * 0.25F);
	}

}