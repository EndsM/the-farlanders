package com.legacy.farlanders.client.render.entity.layer.eyes;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.GlowingEyeLayer;
import com.legacy.farlanders.client.render.model.EnderGuardianModel;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class EnderGuardianEyeLayer extends GlowingEyeLayer<EnderGuardianEntity, EnderGuardianModel<EnderGuardianEntity>>
{
	public EnderGuardianEyeLayer(IEntityRenderer<EnderGuardianEntity, EnderGuardianModel<EnderGuardianEntity>> renderer)
	{
		super(renderer);
	}

	@Override
	public RenderType getRenderType(EnderGuardianEntity entity)
	{
		ResourceLocation glowTexture = TheFarlandersMod.locate("textures/entity/guardian/" + this.getEyeTexture(entity) + "_eyes" + ".png");
		return RenderType.getEyes(glowTexture);
	}

	public String getEyeTexture(IColoredEyes entity)
	{
		switch (entity.getEyeColor())
		{
		case 1:
			return "green";
		case 2:
			return "red";
		case 3:
			return "white";
		case 4:
			return "brown";
		case 5:
			return "blue";
		default:
			return "purple";
		}
	}
}