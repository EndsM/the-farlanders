package com.legacy.farlanders.client.render.entity.layer.eyes;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.model.EnderColossusModel;
import com.legacy.farlanders.entity.hostile.boss.EnderColossusEntity;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.AbstractEyesLayer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ColossusEyesLayer<T extends EnderColossusEntity> extends AbstractEyesLayer<T, EnderColossusModel<T>>
{
	private static final RenderType RENDER_TYPE = RenderType.getEyes(TheFarlandersMod.locate("textures/entity/colossus/white_eyes.png"));

	public ColossusEyesLayer(IEntityRenderer<T, EnderColossusModel<T>> rendererIn)
	{
		super(rendererIn);
	}

	public RenderType getRenderType()
	{
		return RENDER_TYPE;
	}
}