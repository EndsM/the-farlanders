package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.IMutlipleLayers;
import com.legacy.farlanders.client.render.entity.layer.eyes.LooterEyeLayer;
import com.legacy.farlanders.client.render.model.LooterModel;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.util.ResourceLocation;

public class LooterRenderer extends MobRenderer<LooterEntity, LooterModel<LooterEntity>> implements IMutlipleLayers
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/looter/looter.png");

	public LooterRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new LooterModel<>(), 0.5F);
		this.addLayer(new HeldItemLayer<LooterEntity, LooterModel<LooterEntity>>(this)
		{
			public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, LooterEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
			{
				matrixStackIn.push();
				matrixStackIn.translate(0.05F, 0.2F, 0.0F);
				super.render(matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch);
				matrixStackIn.pop();
			}
		});
		this.addLayer(new LooterEyeLayer(this));
	}

	@Override
	protected void preRenderCallback(LooterEntity entitylivingbaseIn, MatrixStack matrixStackIn, float partialTickTime)
	{
		float f1 = 0.9375F;
		matrixStackIn.scale(f1, f1, f1);
	}

	public ResourceLocation getEntityTexture(LooterEntity entity)
	{
		return TEXTURE;
	}
}