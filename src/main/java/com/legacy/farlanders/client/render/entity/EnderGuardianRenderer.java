package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.EnderGuardianEyeLayer;
import com.legacy.farlanders.client.render.model.EnderGuardianModel;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.util.ResourceLocation;

public class EnderGuardianRenderer extends MobRenderer<EnderGuardianEntity, EnderGuardianModel<EnderGuardianEntity>>
{

	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/guardian/ender_guardian.png");

	public EnderGuardianRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new EnderGuardianModel<>(), 0.5F);
		this.addLayer(new HeldItemLayer<EnderGuardianEntity, EnderGuardianModel<EnderGuardianEntity>>(this)
		{
			public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, EnderGuardianEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
			{
				matrixStackIn.push();
				if (((EnderGuardianEntity) entitylivingbaseIn).getAttacking())
				{
					matrixStackIn.translate(0.0F, 0.0F, -0.1F);
				}
				else
				{
					matrixStackIn.translate(0.0F, 0.2F, 0.0F);
				}

				super.render(matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch);
				matrixStackIn.pop();
			}
		});
		this.addLayer(new EnderGuardianEyeLayer(this));
	}

	@Override
	protected void preRenderCallback(EnderGuardianEntity entityliving, MatrixStack matrixStackIn, float f)
	{
		float f1 = 0.9375F;
		matrixStackIn.scale(f1, f1, f1);
	}

	public ResourceLocation getEntityTexture(EnderGuardianEntity entity)
	{
		return TEXTURE;
	}
}