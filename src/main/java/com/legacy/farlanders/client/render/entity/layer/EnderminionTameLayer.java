package com.legacy.farlanders.client.render.entity.layer;

import com.legacy.farlanders.client.render.model.EnderminionModel;
import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class EnderminionTameLayer<T extends EnderminionEntity, M extends EnderminionModel<T>> extends LayerRenderer<T, M>
{
	private static final ResourceLocation BAND = new ResourceLocation("farlanders", "textures/entity/enderminion/tame.png");
	private final EnderminionModel<EnderminionEntity> minionModel = new EnderminionModel<>();

	public EnderminionTameLayer(IEntityRenderer<T, M> p_i50927_1_)
	{
		super(p_i50927_1_);
	}

	@SuppressWarnings("unchecked")
	public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (entitylivingbaseIn.isTamed())
		{
			((EnderminionModel<T>) this.getEntityModel()).copyModelAttributesTo((EnderminionModel<T>) this.minionModel);
			this.minionModel.setLivingAnimations(entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks);
			this.minionModel.setRotationAngles(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
			IVertexBuilder ivertexbuilder = bufferIn.getBuffer(RenderType.getEntityCutoutNoCull(BAND));
			this.minionModel.render(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}