package com.legacy.farlanders.client;

import com.legacy.farlanders.util.FarlandersPerks;
import com.legacy.farlanders.util.FarlandersRankings;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FarlandersClientEvents
{
	private final Minecraft mc = Minecraft.getInstance();

	@SubscribeEvent
	public void onOpenScreenPost(GuiScreenEvent.InitGuiEvent.Post event)
	{
		/*if (event.getGui() instanceof CustomizeSkinScreen && FarlandersRankings.uuidRanked(mc.getSession().getProfile().getId()))
		{
			event.addWidget(new Button(event.getGui().width - 110, event.getGui().height - 30, 100, 20, "Farlanders Perks", (p_213079_1_) ->
			{
				mc.displayGuiScreen(new FarlandersPerksScreen(event.getGui()));
			}));
		}*/
	}

	@SubscribeEvent
	public void onEntityJoinWorld(EntityJoinWorldEvent event)
	{
		if (event.getEntity() != null && event.getEntity() instanceof AbstractClientPlayerEntity && FarlandersRankings.playerRanked((AbstractClientPlayerEntity) event.getEntity()))
		{
			FarlandersPerks.preparePerks((AbstractClientPlayerEntity) event.getEntity());
		}
	}

	/*@SubscribeEvent
	public void onFogRender(EntityViewRenderEvent.RenderFogEvent event)
	{
		Minecraft mc = Minecraft.getInstance();
		World world = mc.world;
		PlayerEntity player = mc.player;
	
		if (world == null || !world.isRemote)
		{
			return;
		}
		
		if (player.dimension == FarlandersDimensions.nightfallType() && mc.gameRenderer.isShaderActive())
		{
			mc.gameRenderer.loadShader(new ResourceLocation("shaders/post/wobble.json"));
			mc.gameRenderer.stopUseShader();
		}
	}*/
}