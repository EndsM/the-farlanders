package com.legacy.farlanders.client.audio;

import com.legacy.farlanders.TheFarlandersMod;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class FarlandersSounds
{

	private static IForgeRegistry<SoundEvent> iSoundRegistry;

	public static SoundEvent ENTITY_FARLANDER_IDLE, ENTITY_FARLANDER_HURT, ENTITY_FARLANDER_DEATH;

	public static SoundEvent ENTITY_ENDERMINION_IDLE, ENTITY_ENDERMINION_HURT, ENTITY_ENDERMINION_DEATH;

	public static SoundEvent ENTITY_MYSTIC_ENDERMINION_IDLE, ENTITY_MYSTIC_ENDERMINION_HURT,
			ENTITY_MYSTIC_ENDERMINION_DEATH;

	public static SoundEvent ENTITY_MYSTIC_ENDERMAN_IDLE, ENTITY_MYSTIC_ENDERMAN_HURT, ENTITY_MYSTIC_ENDERMAN_DEATH,
			ENTITY_MYSTIC_ENDERMAN_CONFUSION, ENTITY_MYSTIC_ENDERMAN_BLINDNESS;

	public static SoundEvent ENTITY_FANMADE_ENDERMAN_IDLE, ENTITY_FANMADE_ENDERMAN_DEATH;

	public static SoundEvent ENTITY_TITAN_IDLE, ENTITY_TITAN_HURT, ENTITY_TITAN_DEATH, ENTITY_TITAN_DEATH_ECHO,
			ENTITY_TITAN_HOLDING_LONG, ENTITY_TITAN_HOLDING_MED, ENTITY_TITAN_HOLDING_SHORT;

	public static SoundEvent ENTITY_ENDER_GUARDIAN_IDLE, ENTITY_ENDER_GUARDIAN_HURT, ENTITY_ENDER_GUARDIAN_DEATH;

	public static SoundEvent ENTITY_ENDER_GOLEM_IDLE, ENTITY_ENDER_GOLEM_HURT, ENTITY_ENDER_GOLEM_DEATH;

	public static SoundEvent MUSIC_NIGHTFALL_AMBIENT;

	public static void init(IForgeRegistry<SoundEvent> registry)
	{
		iSoundRegistry = registry;

		ENTITY_FARLANDER_IDLE = register("entity.farlander.idle");
		ENTITY_FARLANDER_HURT = register("entity.farlander.hurt");
		ENTITY_FARLANDER_DEATH = register("entity.farlander.death");

		ENTITY_ENDERMINION_IDLE = register("entity.enderminion.idle");
		ENTITY_ENDERMINION_HURT = register("entity.enderminion.hurt");
		ENTITY_ENDERMINION_DEATH = register("entity.enderminion.death");

		ENTITY_MYSTIC_ENDERMINION_IDLE = register("entity.mystic_enderminion.idle");
		ENTITY_MYSTIC_ENDERMINION_HURT = register("entity.mystic_enderminion.hurt");
		ENTITY_MYSTIC_ENDERMINION_DEATH = register("entity.mystic_enderminion.death");

		ENTITY_MYSTIC_ENDERMAN_IDLE = register("entity.mystic_enderman.idle");
		ENTITY_MYSTIC_ENDERMAN_HURT = register("entity.mystic_enderman.hurt");
		ENTITY_MYSTIC_ENDERMAN_DEATH = register("entity.mystic_enderman.death");
		ENTITY_MYSTIC_ENDERMAN_CONFUSION = register("entity.mystic_enderman.confusion");
		ENTITY_MYSTIC_ENDERMAN_BLINDNESS = register("entity.mystic_enderman.blindness");

		ENTITY_FANMADE_ENDERMAN_IDLE = register("entity.fanmade_enderman.idle");
		ENTITY_FANMADE_ENDERMAN_DEATH = register("entity.fanmade_enderman.death");

		ENTITY_TITAN_IDLE = register("entity.titan.idle");
		ENTITY_TITAN_HURT = register("entity.titan.hurt");
		ENTITY_TITAN_DEATH = register("entity.titan.death");
		ENTITY_TITAN_DEATH_ECHO = register("entity.titan.death_echo");

		ENTITY_TITAN_HOLDING_LONG = register("entity.titan.holding_long");
		ENTITY_TITAN_HOLDING_MED = register("entity.titan.holding_med");
		ENTITY_TITAN_HOLDING_SHORT = register("entity.titan.holding_short");

		ENTITY_ENDER_GUARDIAN_IDLE = register("entity.ender_guardian.idle");
		ENTITY_ENDER_GUARDIAN_HURT = register("entity.ender_guardian.hurt");
		ENTITY_ENDER_GUARDIAN_DEATH = register("entity.ender_guardian.death");

		ENTITY_ENDER_GOLEM_IDLE = register("entity.ender_golem.idle");
		ENTITY_ENDER_GOLEM_HURT = register("entity.ender_golem.hurt");
		ENTITY_ENDER_GOLEM_DEATH = register("entity.ender_golem.death");

		MUSIC_NIGHTFALL_AMBIENT = register("music.nightfall.ambient");
	}

	private static SoundEvent register(String name)
	{
		ResourceLocation location = TheFarlandersMod.locate(name);
		SoundEvent sound = new SoundEvent(location);
		sound.setRegistryName(location);
		iSoundRegistry.register(sound);

		return sound;
	}
}