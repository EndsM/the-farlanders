package com.legacy.farlanders.registry;

import java.util.Map.Entry;

import com.legacy.farlanders.FarlandersRegistry;
import com.legacy.farlanders.item.FarlandersItemGroup;
import com.legacy.farlanders.item.armor.FarlandersArmorItem;
import com.legacy.farlanders.item.armor.FarlandersArmorMaterial;
import com.legacy.farlanders.item.wand.ItemMysticWandInvisibility;
import com.legacy.farlanders.item.wand.ItemMysticWandLargeFireball;
import com.legacy.farlanders.item.wand.ItemMysticWandOre;
import com.legacy.farlanders.item.wand.ItemMysticWandRegeneration;
import com.legacy.farlanders.item.wand.ItemMysticWandSmallFireball;
import com.legacy.farlanders.item.wand.ItemMysticWandTeleportation;

import net.minecraft.block.Block;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemTier;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.item.SwordItem;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class FarlandersItems
{
	private static IForgeRegistry<Item> iItemRegistry;

	public static Item endumium_crystal, ender_horn, titan_hide, nightfall_shard;

	public static Item nightfall_sword, nightfall_helmet, nightfall_chestplate, nightfall_leggings, nightfall_boots,
			rebel_farlander_helmet, looter_hood;

	public static Item nightfall_staff, mystic_wand_fire_small, mystic_wand_fire_large, mystic_wand_ore,
			mystic_wand_teleport, mystic_wand_regen, mystic_wand_invisible;

	public static Item farlander_spawn_egg, elder_spawn_egg, wanderer_spawn_egg, enderminion_spawn_egg,
			mystic_enderminion_spawn_egg, ender_guardian_spawn_egg;

	public static Item looter_spawn_egg, rebel_spawn_egg, mystic_enderman_spawn_egg, classic_enderman_spawn_egg,
			fanmade_enderman_spawn_egg, ender_golem_spawn_egg, titan_spawn_egg;

	public static Item end_spirit_spawn_egg, nightfall_spirit_spawn_egg, mini_ender_dragon_spawn_egg,
			ender_colossus_spawn_egg;

	public static void init(Register<Item> event)
	{
		FarlandersItems.iItemRegistry = event.getRegistry();
		registerBlockItems();

		farlander_spawn_egg = register("farlander_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.FARLANDER, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		elder_spawn_egg = register("elder_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.ELDER_FARLANDER, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		wanderer_spawn_egg = register("wanderer_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.WANDERER, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		ender_guardian_spawn_egg = register("ender_guardian_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.ENDER_GUARDIAN, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		looter_spawn_egg = register("looter_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.LOOTER, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		rebel_spawn_egg = register("rebel_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.REBEL, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		enderminion_spawn_egg = register("enderminion_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.ENDERMINION, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		mystic_enderminion_spawn_egg = register("mystic_enderminion_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.MYSTIC_ENDERMINION, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		mystic_enderman_spawn_egg = register("mystic_enderman_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.MYSTIC_ENDERMAN, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		classic_enderman_spawn_egg = register("classic_enderman_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.CLASSIC_ENDERMAN, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		fanmade_enderman_spawn_egg = register("fanmade_enderman_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.FANMADE_ENDERMAN, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		ender_golem_spawn_egg = register("ender_golem_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.ENDER_GOLEM, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		titan_spawn_egg = register("titan_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.TITAN, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));

		//end_spirit_spawn_egg = register("end_spirit_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.END_SPIRIT, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		//nightfall_spirit_spawn_egg = register("nightfall_spirit_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.NIGHTFALL_SPIRIT, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		//mini_ender_dragon_spawn_egg = register("mini_ender_dragon_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.MINI_ENDER_DRAGON, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		//ender_colossus_spawn_egg = register("ender_colossus_spawn_egg", new SpawnEggItem(FarlandersEntityTypes.ENDER_COLOSSUS, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));

		endumium_crystal = register("endumium_crystal", new Item(new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		ender_horn = register("ender_horn", new Item(new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		titan_hide = register("titan_hide", new Item(new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		nightfall_shard = register("nightfall_shard", new Item(new Item.Properties().group(null)));
		nightfall_sword = register("nightfall_sword", new SwordItem(ItemTier.IRON, 5, -2.4F, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));

		nightfall_helmet = register("nightfall_helmet", new FarlandersArmorItem(FarlandersArmorMaterial.NIGHTFALL, EquipmentSlotType.HEAD, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		nightfall_chestplate = register("nightfall_chestplate", new ArmorItem(FarlandersArmorMaterial.NIGHTFALL, EquipmentSlotType.CHEST, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		nightfall_leggings = register("nightfall_leggings", new ArmorItem(FarlandersArmorMaterial.NIGHTFALL, EquipmentSlotType.LEGS, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		nightfall_boots = register("nightfall_boots", new ArmorItem(FarlandersArmorMaterial.NIGHTFALL, EquipmentSlotType.FEET, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		rebel_farlander_helmet = register("rebel_farlander_helmet", new FarlandersArmorItem(FarlandersArmorMaterial.REBEL, EquipmentSlotType.HEAD, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		looter_hood = register("looter_hood", new FarlandersArmorItem(FarlandersArmorMaterial.LOOTER, EquipmentSlotType.HEAD, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));

		mystic_wand_fire_small = register("mystic_wand_fire_small", new ItemMysticWandSmallFireball(new Item.Properties().maxStackSize(1).maxDamage(20).group(FarlandersItemGroup.FARLANDER_TAB)));
		mystic_wand_fire_large = register("mystic_wand_fire_large", new ItemMysticWandLargeFireball(new Item.Properties().maxStackSize(1).maxDamage(20).group(FarlandersItemGroup.FARLANDER_TAB)));
		mystic_wand_ore = register("mystic_wand_ore", new ItemMysticWandOre(new Item.Properties().maxStackSize(1).maxDamage(2).group(FarlandersItemGroup.FARLANDER_TAB)));
		mystic_wand_teleport = register("mystic_wand_teleport", new ItemMysticWandTeleportation(new Item.Properties().maxStackSize(1).maxDamage(5).group(FarlandersItemGroup.FARLANDER_TAB)));
		mystic_wand_regen = register("mystic_wand_regen", new ItemMysticWandRegeneration(new Item.Properties().maxStackSize(1).maxDamage(2).group(FarlandersItemGroup.FARLANDER_TAB)));
		mystic_wand_invisible = register("mystic_wand_invisible", new ItemMysticWandInvisibility(new Item.Properties().maxStackSize(1).maxDamage(2).group(FarlandersItemGroup.FARLANDER_TAB)));
		//nightfall_staff = register("nightfall_staff", new ItemNightfallStaff(new Item.Properties().maxStackSize(1).maxDamage(5).group(FarlandersItemGroup.FARLANDER_TAB)));
	}

	private static void registerBlockItems()
	{
		for (Entry<Block, ItemGroup> entry : FarlandersBlocks.blockItemMap.entrySet())
		{
			FarlandersRegistry.register(iItemRegistry, entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), new Item.Properties().group(entry.getValue())));
		}
		FarlandersBlocks.blockItemMap.clear();

		for (Entry<Block, Item.Properties> entry : FarlandersBlocks.blockItemPropertiesMap.entrySet())
		{
			FarlandersRegistry.register(iItemRegistry, entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), entry.getValue()));
		}
		FarlandersBlocks.blockItemPropertiesMap.clear();
	}

	private static Item register(String name, Item item)
	{
		FarlandersRegistry.register(iItemRegistry, name, item);
		return item;
	}
}