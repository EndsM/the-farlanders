package com.legacy.farlanders.registry;

import java.util.LinkedHashMap;
import java.util.Map;

import com.legacy.farlanders.FarlandersRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class FarlandersBlocks
{
	public static Block endumium_ore, endumium_block;

	public static Map<Block, ItemGroup> blockItemMap = new LinkedHashMap<>();
	public static Map<Block, Item.Properties> blockItemPropertiesMap = new LinkedHashMap<>();

	private static IForgeRegistry<Block> iBlockRegistry;

	public static void init(Register<Block> event)
	{
		FarlandersBlocks.iBlockRegistry = event.getRegistry();

		endumium_ore = register("endumium_ore", new Block(Block.Properties.from(Blocks.EMERALD_ORE)));
		endumium_block = register("endumium_block", new Block(Block.Properties.from(Blocks.EMERALD_BLOCK)));
	}

	public static Block register(String name, Block block)
	{
		register(name, block, ItemGroup.BUILDING_BLOCKS);
		return block;
	}

	public static <T extends ItemGroup> Block register(String key, Block block, T itemGroup)
	{
		blockItemMap.put(block, itemGroup);
		return registerBlock(key, block);
	}

	public static Block registerBlock(String name, Block block)
	{
		if (iBlockRegistry != null)
		{
			FarlandersRegistry.register(iBlockRegistry, name, block);
		}

		return block;
	}
}