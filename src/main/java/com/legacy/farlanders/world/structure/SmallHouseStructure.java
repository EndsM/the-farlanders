package com.legacy.farlanders.world.structure;

import java.util.Random;

import com.google.common.collect.Lists;
import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.registry.FarlandersEntityTypes;
import com.legacy.farlanders.registry.FarlandersStructures;
import com.legacy.structure_gel.util.ConfigTemplates.StructureConfig;
import com.legacy.structure_gel.worldgen.jigsaw.AbstractGelStructurePiece;
import com.legacy.structure_gel.worldgen.jigsaw.GelConfigJigsawStructure;
import com.mojang.serialization.Codec;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.SpawnReason;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.feature.jigsaw.JigsawManager.IPieceFactory;
import net.minecraft.world.gen.feature.jigsaw.JigsawPiece;
import net.minecraft.world.gen.feature.structure.IStructurePieceType;
import net.minecraft.world.gen.feature.structure.VillageConfig;
import net.minecraft.world.gen.feature.template.TemplateManager;

public class SmallHouseStructure extends GelConfigJigsawStructure
{
	public SmallHouseStructure(Codec<VillageConfig> configFactoryIn, StructureConfig config)
	{
		super(configFactoryIn, config, 0, true, true);
		this.setSpawnList(EntityClassification.MONSTER, Lists.newArrayList());
	}

	@Override
	public int getSeed()
	{
		return 395674;
	}

	@Override
	public IPieceFactory getPieceType()
	{
		return SmallHouseStructure.Piece::new;
	}

	public static class Piece extends AbstractGelStructurePiece
	{
		public Piece(TemplateManager template, JigsawPiece jigsawPiece, BlockPos pos, int groundLevelDelta, Rotation rotation, MutableBoundingBox boundingBox)
		{
			super(template, jigsawPiece, pos, groundLevelDelta, rotation, boundingBox);
		}

		public Piece(TemplateManager template, CompoundNBT nbt)
		{
			super(template, nbt);
		}

		@Override
		public IStructurePieceType getStructurePieceType()
		{
			return FarlandersStructures.SMALL_HOUSE.getPieceType();
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, IServerWorld worldIn, Random rand, MutableBoundingBox bounds)
		{
			if (key.equals("farlander"))
			{
				setAir(worldIn, pos);
				FarlanderEntity entity = createEntity(FarlandersEntityTypes.FARLANDER, worldIn, pos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.equals("elder"))
			{
				setAir(worldIn, pos);
				ElderFarlanderEntity entity = createEntity(FarlandersEntityTypes.ELDER_FARLANDER, worldIn, pos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.equals("golem"))
			{
				setAir(worldIn, pos);

				int safeY = worldIn.getHeight(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, pos.getX(), pos.getZ());
				BlockPos groundPos = new BlockPos(pos.getX(), safeY, pos.getZ());

				EnderGolemEntity entity = createEntity(FarlandersEntityTypes.ENDER_GOLEM, worldIn, groundPos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(groundPos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.equals("chest"))
			{
				worldIn.setBlockState(pos, Blocks.COBWEB.getDefaultState(), 3);
				ChestTileEntity tile = (ChestTileEntity) worldIn.getTileEntity(pos.down());
				tile.setLootTable(TheFarlandersMod.locate("chests/elder_house_chest"), worldIn.getRandom().nextLong());
			}
		}
	}
}
