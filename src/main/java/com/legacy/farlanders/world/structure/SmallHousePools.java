package com.legacy.farlanders.world.structure;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FarlandersProcessors;
import com.legacy.structure_gel.util.GelCollectors;
import com.legacy.structure_gel.worldgen.jigsaw.JigsawRegistryHelper;

import net.minecraft.world.gen.feature.jigsaw.JigsawPattern;

public class SmallHousePools
{
	public static final JigsawPattern ROOT;

	public static void init()
	{
	}

	static
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(TheFarlandersMod.MODID, "small_house/");
		ROOT = registry.register("root", registry.builder().names("root").build());
		registry.register("house", registry.builder().names(GelCollectors.mapOf(String.class, Integer.class, "house_birch", 10, "house_oak", 10, "house_nether_brick", 5, "house_elder", 1)).processors(FarlandersProcessors.STAIRS_TO_GRASS).maintainWater(false).build());
		registry.register("golem", registry.builder().names("golem").maintainWater(false).build());
	}
}
