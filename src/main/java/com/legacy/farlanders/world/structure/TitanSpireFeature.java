package com.legacy.farlanders.world.structure;

import java.util.Random;

import com.legacy.farlanders.FarlandersConfig;
import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.registry.FarlandersEntityTypes;
import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class TitanSpireFeature extends Feature<NoFeatureConfig>
{
	private BlockState replaceID;

	public TitanSpireFeature(Codec<NoFeatureConfig> configFactoryIn)
	{
		super(configFactoryIn);
		this.replaceID = Blocks.GRASS_BLOCK.getDefaultState();
	}

	public boolean locationIsValidSpawn(IWorld worldIn, int x, int y, int z)
	{
		BlockPos pos = new BlockPos(x, y, z);

		int distanceToAir = 0;
		BlockState checkID = worldIn.getBlockState(pos);
		for (; distanceToAir < 255 && checkID != Blocks.AIR.getDefaultState(); distanceToAir++)
		{
			checkID = worldIn.getBlockState(pos.add(0.0D, distanceToAir, 0.0D));

			if (distanceToAir > 100)
				return false;
			if (distanceToAir < 0)
				return false;
		}

		if (distanceToAir > 2)
			return false;

		y += distanceToAir - 1;
		BlockState blockID = worldIn.getBlockState(pos);
		BlockState blockIDAbove = worldIn.getBlockState(pos.up());
		BlockState blockIDBelow = worldIn.getBlockState(pos.down());

		if (blockIDAbove != Blocks.AIR.getDefaultState())
			return false;
		if ((blockID.getBlock() == Blocks.SNOW && blockIDBelow == replaceID) || (blockID.getBlock() == Blocks.GRASS && blockIDBelow == replaceID) || (blockID == Blocks.POPPY.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.DANDELION.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.DEAD_BUSH.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.CACTUS.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.BROWN_MUSHROOM.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.RED_MUSHROOM.getDefaultState() && blockIDBelow == replaceID))
			return true;
		else if (blockID == replaceID)
			return true;

		return false;
	}

	@Override
	public boolean generate(ISeedReader worldIn, ChunkGenerator generatorIn, Random randomIn, BlockPos posIn, NoFeatureConfig configIn)
	{
		int x = posIn.getX();
		int y = posIn.getY();
		int z = posIn.getZ();

		if (FarlandersConfig.COMMON.obsidianSpireSpawnRate.get() > 1 && randomIn.nextInt(FarlandersConfig.COMMON.obsidianSpireSpawnRate.get()) != 0 || FarlandersConfig.COMMON.obsidianSpireSpawnRate.get() <= 1)
			return false;

		if (this.locationIsValidSpawn(worldIn, x, y, z))
		{
			int yOffset = randomIn.nextInt(32) + 8;
			int xZOffset = randomIn.nextInt(4) + 3;
			int newY;
			int newX = 0;
			int newZ = 0;
			int finalX;

			for (newY = x - xZOffset; newY <= x + xZOffset; ++newY)
			{
				for (newX = z - xZOffset; newX <= z + xZOffset; ++newX)
				{
					newZ = newY - x;
					finalX = newX - z;

					if (newZ * newZ + finalX * finalX <= xZOffset * xZOffset + 1 && worldIn.getBlockState(new BlockPos(newY, y - 1, newX)) != this.replaceID)
					{
						return false;
					}
				}
			}

			for (newY = y; newY < y + yOffset && newY < 128; ++newY)
			{
				for (newX = x - xZOffset; newX <= x + xZOffset; ++newX)
				{
					for (newZ = z - xZOffset; newZ <= z + xZOffset; ++newZ)
					{
						finalX = newX - x;
						int finalZ = newZ - z;

						if (finalX * finalX + finalZ * finalZ <= xZOffset * xZOffset + 1)
						{
							worldIn.setBlockState(new BlockPos(newX, newY, newZ), Blocks.OBSIDIAN.getDefaultState(), 2);
						}
					}
				}
			}

			if (worldIn.getRandom().nextBoolean())
			{
				// Spire Loot Chest
				worldIn.setBlockState(new BlockPos(newX - 6, newY - 3, newZ - 4), Blocks.CHEST.getDefaultState(), 2);
				ChestTileEntity tile = (ChestTileEntity) worldIn.getTileEntity(new BlockPos(newX - 6, newY - 3, newZ - 4));
				tile.setLootTable(TheFarlandersMod.locate("chests/spire_chest"), worldIn.getRandom().nextLong());
			}
			else
			{
				// Spire Loot Chest
				worldIn.setBlockState(new BlockPos(newX - 6, newY - 6, newZ - 4), Blocks.CHEST.getDefaultState(), 2);
				ChestTileEntity tile2 = (ChestTileEntity) worldIn.getTileEntity(new BlockPos(newX - 6, newY - 6, newZ - 4));
				tile2.setLootTable(TheFarlandersMod.locate("chests/spire_chest"), worldIn.getRandom().nextLong());
			}

			{
				if (worldIn.getRandom().nextInt(4) == 3)
				{
					TitanEntity var4 = new TitanEntity(FarlandersEntityTypes.TITAN, worldIn.getWorld());
					var4.setPosition(newX - 6, newY + 2, newZ - 4);
					var4.enablePersistence();
					worldIn.addEntity(var4);
				}
				else
				{
					EnderGolemEntity var5 = new EnderGolemEntity(FarlandersEntityTypes.ENDER_GOLEM, worldIn.getWorld());
					var5.setPosition(newX - 6, newY + 2, newZ - 4);
					var5.enablePersistence();
					worldIn.addEntity(var5);
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}
}
