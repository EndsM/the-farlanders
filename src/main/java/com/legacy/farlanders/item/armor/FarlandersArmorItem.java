package com.legacy.farlanders.item.armor;

import com.legacy.farlanders.client.render.model.armor.LooterHoodModel;
import com.legacy.farlanders.client.render.model.armor.NightfallHelmetModel;
import com.legacy.farlanders.client.render.model.armor.RebelHelmetModel;
import com.legacy.farlanders.registry.FarlandersItems;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class FarlandersArmorItem extends ArmorItem
{
	public FarlandersArmorItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder)
	{
		super(materialIn, slot, builder);
	}

	@SuppressWarnings("unchecked")
	@OnlyIn(Dist.CLIENT)
	@Override
	public <A extends BipedModel<?>> A getArmorModel(LivingEntity entityLiving, ItemStack itemStack, EquipmentSlotType armorSlot, A _default)
	{
		if (this == FarlandersItems.looter_hood)
			return (A) new LooterHoodModel<LivingEntity>(0.0F);
		else if (this == FarlandersItems.rebel_farlander_helmet)
			return (A) new RebelHelmetModel<LivingEntity>(0.0F);
		else
			return (A) new NightfallHelmetModel<LivingEntity>(1.0F);
	}

}
