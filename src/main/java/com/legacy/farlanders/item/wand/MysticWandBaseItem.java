package com.legacy.farlanders.item.wand;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;

public class MysticWandBaseItem extends Item
{
	private TextFormatting color = TextFormatting.WHITE;

	public MysticWandBaseItem(Properties properties)
	{
		super(properties);
	}

	public String getTranslationKey(ItemStack stack)
	{
		return color + "item.farlanders.mystic_wand";
	}

	protected TextFormatting getTextColor()
	{
		return color;
	}

	protected void setTextColor(TextFormatting color)
	{
		this.color = color;
	}
}
