package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.SmallFireballEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemMysticWandSmallFireball extends MysticWandBaseItem
{
	public ItemMysticWandSmallFireball(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(TextFormatting.GOLD);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
	{
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		playerIn.getCooldownTracker().setCooldown(this, 20);

		if (!worldIn.isRemote)
		{
			Vector3d look = playerIn.getLookVec();
			SmallFireballEntity fireball2 = new SmallFireballEntity(worldIn, playerIn, 1, 1, 1);
			fireball2.setPosition(playerIn.getPosX() + look.x, playerIn.getPosY() + 1.5F + look.y, playerIn.getPosZ() + look.z);
			fireball2.accelerationX = look.x * 0.1;
			fireball2.accelerationY = look.y * 0.1;
			fireball2.accelerationZ = look.z * 0.1;
			worldIn.addEntity(fireball2);

			itemstack.damageItem(1, playerIn, (player) -> player.sendBreakAnimation(playerIn.getActiveHand()));
		}

		worldIn.playSound(playerIn, playerIn.getPosition(), SoundEvents.ENTITY_GHAST_SHOOT, SoundCategory.PLAYERS, 1.0F, 1.5F);
		playerIn.addStat(Stats.ITEM_USED.get(this));
		return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
	}

	@Override
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add(new TranslationTextComponent("gui.item.wand.small_fireball").mergeStyle(TextFormatting.GRAY));
	}
}