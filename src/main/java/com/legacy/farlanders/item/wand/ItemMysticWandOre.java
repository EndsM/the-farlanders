package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.registry.FarlandersBlocks;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class ItemMysticWandOre extends MysticWandBaseItem
{

	public ItemMysticWandOre(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(TextFormatting.GREEN);
	}

	public ActionResultType onItemUse(ItemUseContext context)
	{
		PlayerEntity playerentity = context.getPlayer();
		IWorld iworld = context.getWorld();
		BlockPos blockpos = context.getPos();
		int randomNr = 0;

		if (!playerentity.canPlayerEdit(blockpos, null, context.getItem()))
		{
			return ActionResultType.FAIL;
		}

		Block i = iworld.getBlockState(blockpos).getBlock();

		if (i == Blocks.STONE)
		{
			Block block = null;
			if (iworld.isRemote())
			{
				iworld.playSound(playerentity, blockpos, SoundEvents.ENTITY_PLAYER_LEVELUP, SoundCategory.PLAYERS, 1.0F, 1.0F);
				return ActionResultType.SUCCESS;
			}
			else
			{
				randomNr = iworld.getRandom().nextInt(30);
				if (randomNr == 0)
					block = Blocks.DIAMOND_ORE;
				else if (randomNr >= 1 && randomNr <= 2)
					block = Blocks.GOLD_ORE;
				else if (randomNr >= 3 && randomNr <= 4)
					block = Blocks.LAPIS_ORE;
				else if (randomNr >= 5 && randomNr <= 7)
					block = Blocks.REDSTONE_ORE;
				else if (randomNr >= 8 && randomNr <= 11)
					block = Blocks.IRON_ORE;
				else if (randomNr == 12)
					block = Blocks.EMERALD_ORE;
				else if (randomNr == 13)
					block = FarlandersBlocks.endumium_ore;
				else
					block = Blocks.COAL_ORE;

				iworld.setBlockState(blockpos, block.getDefaultState(), 2);
				playerentity.getCooldownTracker().setCooldown(this, 60);
				playerentity.addStat(Stats.ITEM_USED.get(this));

				context.getItem().damageItem(1, playerentity, (p_220009_1_) ->
				{
					p_220009_1_.sendBreakAnimation(playerentity.getActiveHand());
				});

				return ActionResultType.SUCCESS;
			}
		}
		else if (i == Blocks.NETHERRACK)
		{
			Block block = null;

			if (iworld.isRemote())
			{
				iworld.playSound(playerentity, blockpos, SoundEvents.ENTITY_PLAYER_LEVELUP, SoundCategory.PLAYERS, 1.0F, 1.0F);
				return ActionResultType.SUCCESS;
			}
			else
			{
				block = Blocks.NETHER_QUARTZ_ORE;
				iworld.setBlockState(blockpos, block.getDefaultState(), 2);
				playerentity.getCooldownTracker().setCooldown(this, 60);
				playerentity.addStat(Stats.ITEM_USED.get(this));

				context.getItem().damageItem(1, playerentity, (p_220009_1_) ->
				{
					p_220009_1_.sendBreakAnimation(playerentity.getActiveHand());
				});

				return ActionResultType.SUCCESS;
			}
		}
		else
		{
			TranslationTextComponent chat = new TranslationTextComponent("gui.item.wand.ore.failure");
			playerentity.sendStatusMessage(chat, true);
			return ActionResultType.FAIL;
		}
	}

	@Override
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add(new TranslationTextComponent("gui.item.wand.ore").mergeStyle(TextFormatting.GRAY));
	}
}