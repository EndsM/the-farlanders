package com.legacy.farlanders.item;

import com.legacy.farlanders.registry.FarlandersItems;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class FarlandersItemGroup 
{

	public static final ItemGroup FARLANDER_TAB = new ItemGroup("farlanders")
	{
		@Override
		public ItemStack createIcon() 
		{
			return new ItemStack(FarlandersItems.mystic_wand_fire_small);
		}
	};
}