package com.legacy.farlanders.util;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.legacy.farlanders.TheFarlandersMod;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.network.play.NetworkPlayerInfo;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class FarlandersPerks
{
	private static final ResourceLocation MYSTIC_CAPE = TheFarlandersMod.locate("textures/entity/mystic.png");
	private static final ExecutorService THREAD_POOL = new ThreadPoolExecutor(0, 2, 1L, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
	private static final Field PLAYER_TEXTURES = ObfuscationReflectionHelper.findField(NetworkPlayerInfo.class, "field_187107_a");

	public static void preparePerks(AbstractClientPlayerEntity player)
	{
		THREAD_POOL.submit(() ->
		{
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
				return;
			}

			Minecraft.getInstance().deferTask(() -> giveCapePerk(player));
		});
	}

	@SuppressWarnings("unchecked")
	private static void giveCapePerk(AbstractClientPlayerEntity player)
	{
		NetworkPlayerInfo playerInfo;
		try
		{
			playerInfo = Minecraft.getInstance().getConnection().getPlayerInfo(player.getUniqueID());

			if (playerInfo == null)
			{
				return;
			}

			Map<MinecraftProfileTexture.Type, ResourceLocation> playerTextures;
			playerTextures = (Map<MinecraftProfileTexture.Type, ResourceLocation>) PLAYER_TEXTURES.get(playerInfo);
			playerTextures.put(MinecraftProfileTexture.Type.CAPE, MYSTIC_CAPE);

		}
		catch (Throwable t)
		{
			return;
		}

	}
}
