package com.legacy.farlanders.util;

import java.util.HashMap;
import java.util.UUID;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;

public class FarlandersRankings
{
	private static HashMap<String, UUID> rankedPlayers = new HashMap<String, UUID>();

	public static void init()
	{
		putUUID("99ea8410-b36b-4850-b67c-48f3c581b943"); // Fabiulu
		putUUID("1d680bb6-2a9a-4f25-bf2f-a1af74361d69"); // Phygie
		// putUUID("4bfb28a3-005d-4fc9-9238-a55c6c17b575"); // Lachney
		// putUUID("75c298f9-27c8-415b-9a16-329e3884054b"); // MCVinnyQ
	}

	public static void putUUID(String uuid)
	{
		rankedPlayers.put(uuid, UUID.fromString(uuid));
	}

	public static boolean playerRanked(AbstractClientPlayerEntity player)
	{
		return rankedPlayers.get(player.getUniqueID().toString()) != null;
	}

	public static boolean uuidRanked(UUID uuid)
	{
		return rankedPlayers.get(uuid.toString()) != null;
	}
}
