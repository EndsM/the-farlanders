package com.legacy.farlanders;

import java.util.logging.Logger;

import com.legacy.farlanders.client.FarlandersClientEvents;
import com.legacy.farlanders.client.render.FarlanderEntityRendering;
import com.legacy.farlanders.event.FarlandersEvents;
import com.legacy.farlanders.registry.FarlandersFeatures;
import com.legacy.farlanders.util.FarlandersRankings;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(TheFarlandersMod.MODID)
public class TheFarlandersMod
{
	public static final String NAME = "The Farlanders";
	public static final String MODID = "farlanders";
	public static final Logger LOGGER = Logger.getLogger(MODID);

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public TheFarlandersMod()
	{
		/*ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, FarlandersConfig.CLIENT_SPEC);*/
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, FarlandersConfig.COMMON_SPEC);

		FMLJavaModLoadingContext.get().getModEventBus().addListener(TheFarlandersMod::commonInit);
		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> FMLJavaModLoadingContext.get().getModEventBus().addListener(TheFarlandersMod::clientInit));
	}

	public static void clientInit(FMLClientSetupEvent event)
	{
		FarlanderEntityRendering.init();

		// MinecraftForge.EVENT_BUS.register(new FarlandersMusicHandler());
		MinecraftForge.EVENT_BUS.register(new FarlandersClientEvents());
	}

	public static void commonInit(FMLCommonSetupEvent event)
	{
		FarlandersRankings.init();

		MinecraftForge.EVENT_BUS.addListener((BiomeLoadingEvent biomeEvent) -> FarlandersFeatures.addFeatures(biomeEvent));
		MinecraftForge.EVENT_BUS.register(new FarlandersEvents());

		FarlandersFeatures.Configured.init();
	}
}
